// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type sequenceVersionRow struct {
	ID             int64 `xorm:"pk autoincr"`
	TestSequenceID int64
	VersionNr      int
	Description    string

	Message      string
	IsMinor      bool
	CreationDate time.Time
}

func sequenceVersionFromRow(r sequenceVersionRow) test.SequenceVersion {
	return test.SequenceVersion{
		VersionNr:   r.VersionNr,
		Description: r.Description,

		Message:      r.Message,
		IsMinor:      r.IsMinor,
		CreationDate: r.CreationDate,
	}
}

func rowFromSequenceVersion(sv test.SequenceVersion) sequenceVersionRow {
	return sequenceVersionRow{
		VersionNr:   sv.VersionNr,
		Description: sv.Description,

		Message:      sv.Message,
		IsMinor:      sv.IsMinor,
		CreationDate: sv.CreationDate,
	}
}

func saveSequenceVersions(s xorm.Interface, srID int64, svs []test.SequenceVersion) error {
	osvrs, err := listSequenceVersionRows(s, srID)
	if err != nil {
		return err
	}

	vtsc := len(svs) - len(osvrs)
	for _, sv := range svs[:vtsc] {
		err := saveSequenceVersion(s, srID, sv)
		if err != nil {
			return err
		}
	}

	return nil
}

func saveSequenceVersion(s xorm.Interface, srID int64, sv test.SequenceVersion) error {
	svr := rowFromSequenceVersion(sv)
	svr.TestSequenceID = srID

	err := insertSequenceVersionRow(s, &svr)
	if err != nil {
		return err
	}

	err = savePreconditionsSequence(s, svr.ID, sv.Preconditions)
	if err != nil {
		return err
	}

	err = saveSequenceVersionTestCases(s, svr.ID, sv.Cases)
	if err != nil {
		return err
	}

	err = saveTestersForSequenceVersion(s, svr, sv.Tester)
	return err
}

func insertSequenceVersionRow(s xorm.Interface, vr *sequenceVersionRow) error {
	_, err := s.Table(sequenceVersionTable).Insert(vr)
	return err
}

func listSequenceVersions(s xorm.Interface, pID id.ProjectID, srID int64) ([]test.SequenceVersion, error) {
	vrs, err := listSequenceVersionRows(s, srID)
	if err != nil {
		return nil, err
	}

	var versions []test.SequenceVersion
	for _, r := range vrs {
		var v test.SequenceVersion
		v, err = buildSequenceVersion(s, pID, r)
		if err != nil {
			return nil, err
		}

		versions = append(versions, v)
	}

	return versions, nil
}

func buildSequenceVersion(s xorm.Interface, pID id.ProjectID, vr sequenceVersionRow) (test.SequenceVersion, error) {
	var err error
	v := sequenceVersionFromRow(vr)

	v.Preconditions, err = getPreconditionsSequence(s, vr.ID)
	if err != nil {
		return test.SequenceVersion{}, err
	}
	v.Cases, err = listSequenceVersionCases(s, pID, vr.ID)
	if err != nil {
		return test.SequenceVersion{}, err
	}

	v.Tester, err = listTestersForSequenceVersion(s, vr.ID)
	if err != nil {
		return test.SequenceVersion{}, err
	}

	temp, err := test.UpdateInfo(&v)
	if err != nil {
		return test.SequenceVersion{}, err
	}

	v.SequenceInfo = temp.SequenceInfo
	return v, nil
}

func listSequenceVersionRows(s xorm.Interface, srID int64) ([]sequenceVersionRow, error) {
	var vrs []sequenceVersionRow
	err := s.Table(sequenceVersionTable).Desc(versionNumberField).Find(&vrs, &sequenceVersionRow{TestSequenceID: srID})
	if err != nil {
		return nil, err
	}

	return vrs, nil
}
