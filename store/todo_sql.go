/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/todo"
)

const (
	errNoSuchReferenceType = "no reference type with the id %v"
)

type todoSQL struct {
	e *xorm.Engine
}

type todoListRow struct {
	ID     int64
	UserID int64
}

type todoItemRow struct {
	ID            int64 // ID of the row
	TodoItemIndex int   // The index of the item in the todo-list
	ListID        int64
	AuthorID      int64
	ProjectID     int64
	Type          int
	ReferenceID   int64 // ID to the row of the reference
	ReferenceType int   // Type of the reference
	Deadline      time.Time
	CreationDate  time.Time
	Done          bool
}

func (todoSQL todoSQL) Add(list todo.List) error {
	session := todoSQL.e.NewSession()
	defer session.Close()

	if err := session.Begin(); err != nil {
		return err
	}

	err := saveTODOList(session, list)
	if err != nil {
		err = multierror.Append(err, session.Rollback())
		return err
	}

	return session.Commit()
}

// saveTODOList saves a todo-list and every todo-item
func saveTODOList(s xorm.Interface, list todo.List) error {
	userID, err := lookupUserRowIDForUser(s, id.NewActorID(list.Username))
	if err != nil {
		return err
	}

	listRow := todoListRow{
		UserID: userID,
	}

	listID, ex, err := getTodoListRowID(s, userID)
	if err != nil {
		return err
	}
	if !ex {
		err = insertTodoListRow(s, &listRow)
		if err != nil {
			return err
		}
	} else {
		listRow.ID = listID
	}

	return saveTodoItems(s, list, listRow.ID)
}

// getTodoListRowID returns the ID of the list for the given owner
func getTodoListRowID(s xorm.Interface, userID int64) (int64, bool, error) {
	list, ex, err := getTodoListRow(s, userID)
	return list.ID, ex, err
}

// getTodoListRow returns the row of the list for the given user
func getTodoListRow(s xorm.Interface, userID int64) (todoListRow, bool, error) {
	list := todoListRow{
		UserID: userID,
	}
	ex, err := s.Table(todoListTable).Get(&list)
	if err != nil || !ex {
		return todoListRow{}, false, err
	}

	return list, true, nil
}

// insertTodoListRow inserts a new todo-list into the table
func insertTodoListRow(s xorm.Interface, listRow *todoListRow) error {
	_, err := s.Table(todoListTable).Insert(listRow)
	return err
}

// saveTodoItems saves all todo-items of the given todo-list
func saveTodoItems(s xorm.Interface, list todo.List, listID int64) error {
	for _, item := range list.TODOs {
		err := saveTodoItem(s, item, listID)
		if err != nil {
			return err
		}
	}

	return nil
}

// saveTodoItem saves (inserts or updates) the given todo-item
func saveTodoItem(s xorm.Interface, item todo.Item, listID int64) error {
	// Check if item exists, if exists -> update, else insert
	itemRow, ex, err := getTodoItemRow(s, item.Index, listID)
	if err != nil {
		return err
	}
	if !ex {
		itemRow, err = fillItemRow(s, itemRow, item)
		if err != nil {
			return err
		}
		err = insertTodoItemRow(s, itemRow)
	} else {
		// The only value that can change after creation
		// is the "DONE" status
		itemRow.Done = item.Done

		err = updateTodoItemRow(s, itemRow)
	}

	return err
}

// fillItemRow fills the itemRow with the information of the given item.
//
// Returns the filled itemRow or an error if any occurred.
func fillItemRow(s xorm.Interface, itemRow *todoItemRow, item todo.Item) (*todoItemRow, error) {

	// Add authorID to itemRow
	authorRow, ex, err := getUserRow(s, item.Author)
	if !ex || err != nil {
		return nil, err
	}
	itemRow.AuthorID = authorRow.ID

	// Add projectID to itemRow
	projectRowID, ex, err := getProjectRowID(s, item.ProjectID)
	if !ex || err != nil {
		return nil, err
	}
	itemRow.ProjectID = projectRowID

	// Set the type of the item
	itemRow.Type = int(item.Type)

	// Set id to the reference
	switch item.Reference.Type {
	case todo.Case:
		caseID, ex, err := getCaseRowID(s, id.NewTestID(item.ProjectID, item.Reference.ID, true))
		if !ex || err != nil {
			return nil, err
		}
		itemRow.ReferenceID = caseID
		itemRow.ReferenceType = int(todo.Case)
	case todo.Sequence:
		seqID, ex, err := getSequenceRowID(s, id.NewTestID(item.ProjectID, item.Reference.ID, false))
		if !ex || err != nil {
			return nil, err
		}
		itemRow.ReferenceID = seqID
		itemRow.ReferenceType = int(todo.Sequence)
	default:
		return nil, fmt.Errorf(errNoSuchReferenceType, itemRow.ReferenceType)
	}

	itemRow.Deadline = item.Deadline
	itemRow.CreationDate = item.CreationDate
	itemRow.Done = item.Done

	return itemRow, nil
}

// getTodoItemRow returns the row of the todo-item with the given itemID
// from the list with the given listID.
//
// If an error occurred, false and the error
// will be returned. If the item does not exist, the row with the index
// and the listID will be returned.
func getTodoItemRow(s xorm.Interface, itemID int, listID int64) (*todoItemRow, bool, error) {
	itemRow := todoItemRow{
		TodoItemIndex: itemID,
		ListID:        listID,
	}
	ex, err := s.Table(todoItemTable).Get(&itemRow)
	if err != nil {
		return &todoItemRow{}, false, err
	}
	if !ex {
		return &itemRow, false, nil
	}
	return &itemRow, true, nil
}

func insertTodoItemRow(s xorm.Interface, itemRow *todoItemRow) error {
	_, err := s.Table(todoItemTable).Insert(itemRow)
	return err
}

func updateTodoItemRow(s xorm.Interface, itemRow *todoItemRow) error {
	// AllCols() is needed because boolean columns are not updated by default
	affected, err := s.Table(todoItemTable).ID(itemRow.ID).AllCols().Update(itemRow)
	if err != nil {
		return err
	}

	if affected != 1 {
		return fmt.Errorf(errorNoAffectedRows, affected)
	}

	return nil
}

func (todoSQL todoSQL) Get(username string) (*todo.List, error) {
	s := todoSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}
	// Get id of user
	userRow, ex, err := getUserRow(s, id.NewActorID(username))
	if !ex || err != nil {
		return nil, err
	}
	// Get list for user
	listRow, ex, err := getTodoListRow(s, userRow.ID)
	if err != nil {
		return nil, err
	}
	var todoList *todo.List
	// If the user does not have a list, create a new one,
	// because every user has to have one!
	if !ex {
		todoList = todo.NewList(userRow.Name)
	} else {
		todoItems, err := getTodoItems(s, listRow.ID)
		if err != nil {
			return nil, err
		}
		todoList = todo.NewList(userRow.Name)
		for _, item := range todoItems {
			todoList.TODOs = append(todoList.TODOs, *item)
		}
	}

	return todoList, s.Commit()
}

// getTodoItems returns all todo-items for the list with the given row-id
func getTodoItems(s xorm.Interface, listRowID int64) ([]*todo.Item, error) {
	itemRows, err := listItemRows(s, listRowID)
	if err != nil {
		return nil, err
	}
	var todoItems []*todo.Item
	for _, itemRow := range itemRows {
		item := todoItemFromRow(itemRow)
		// Add projectID to todo-item
		projectRow, ex, err := getProjectRowByID(s, itemRow.ProjectID)
		if !ex || err != nil {
			return nil, err
		}
		ownerRow, err := getOwnerRowByID(s, projectRow.OwnerID)
		if err != nil {
			return nil, err
		}
		item.ProjectID = id.NewProjectID(id.NewActorID(ownerRow.Actor()), projectRow.Name)
		// Add author to todo-item
		userRow, ex, err := getUserRowByID(s, itemRow.AuthorID)
		if !ex || err != nil {
			return nil, err
		}
		item.Author = id.NewActorID(userRow.Name)
		// Depending on itemRow.ReferenceType, select the reference item from the respective table
		switch itemRow.ReferenceType {
		case int(todo.Case):
			item.Reference.Type = todo.Case
			caseRow, ex, err := getCaseRowByID(s, itemRow.ReferenceID)
			if !ex || err != nil {
				return nil, err
			}
			item.Reference.ID = caseRow.Name
		case int(todo.Sequence):
			item.Reference.Type = todo.Sequence
			sequenceRow, ex, err := getSequenceRowByID(s, itemRow.ReferenceID)
			if !ex || err != nil {
				return nil, err
			}
			item.Reference.ID = sequenceRow.Name
		default:
			return nil, fmt.Errorf(errNoSuchReferenceType, itemRow.ReferenceType)
		}

		todoItems = append(todoItems, item)
	}

	return todoItems, nil
}

// listItemRows returns all todoItemRows for the list with the given row-id
func listItemRows(s xorm.Interface, listRowID int64) ([]todoItemRow, error) {
	var itemRows []todoItemRow
	err := s.Table(todoItemTable).Desc(todoItemIndexField).Find(&itemRows, &todoItemRow{ListID: listRowID})
	if err != nil {
		return nil, err
	}
	return itemRows, nil
}

// todoItemFromRow converts a todoItemRow to its todo-item with the
// information it contains. The references (to project, author, reference)
// are not resolved. These attributes have to be added separately.
func todoItemFromRow(row todoItemRow) *todo.Item {
	return &todo.Item{
		Index:        row.TodoItemIndex,
		Type:         todo.Type(row.Type),
		Deadline:     row.Deadline,
		CreationDate: row.CreationDate,
		Done:         row.Done,
	}
}
