// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

type projectUserMemberShipRow struct {
	ID          int64
	ProjectID   int64
	UserID      int64
	RoleID      int64
	MemberSince time.Time
}

func saveProjectMembers(s xorm.Interface, pr projectRow, nms map[id.ActorID]project.UserMembership) error {
	err := deleteMemberRowsForProject(s, pr.ID)
	if err != nil {
		return err
	}

	var mIDs []id.ActorID
	var rNames []project.RoleName
	for nm := range nms {
		mIDs = append(mIDs, nm)
		rNames = append(rNames, nms[nm].Role)
	}

	if err != nil {
		return err
	}
	var mrs []projectUserMemberShipRow
	for i, actorID := range mIDs {

		rRow, ex, erro := getRoleRowByNameForProject(s, rNames[i].String(), pr.ID)
		if erro != nil {
			return erro
		}
		if !ex {
			return fmt.Errorf(errorNoRole, rNames[i].String())
		}
		uID, err := lookupUserRowIDForUser(s, actorID)
		if err != nil {
			fmt.Println("err")
			return err
		}
		mr := projectUserMemberShipRow{
			ProjectID:   pr.ID,
			UserID:      uID,
			RoleID:      rRow.ID,
			MemberSince: time.Now().UTC().Round(time.Second)}
		mrs = append(mrs, mr)
	}

	err = insertProjectMemberRows(s, mrs...)
	return err
}

func insertProjectMemberRows(s xorm.Interface, pmrs ...projectUserMemberShipRow) error {
	_, err := s.Table(projectMembershipTable).Insert(&pmrs)
	return err
}

func deleteMemberRowsForProject(s xorm.Interface, prID int64) error {
	_, err := s.Table(projectMembershipTable).Delete(&projectUserMemberShipRow{ProjectID: prID})
	return err
}

func listProjectMemberships(s xorm.Interface, prID int64) (map[id.ActorID]project.UserMembership, error) {
	mrs, err := listUserMemberRowsForProject(s, prID)
	if err != nil {
		return nil, err
	}

	ms := make(map[id.ActorID]project.UserMembership)
	for _, mr := range mrs {
		m, err := memberShipFromRow(s, mr)
		if err != nil {
			return nil, err
		}
		ms[m.User] = *m
	}

	return ms, nil
}

func listUserMemberRowsForProject(s xorm.Interface, prID int64) ([]projectUserMemberShipRow, error) {
	var mrs []projectUserMemberShipRow
	err := s.Table(projectMembershipTable).Find(&mrs, &projectUserMemberShipRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}

	return mrs, nil
}

// listUserMemberShipsForMember lists all UserMemberShipRows for a member
func listUserMemberShipsForMember(s xorm.Interface, memberID int64) ([]projectUserMemberShipRow, error) {
	var projectIDs []projectUserMemberShipRow
	err := s.Table(projectMembershipTable).Find(&projectIDs, &projectUserMemberShipRow{UserID: memberID})
	if err != nil {
		return nil, err
	}

	return projectIDs, nil
}

func memberShipFromRow(s xorm.Interface, mr projectUserMemberShipRow) (*project.UserMembership, error) {
	u, ex, err := getUserByID(s, mr.UserID)
	if err != nil || !ex {
		return nil, err
	}

	pr, ex, err := getProjectRoleByID(s, mr.RoleID)
	if err != nil || !ex {
		return nil, err
	}

	return &project.UserMembership{
		User:        id.NewActorID(u.Name),
		Role:        pr.Name,
		MemberSince: mr.MemberSince,
	}, nil
}
