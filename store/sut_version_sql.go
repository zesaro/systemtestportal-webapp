// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"fmt"

	"github.com/go-xorm/xorm"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	errorNoSUTVersion = "no version with name %#v"
)

type sutVersionRow struct {
	ID        int64
	ProjectID int64
	Name      string
}

func sutVersionFromRow(vr sutVersionRow) *project.Version {
	return &project.Version{
		Name: vr.Name,
	}
}

func rowFromSUTVersion(v *project.Version) sutVersionRow {
	return sutVersionRow{
		Name: v.Name,
	}
}

func saveSUTVersions(s xorm.Interface, prID int64, nvs map[string]*project.Version) error {
	ovs, err := listSUTVersionRows(s, prID)
	if err != nil {
		return err
	}

	var dvrs []sutVersionRow
	for _, ovr := range ovs {
		if _, ok := nvs[ovr.Name]; !ok {
			dvrs = append(dvrs, ovr)
		}
	}

	err = deleteSUTVersionRows(s, dvrs...)
	if err != nil {
		return err
	}

	for _, nv := range nvs {
		err = saveSUTVersion(s, prID, *nv)
		if err != nil {
			return err
		}
	}

	return nil
}

func saveSUTVersion(s xorm.Interface, prID int64, v project.Version) error {
	ovr := sutVersionRow{ProjectID: prID, Name: v.Name}
	ex, err := s.Table(sutVersionTable).Get(&ovr)
	if err != nil {
		return err
	}

	nvr := rowFromSUTVersion(&v)
	nvr.ProjectID = prID
	if !ex {
		err = insertSUTVersionRow(s, &nvr)
	} else {
		nvr.ID = ovr.ID
		err = updateSUTVersionRow(s, &nvr)
	}

	if err != nil {
		return err
	}

	err = saveSUTVariants(s, nvr.ID, v.Variants)

	return err
}

func insertSUTVersionRow(s xorm.Interface, vr *sutVersionRow) error {
	_, err := s.Table(sutVersionTable).Insert(vr)
	return err
}

func updateSUTVersionRow(s xorm.Interface, vr *sutVersionRow) error {
	aff, err := s.Table(sutVersionTable).ID(vr.ID).Update(vr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteSUTVersionRows(s xorm.Interface, vrs ...sutVersionRow) error {
	var ids []int64
	for _, vr := range vrs {
		ids = append(ids, vr.ID)
	}

	_, err := s.Table(sutVersionTable).In(idField, ids).Delete(&sutVersionRow{})
	return err
}

func listSUTVersions(s xorm.Interface, prID int64) (map[string]*project.Version, error) {
	vrs, err := listSUTVersionRows(s, prID)
	if err != nil {
		return nil, err
	}

	vs := make(map[string]*project.Version)
	for _, vr := range vrs {
		v := sutVersionFromRow(vr)
		v.Variants, err = listSUTVariants(s, vr.ID)
		if err != nil {
			return nil, err
		}

		vs[v.Name] = v
	}

	return vs, nil
}

func listSUTVersionRows(s xorm.Interface, prID int64) ([]sutVersionRow, error) {
	var vrs []sutVersionRow
	err := s.Table(sutVersionTable).Find(&vrs, &sutVersionRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}

	return vrs, nil
}

func lookupSUTVersionRowID(s xorm.Interface, prID int64, name string) (int64, error) {
	svr := sutVersionRow{ProjectID: prID, Name: name}
	ex, err := s.Table(sutVersionTable).Get(&svr)
	if err != nil {
		return 0, err
	}

	if !ex {
		return 0, fmt.Errorf(errorNoSUTVersion, name)
	}

	return svr.ID, nil
}
