-- This file is part of SystemTestPortal.
-- Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
-- 
-- SystemTestPortal is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
-- 
-- SystemTestPortal is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
-- 
-- You should have received a copy of the GNU General Public License
-- along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.

-- +migrate Up
INSERT INTO project_roles (ID,
	project_id,
	Name,

	Execute,

	Create_Case,
	Edit_Case,
	Delete_Case,
	Duplicate_Case,
	Assign_Case,

	Create_Sequence,
	Edit_Sequence,
	Delete_Sequence,
	Duplicate_Sequence,
	Assign_Sequence,

	Edit_Members,

	Edit_Project,
	Delete_Project,
	Edit_Permissions)
VALUES
    (1, 1, 'Tester', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
    (2, 2, 'Tester', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
    (3, 3, 'Tester', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- +migrate Down
DELETE FROM project_roles;