// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	sequenceField = "test_sequence"
)

type sequenceLabelRow struct {
	ID           int64
	TestSequence int64
	Label        int64
}

func saveSequenceLabels(s xorm.Interface, sr sequenceRow, npls []project.Label) error {
	err := deleteSequenceLabelRowsForSequence(s, sr.ID)
	if err != nil {
		return err
	}

	var names []string
	for _, npl := range npls {
		names = append(names, npl.Name)
	}

	plrIDs, err := lookupProjectLabelRowIDs(s, sr.ProjectID, names...)
	if err != nil {
		return err
	}

	var slrs []sequenceLabelRow
	for _, plrID := range plrIDs {
		slr := sequenceLabelRow{TestSequence: sr.ID, Label: plrID}
		slrs = append(slrs, slr)
	}

	err = insertSequenceLabelRows(s, slrs...)
	return err
}

func insertSequenceLabelRows(s xorm.Interface, slr ...sequenceLabelRow) error {
	_, err := s.Table(sequenceLabelTable).Insert(&slr)
	return err
}

func deleteSequenceLabelRowsForSequence(s xorm.Interface, srID int64) error {
	_, err := s.Table(sequenceLabelTable).Delete(&sequenceLabelRow{TestSequence: srID})
	return err
}

func listSequenceLabels(s xorm.Interface, trID int64) ([]project.Label, error) {
	plrs, err := listLabelRowsForSequence(s, trID)

	var pls []project.Label
	for _, plr := range plrs {
		pl := labelFromRow(plr)
		pls = append(pls, pl)
	}

	return pls, err
}

func listLabelRowsForSequence(s xorm.Interface, trID int64) ([]projectLabelRow, error) {
	var lIDs []int64
	err := s.Table(sequenceLabelTable).Distinct(labelField).In(sequenceField, trID).Find(&lIDs)
	if err != nil {
		return nil, err
	}

	var plrs []projectLabelRow
	err = s.Table(projectLabelTable).In(idField, lIDs).Asc(nameField).Find(&plrs)
	if err != nil {
		return nil, err
	}

	return plrs, nil
}
