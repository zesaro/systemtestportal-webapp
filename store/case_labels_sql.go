// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"sort"
	"strings"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

const (
	testCaseField = "test_case"
	labelField    = "label"
)

type caseLabelRow struct {
	ID       int64
	TestCase int64
	Label    int64
}

func saveCaseLabels(s xorm.Interface, cr caseRow, npls []project.Label) error {
	err := deleteCaseLabelRowsForCase(s, cr.ID)
	if err != nil {
		return err
	}

	var names []string
	for _, npl := range npls {
		names = append(names, npl.Name)
	}

	plrIDs, err := lookupProjectLabelRowIDs(s, cr.ProjectID, names...)
	if err != nil {
		return err
	}

	var clrs []caseLabelRow
	for _, plrID := range plrIDs {
		clr := caseLabelRow{TestCase: cr.ID, Label: plrID}
		clrs = append(clrs, clr)
	}

	err = insertCaseLabelRows(s, clrs...)
	return err
}

func insertCaseLabelRows(s xorm.Interface, clr ...caseLabelRow) error {
	_, err := s.Table(caseLabelTable).Insert(&clr)
	return err
}

func deleteCaseLabelRowsForCase(s xorm.Interface, crID int64) error {
	_, err := s.Table(caseLabelTable).Delete(&caseLabelRow{TestCase: crID})
	return err
}

func listCaseLabels(s xorm.Interface, cr caseRow) ([]project.Label, error) {
	plrs, err := listLabelRowsForCase(s, cr)

	var pls []project.Label
	for _, plr := range plrs {
		pl := labelFromRow(plr)
		pls = append(pls, pl)
	}
	pls = sortLabels(pls)
	return pls, err
}

func listLabelRowsForCase(s xorm.Interface, cr caseRow) ([]projectLabelRow, error) {
	var lIDs []int64
	err := s.Table(caseLabelTable).Distinct(labelField).In(testCaseField, cr.ID).Find(&lIDs)
	if err != nil {
		return nil, err
	}

	var plrs []projectLabelRow
	err = s.Table(projectLabelTable).In(idField, lIDs).Asc(nameField).Find(&plrs)
	if err != nil {
		return nil, err
	}

	return plrs, nil
}

func sortLabels(labels []project.Label) []project.Label {
	sort.Slice(labels, func(i, j int) bool {
		cmp := strings.Compare(labels[i].Name, labels[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
	return labels
}
