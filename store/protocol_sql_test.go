// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"reflect"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
)

func TestAddAndGetRegularCase(t *testing.T) {
	protocolStore := protocolsSQL{engineWithSampleData(t)}
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[0], dummydata.Cases[0].TestCaseVersions[0], t)

	protocol, err := protocolStore.GetCaseExecutionProtocols(dummydata.CaseProtocols[0].TestVersion.TestID)
	if err != nil {
		t.Errorf("Unexpected error occured while retrieving the caseExecutionProtocols")
	}
	// convert time stamp to UTC ...
	protocol[0].ExecutionDate = protocol[0].ExecutionDate.UTC()
	if !reflect.DeepEqual(protocol[0], dummydata.CaseProtocols[0]) {
		t.Errorf("Expected \n%v\n, got \n%v", dummydata.CaseProtocols[0], protocol[0])
	}
}

func TestAddAndGetMultipleCase(t *testing.T) {
	protocolStore := protocolsSQL{engineWithSampleData(t)}
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[0], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[1], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[2], dummydata.Cases[0].TestCaseVersions[0], t)

	protocols, err := protocolStore.GetCaseExecutionProtocols(dummydata.CaseProtocols[0].TestVersion.TestID)
	if err != nil {
		t.Errorf("Unexpected error occured while retrieving the caseExecutionProtocols")
	}
	for index, protocol := range protocols {
		// convert time stamp to UTC ...
		protocol.ExecutionDate = protocol.ExecutionDate.UTC()
		if !reflect.DeepEqual(protocol, dummydata.CaseProtocols[index]) {
			t.Errorf("Expected \n%v\n, got \n%v", dummydata.CaseProtocols[index], protocol)
		}
	}
}

func TestAddAndGetRegularSequence(t *testing.T) {
	protocolStore := protocolsSQL{engineWithSampleData(t)}
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[0], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[7], dummydata.Cases[4].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[11], dummydata.Cases[6].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[15], dummydata.Cases[8].TestCaseVersions[0], t)
	addSequenceProtocol(protocolStore, dummydata.SequenceProtocols()[0], dummydata.Sequences[1].SequenceVersions[0], t)

	protocol, err := protocolStore.GetSequenceExecutionProtocols(dummydata.SequenceProtocols()[0].TestVersion.TestID)
	if err != nil {
		t.Errorf("Unexpected error occured while retrieving the sequenceExecutionProtocols")
	}
	// set time to UTC ..
	protocol[0].ExecutionDate = protocol[0].ExecutionDate.UTC()
	if !reflect.DeepEqual(protocol[0], dummydata.SequenceProtocols()[0]) {
		t.Errorf("Expected \n%v\n, got \n%v", dummydata.SequenceProtocols()[0], protocol[0])
	}
}

func TestAddAndGetMultipleSequence(t *testing.T) {
	protocolStore := protocolsSQL{engineWithSampleData(t)}
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[0], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[7], dummydata.Cases[4].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[11], dummydata.Cases[6].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[15], dummydata.Cases[8].TestCaseVersions[0], t)
	addSequenceProtocol(protocolStore, dummydata.SequenceProtocols()[0], dummydata.Sequences[1].SequenceVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[3], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[10], dummydata.Cases[1].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[14], dummydata.Cases[4].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[18], dummydata.Cases[6].TestCaseVersions[0], t)
	addSequenceProtocol(protocolStore, dummydata.SequenceProtocols()[1], dummydata.Sequences[1].SequenceVersions[0], t)

	protocols, err := protocolStore.GetSequenceExecutionProtocols(dummydata.SequenceProtocols()[0].TestVersion.TestID)
	if err != nil {
		t.Errorf("Unexpected error occured while retrieving the sequenceExecutionProtocols")
	}

	for index, protocol := range protocols {
		// convert time stamp to UTC ...
		protocol.ExecutionDate = protocol.ExecutionDate.UTC()
		if !reflect.DeepEqual(protocol, dummydata.SequenceProtocols()[index]) {
			t.Errorf("Expected \n%v\n, got \n%v", dummydata.SequenceProtocols()[index], protocol)
		}
	}
}

func GetTestVersionProtocolsMultipleCases(t *testing.T) {
	protocolStore := protocolsSQL{engineWithSampleData(t)}
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[0], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[1], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[2], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[3], dummydata.Cases[0].TestCaseVersions[0], t)

	protocols, err := protocolStore.GetTestVersionProtocols(dummydata.Cases[0].TestCaseVersions[0].ID())
	if err != nil {
		t.Errorf("Unexpected error occured while retrieving protocols")
	}

	if len(protocols) != 4 {
		t.Errorf("Expected 4 protocols, got %v", len(protocols))
	}
}

func GetTestVersionProtocolsMultipleSequences(t *testing.T) {
	protocolStore := protocolsSQL{engineWithSampleData(t)}
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[0], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[7], dummydata.Cases[4].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[11], dummydata.Cases[6].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[15], dummydata.Cases[8].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[3], dummydata.Cases[0].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[10], dummydata.Cases[1].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[14], dummydata.Cases[4].TestCaseVersions[0], t)
	addCaseProtocol(protocolStore, dummydata.CaseProtocols[18], dummydata.Cases[6].TestCaseVersions[0], t)
	addSequenceProtocol(protocolStore, dummydata.SequenceProtocols()[0], dummydata.Sequences[1].SequenceVersions[0], t)
	addSequenceProtocol(protocolStore, dummydata.SequenceProtocols()[1], dummydata.Sequences[1].SequenceVersions[0], t)

	protocols, err := protocolStore.GetTestVersionProtocols(dummydata.Sequences[1].SequenceVersions[0].ID())
	if err != nil {
		t.Errorf("Unexpected error occured while retrieving protocols")
	}

	if len(protocols) != 2 {
		t.Errorf("Expected 2 protocols, got %v", len(protocols))
	}

}
func addCaseProtocol(protocolStore Protocols, protocol test.CaseExecutionProtocol, caseVersion test.CaseVersion, t *testing.T) {
	err := protocolStore.AddCaseProtocol(&protocol, caseVersion)
	if err != nil {
		t.Errorf("Unexpected error occured while adding a caseExecutionProtocol")
	}
}

func addSequenceProtocol(protocolStore Protocols, protocol test.SequenceExecutionProtocol, sequenceVersion test.SequenceVersion, t *testing.T) {
	err := protocolStore.AddSequenceProtocol(&protocol, sequenceVersion)
	if err != nil {
		t.Errorf("Unexpected error occured while adding a caseExecutionProtocol")
	}
}
