/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package store

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/usersession"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// Users defines the interfaces a store for users needs to fulfill
type Users interface {
	handler.UserLister
	creation.RegisterServer
	usersession.Auth
}

// TODOs defines the interfaces a store for todos needs to fulfill
type TODOs interface {
	handler.TODOListAdder
	handler.TODOListGetter
}

// Groups defines the interfaces a store for groups needs to fulfill
type Groups interface {
	handler.GroupLister
	middleware.GroupRetriever
	handler.GroupAdder
}

// Projects defines the interfaces a store for projects needs to fulfill
type Projects interface {
	ListForOwner(id.ActorID) ([]*project.Project, error)
	ListForMember(id.ActorID) ([]*project.Project, error)
	ListForActor(id.ActorID) ([]*project.Project, error)
	ListAll() ([]*project.Project, error)
	ListPublic() ([]*project.Project, error)
	ListInternal() ([]*project.Project, error)
	ListPrivate(id.ActorID) ([]*project.Project, error)
	middleware.ProjectStore
	id.ProjectExistenceChecker
	handler.ProjectAdder
	handler.ProjectDeleter
}

// Cases defines the interfaces a store for test cases needs to fulfill
type Cases interface {
	handler.TestCaseLister
	middleware.TestCaseStore
	id.TestExistenceChecker
	handler.TestCaseAdder
	handler.TestCaseRenamer
	handler.TestCaseDeleter
}

// Sequences defines the interfaces a store for test sequences needs to fulfill
type Sequences interface {
	handler.TestSequenceLister
	middleware.TestSequenceStore
	id.TestExistenceChecker
	handler.TestSequenceAdder
	handler.TestSequenceRenamer
	handler.TestSequenceDeleter
}

type Protocols interface {
	// AddCaseProtocol adds the given protocol to the store
	AddCaseProtocol(r *test.CaseExecutionProtocol, testCaseVersion test.CaseVersion) (err error)
	// AddSequenceProtocol adds the given protocol to the store
	AddSequenceProtocol(r *test.SequenceExecutionProtocol, testSequenceVersion test.SequenceVersion) (err error)
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testcaseID id.TestID) ([]test.CaseExecutionProtocol, error)
	// GetCaseExecutionProtocols gets the protocols for all testcases of the project with given id.
	GetCaseExecutionProtocolsForProject(project id.ProjectID) ([]test.CaseExecutionProtocol, error)
	// GetCaseExecutionProtocol gets the protocol with the given id for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(sequenceID id.TestID) ([]test.SequenceExecutionProtocol, error)
	//GetSequenceExecutionProtocolsForProject gets the protocols for all test sequences of the project with given id.
	GetSequenceExecutionProtocolsForProject(pID id.ProjectID) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error)
	// GetTestVersionProtocols gets the protocols for the testVersion with given id
	GetTestVersionProtocols(testVersionID id.TestVersionID) ([]id.ProtocolID, error)
}
