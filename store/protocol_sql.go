// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"fmt"
	"time"

	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type protocolsSQL struct {
	e *xorm.Engine
}
type caseProtocolRow struct {
	Id              int64
	ProtocolNr      int
	TestCase        int64
	TestVersion     int
	TestSequence    int64
	ProjectRowId    int64
	Tester          string
	IsAnonymous     bool
	SUTVersion      string
	SUTVariant      string
	ExecutionDate   time.Time
	Result          string
	Comment         string
	OtherNeededTime int64
}

type stepProtocolRow struct {
	Id               int64
	TestCase         int64
	CaseProtocol     int
	NeededTime       int64
	ObservedBehavior string
	Result           string
	Comment          string
	Visited          int8
}

type sequenceProtocolRow struct {
	Id              int64
	ProtocolNr      int
	TestSequence    int64
	TestVersion     int
	ProjectRowId    int64
	Result          string
	Tester          string
	IsAnonymous     bool
	SUTVersion      string
	SUTVariant      string
	ExecutionDate   time.Time
	OtherNeededTime int64
}

type preconditionResultRow struct {
	Id                 int64
	ProtocolId         int64
	PreconditionId     int64
	PreconditionResult string
}

func buildCaseProtocolRow(s xorm.Interface, protocol *test.CaseExecutionProtocol) (caseProtocolRow, error) {
	testCaseRow, ex, err := getCaseRowByTestID(s, protocol.TestVersion.TestID)
	if err != nil || !ex {
		return caseProtocolRow{}, err
	}

	return caseProtocolRow{
		ProtocolNr:      protocol.ProtocolNr,
		TestCase:        testCaseRow.ID,
		TestVersion:     protocol.TestVersion.TestVersion(),
		ProjectRowId:    testCaseRow.ProjectID,
		Tester:          protocol.UserName,
		IsAnonymous:     protocol.IsAnonymous,
		SUTVersion:      protocol.SUTVersion,
		SUTVariant:      protocol.SUTVariant,
		ExecutionDate:   protocol.ExecutionDate,
		Result:          protocol.Result.String(),
		Comment:         protocol.Comment,
		OtherNeededTime: protocol.OtherNeededTime.Nanoseconds(),
	}, nil

}

func buildStepProtocolRow(step test.StepExecutionProtocol, testCaseNr int64, caseProtocolNr int) stepProtocolRow {
	var visited int8
	if step.Visited {
		visited = 1
	} else {
		visited = 0
	}
	return stepProtocolRow{
		CaseProtocol:     caseProtocolNr,
		TestCase:         testCaseNr,
		NeededTime:       step.NeededTime.Nanoseconds(),
		ObservedBehavior: step.ObservedBehavior,
		Result:           step.Result.String(),
		Comment:          step.Comment,
		Visited:          visited,
	}
}

func buildSequenceProtocolRow(s xorm.Interface, sequenceProtocol test.SequenceExecutionProtocol) (sequenceProtocolRow, error) {
	testSequenceRow, ex, err := getSequenceRowByTestID(s, sequenceProtocol.TestVersion.TestID)
	if err != nil || !ex {
		return sequenceProtocolRow{}, err
	}

	return sequenceProtocolRow{
		TestSequence:    testSequenceRow.ID,
		TestVersion:     sequenceProtocol.TestVersion.TestVersion(),
		ProtocolNr:      sequenceProtocol.ProtocolNr,
		ProjectRowId:    testSequenceRow.ProjectID,
		Result:          sequenceProtocol.Result.String(),
		Tester:          sequenceProtocol.UserName,
		IsAnonymous:     sequenceProtocol.IsAnonymous,
		SUTVersion:      sequenceProtocol.SUTVersion,
		SUTVariant:      sequenceProtocol.SUTVariant,
		ExecutionDate:   sequenceProtocol.ExecutionDate,
		OtherNeededTime: sequenceProtocol.OtherNeededTime.Nanoseconds(),
	}, nil
}

func buildPreconditionResultRow(protocolId int64, precId int64, result string) preconditionResultRow {
	return preconditionResultRow{
		ProtocolId:         protocolId,
		PreconditionId:     precId,
		PreconditionResult: result,
	}
}

// AddCaseProtocol stores a test.CaseExecutionProtocol
func (prSQL protocolsSQL) AddCaseProtocol(caseExecProtocol *test.CaseExecutionProtocol, testCaseVersion test.CaseVersion) (err error) {
	s := prSQL.e.NewSession()
	defer s.Close()
	if err := s.Begin(); err != nil {
		return err
	}

	err = saveCaseProtocol(s, caseExecProtocol, testCaseVersion)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

// saveCaseProtocol stores a *test.CaseExecutionProtocol, including all of the included test.stepExecutionProtocol-s
func saveCaseProtocol(s xorm.Interface, protocol *test.CaseExecutionProtocol, testCaseVersion test.CaseVersion) error {
	caseProtocolRow, err := buildCaseProtocolRow(s, protocol)
	if err != nil {
		return err
	}
	// write row into caseProtocolTable
	_, err = s.Table(caseProtocolTable).Insert(&caseProtocolRow)

	if err != nil {
		return err
	}

	// write stepExecutionProtocols into stepProtocolTable
	for _, stepProtocol := range protocol.StepProtocols {
		stepProtocolRow := buildStepProtocolRow(stepProtocol, caseProtocolRow.Id, caseProtocolRow.ProtocolNr)
		_, err := s.Table(stepProtocolTable).Insert(stepProtocolRow)
		if err != nil {
			return err
		}
	}

	for _, result := range protocol.PreconditionResults {
		preconditionResultRow := buildPreconditionResultRow(caseProtocolRow.Id, result.Precondition.Id, result.Result)
		_, err := s.Table(caseProtocolPreconditionTable).Insert(&preconditionResultRow)
		if err != nil {
			return err
		}
	}
	return nil
}

//AddSequenceProtocol stores a test.SequenceExecutionProtocol in the database
func (prSQL protocolsSQL) AddSequenceProtocol(r *test.SequenceExecutionProtocol, sequenceVersion test.SequenceVersion) (err error) {
	s := prSQL.e.NewSession()
	defer s.Close()
	if err := s.Begin(); err != nil {
		return err
	}

	// store the actual sequenceExecutionProtocol
	err = saveSequenceProtocol(s, r, sequenceVersion)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	// tag all the related caseExecutionProtocols
	err = tagCaseProtocolsAsSequence(s, r.CaseExecutionProtocols, r)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

// tagCaseProtocolsAsSequence sets the "test_sequence" column of all related caseExecutionProtocols to the unique row id of the sequenceProtocol
// a caseExecutionProtocol is identified through the id.ProtocolID
func tagCaseProtocolsAsSequence(s xorm.Interface, protocols []id.ProtocolID, sequenceProtocol *test.SequenceExecutionProtocol) error {
	for _, protocolNr := range protocols {
		caseRow, ex, err := getCaseRowByTestID(s, protocolNr.TestVersionID.TestID)
		if err != nil || !ex {
			return err
		}

		var row caseProtocolRow
		_, err = s.Table(caseProtocolTable).Where("project_row_id = ?", caseRow.ProjectID).Where("test_case = ?", caseRow.ID).Where("test_version = ?", protocolNr.TestVersionID.TestVersion()).Where("protocol_nr = ?", protocolNr.Protocol()).Get(&row)
		if err != nil {
			return err
		}

		sequenceRow, ex, err := getSequenceRowByTestID(s, sequenceProtocol.TestVersion.TestID)
		if err != nil || !ex {
			return err
		}

		var seqProtRowId int64
		_, err = s.Table(sequenceProtocolTable).Where("project_row_id = ?", sequenceRow.ProjectID).Where("test_sequence = ?", sequenceRow.ID).Where("protocol_nr = ?", sequenceProtocol.ProtocolNr).Where("test_version = ?", sequenceProtocol.TestVersion.TestVersion()).Cols("id").Get(&seqProtRowId)
		row.TestSequence = seqProtRowId
		_, err = s.Table(caseProtocolTable).Update(&row, &caseProtocolRow{ProjectRowId: sequenceRow.ProjectID, TestCase: caseRow.ID, TestVersion: protocolNr.TestVersionID.TestVersion(), ProtocolNr: protocolNr.Protocol()})
		if err != nil {
			return err
		}
	}

	return nil
}

// saveSequenceProtocol writes a sequenceExecutionProtocol to the sequenceProtocolTable
func saveSequenceProtocol(s xorm.Interface, protocol *test.SequenceExecutionProtocol, sequenceVersion test.SequenceVersion) error {
	sequenceProtocolRow, err := buildSequenceProtocolRow(s, *protocol)
	if err != nil {
		return err
	}
	_, err = s.Table(sequenceProtocolTable).Insert(&sequenceProtocolRow)
	if err != nil {
		return err
	}

	for _, result := range protocol.PreconditionResults {
		preconditionResultRow := buildPreconditionResultRow(sequenceProtocolRow.Id, result.Precondition.Id, result.Result)
		_, err := s.Table(sequenceProtocolPreconditionTable).Insert(&preconditionResultRow)
		if err != nil {
			return err
		}
	}

	return nil
}

//GetCaseExecutionProtocols returns a slice of all CaseExecutionProtocols that belong to the given test case.
func (prSQL protocolsSQL) GetCaseExecutionProtocols(testcaseID id.TestID) ([]test.CaseExecutionProtocol, error) {
	s := prSQL.e.NewSession()
	defer s.Close()
	if err := s.Begin(); err != nil {
		return nil, err
	}

	caseProtocols, err := listCaseProtocols(s, testcaseID, -1)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return caseProtocols, s.Commit()
}

// listCaseProtocols returns a slice with all caseExecutionProtocols related to a testCase, identified by it's id.TestID
// If the sequence parameter is >0 only caseExecutionProtocols with 'test_sequence = sequence' will be returned,
// i.e. the related caseExecutionProtocols for a sequenceExecutionProtocol
func listCaseProtocols(s xorm.Interface, testCase id.TestID, sequence int64) ([]test.CaseExecutionProtocol, error) {
	var caseProtocolRows []caseProtocolRow
	if sequence < 0 {
		testCaseRow, ex, err := getCaseRowByTestID(s, testCase)
		if err != nil || !ex {
			return nil, err
		}
		err = s.Table(caseProtocolTable).Where("project_row_id = ?", testCaseRow.ProjectID).Where("test_case = ?", testCaseRow.ID).Find(&caseProtocolRows)
		if err != nil {
			return nil, err
		}
	} else {
		projectRowID, ex, err := getProjectRowID(s, testCase.ProjectID)
		if err != nil || !ex {
			return nil, err
		}
		err = s.Table(caseProtocolTable).Where("test_sequence = ?", sequence).Where("project_row_id = ?", projectRowID).Find(&caseProtocolRows)
		if err != nil {
			return nil, err
		}
	}

	caseProtocols, err := buildCaseProtocols(s, caseProtocolRows)
	if err != nil {
		return nil, err
	}

	return caseProtocols, nil

}

// buildCaseProtocols returns a slice of caseExecutionProtocols, built from the caseProtocolRows
func buildCaseProtocols(s xorm.Interface, caseProtocolRows []caseProtocolRow) ([]test.CaseExecutionProtocol, error) {
	var caseProtocols []test.CaseExecutionProtocol
	for _, row := range caseProtocolRows {
		protocol, err := buildCaseProtocol(s, row)
		if err != nil {
			return nil, err
		}
		caseProtocols = append(caseProtocols, protocol)
	}
	return caseProtocols, nil
}

// buildCaseProtocol constructs a caseExecutionProtocol from a caseProtocolRow
func buildCaseProtocol(s xorm.Interface, row caseProtocolRow) (test.CaseExecutionProtocol, error) {
	result, err := test.GetResultFromString(row.Result)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}

	// need to get related case/project/user data to reconstruct the correct IDs
	csRow, ex, err := getCaseRowByID(s, row.TestCase)
	if err != nil || !ex {
		return test.CaseExecutionProtocol{}, err
	}
	prjRow, ex, err := getProjectRowByID(s, row.ProjectRowId)
	if err != nil || !ex {
		return test.CaseExecutionProtocol{}, err
	}
	owner, err := getOwnerRowByID(s, prjRow.OwnerID)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}
	testID := id.NewTestID(id.NewProjectID(owner, prjRow.Name), csRow.Name, true)

	time, err := time.ParseDuration(fmt.Sprint(row.OtherNeededTime) + "ns")
	duration := duration.Duration{
		Duration: time,
	}
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}

	preconditionResults, err := getPreconditionsForProtocol(s, row.Id, true)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}

	caseProtocol := test.CaseExecutionProtocol{
		TestVersion:         id.NewTestVersionID(testID, row.TestVersion),
		ProtocolNr:          row.ProtocolNr,
		UserName:            row.Tester,
		IsAnonymous:         row.IsAnonymous,
		SUTVersion:          row.SUTVersion,
		SUTVariant:          row.SUTVariant,
		ExecutionDate:       row.ExecutionDate,
		Result:              result,
		Comment:             row.Comment,
		OtherNeededTime:     duration,
		PreconditionResults: preconditionResults,
	}

	caseProtocol, err = addStepProtocols(s, caseProtocol, row.Id)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}

	return caseProtocol, nil
}

func getPreconditionsForProtocol(s xorm.Interface, rowId int64, isCase bool) (preconditionResults []test.PreconditionResult, err error) {
	var preconditionResultRows []preconditionResultRow
	preconditionResults = make([]test.PreconditionResult, 0)
	var table string
	if isCase {
		table = caseProtocolPreconditionTable
	} else {
		table = sequenceProtocolPreconditionTable
	}
	err = s.Table(table).Where("protocol_id = ?", rowId).Find(&preconditionResultRows)
	if err != nil {
		return nil, err
	}

	for _, row := range preconditionResultRows {
		precondition, err := getPreconditionCaseById(s, row.PreconditionId)
		if err != nil {
			return nil, err
		}
		preconditionResult := test.PreconditionResult{
			Precondition: precondition,
			Result:       row.PreconditionResult,
		}
		preconditionResults = append(preconditionResults, preconditionResult)
	}
	return preconditionResults, nil
}

// addStepProtocols populates the caseExecutionProtocol.StepProtocols field with the related stepProtocols
func addStepProtocols(s xorm.Interface, caseProtocol test.CaseExecutionProtocol, rowId int64) (test.CaseExecutionProtocol, error) {
	stepProtocols, err := listStepProtocols(s, rowId)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}
	caseProtocol.StepProtocols = stepProtocols
	return caseProtocol, nil
}

// listStepProtocols returns a slice of stepExecutionProtocols for a caseRowId, which is the unique row identifier of a caseProtocolRow
func listStepProtocols(s xorm.Interface, caseRowId int64) ([]test.StepExecutionProtocol, error) {
	var stepProtocolRows []stepProtocolRow
	err := s.Table(stepProtocolTable).Where("test_case= ?", caseRowId).Find(&stepProtocolRows)
	if err != nil {
		return nil, err
	}

	stepProtocols, err := buildStepProtocols(stepProtocolRows)
	if err != nil {
		return nil, err
	}

	return stepProtocols, nil
}

// buildStepProtocols constructs all the stepExecutionProtocols from the stepProtoclRow slice
func buildStepProtocols(stepProtocolRows []stepProtocolRow) ([]test.StepExecutionProtocol, error) {
	var stepProtocols []test.StepExecutionProtocol
	for _, row := range stepProtocolRows {
		protocol, err := buildStepProtocol(row)
		if err != nil {
			return nil, err
		}
		stepProtocols = append(stepProtocols, protocol)
	}

	return stepProtocols, nil
}

// buildStepProtocol constructs stepExecutionProtocol from the stepProtocolRow
func buildStepProtocol(row stepProtocolRow) (test.StepExecutionProtocol, error) {
	result, err := test.GetResultFromString(row.Result)
	if err != nil {
		return test.StepExecutionProtocol{}, err
	}

	time, err := time.ParseDuration(fmt.Sprint(row.NeededTime) + "ns")
	duration := duration.Duration{
		Duration: time,
	}
	if err != nil {
		return test.StepExecutionProtocol{}, err
	}

	var visited bool
	if row.Visited == 0 {
		visited = false
	} else {
		visited = true
	}
	stepProtocol := test.StepExecutionProtocol{
		NeededTime:       duration,
		ObservedBehavior: row.ObservedBehavior,
		Result:           result,
		Comment:          row.Comment,
		Visited:          visited,
	}
	return stepProtocol, nil
}

//GetCaseExecutionProtocolsForProject returns a slice of all CaseExecutionProtocols, that belongs to the given project.
func (prSQL protocolsSQL) GetCaseExecutionProtocolsForProject(projectID id.ProjectID) ([]test.CaseExecutionProtocol, error) {
	s := prSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	caseProtocols, err := listCaseProtocolsForProject(s, projectID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return caseProtocols, s.Commit()
}

// listCaseProtocolsForProject lists all caseExecutionProtocols related to the project, identified by id.ProjectID
func listCaseProtocolsForProject(s xorm.Interface, projectID id.ProjectID) ([]test.CaseExecutionProtocol, error) {
	var caseProtocolRows []caseProtocolRow
	projectRowID, ex, err := getProjectRowID(s, projectID)
	if err != nil || !ex {
		return nil, err
	}
	err = s.Table(caseProtocolTable).Where("project_row_id = ?", projectRowID).Find(&caseProtocolRows)
	if err != nil {
		return nil, err
	}

	caseProtocols, err := buildCaseProtocols(s, caseProtocolRows)
	if err != nil {
		return nil, err
	}

	return caseProtocols, nil

}

// GetSequenceExecutionProtocols returns a slice of all SequenceExecutionProtocol that belong to a given sequence, identified by the id.TestID
func (prSQL protocolsSQL) GetSequenceExecutionProtocols(testID id.TestID) ([]test.SequenceExecutionProtocol, error) {
	s := prSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	sequenceProtocols, err := listSequenceProtocols(s, testID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return sequenceProtocols, s.Commit()
}

// GetSequenceExecutionProtocolsForProject returns a slice of SequenceExecutionProtocols related to a given id.ProjectID
func (prSQL protocolsSQL) GetSequenceExecutionProtocolsForProject(projectID id.ProjectID) ([]test.SequenceExecutionProtocol, error) {
	s := prSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}

	sequenceProtocols, err := listSequenceProtocolsForProject(s, projectID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	return sequenceProtocols, s.Commit()
}

// listSequenceProtocolsForProject returns a slice of SequenceExecutionProtocols related to a given id.ProjectID
func listSequenceProtocolsForProject(s xorm.Interface, projectID id.ProjectID) ([]test.SequenceExecutionProtocol, error) {
	var sequenceProtocolRows []sequenceProtocolRow
	projectRowID, ex, err := getProjectRowID(s, projectID)
	if err != nil || !ex {
		return nil, err
	}
	err = s.Table(sequenceProtocolTable).Where("project_row_id = ?", projectRowID).Find(&sequenceProtocolRows)
	if err != nil {
		return nil, err
	}

	sequenceProtocols, err := buildSequenceProtocols(s, sequenceProtocolRows)
	if err != nil {
		return nil, err
	}

	return sequenceProtocols, nil
}

// listSequenceProtocols lists all sequenceExecutionProtocols related to a given testSequence, identified by id.TestID
func listSequenceProtocols(s xorm.Interface, testSequence id.TestID) ([]test.SequenceExecutionProtocol, error) {
	var sequenceProtocolRows []sequenceProtocolRow

	testSequenceRow, ex, err := getSequenceRowByTestID(s, testSequence)
	if err != nil || !ex {
		return nil, err
	}

	err = s.Table(sequenceProtocolTable).Where("project_row_id = ?", testSequenceRow.ProjectID).Where("test_sequence = ?", testSequenceRow.ID).Find(&sequenceProtocolRows)
	if err != nil {
		return nil, err
	}

	sequenceProtocols, err := buildSequenceProtocols(s, sequenceProtocolRows)
	if err != nil {
		return nil, err
	}

	return sequenceProtocols, nil
}

// buildSequenceProtocols constructs sequenceExecutionProtocols from sequenceProtocolRows
func buildSequenceProtocols(s xorm.Interface, sequenceProtocolRows []sequenceProtocolRow) ([]test.SequenceExecutionProtocol, error) {
	var sequenceProtocols []test.SequenceExecutionProtocol
	for _, row := range sequenceProtocolRows {
		protocol, err := buildSequenceProtocol(s, row)
		if err != nil {
			return nil, err
		}
		sequenceProtocols = append(sequenceProtocols, protocol)
	}

	return sequenceProtocols, nil
}

// buildSequenceProtocl constructs sequenceExecutionProtocol from a sequenceProtocolRow
func buildSequenceProtocol(s xorm.Interface, row sequenceProtocolRow) (test.SequenceExecutionProtocol, error) {
	result, err := test.GetResultFromString(row.Result)
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}

	sqRow, ex, err := getSequenceRowByID(s, row.TestSequence)
	if err != nil || !ex {
		return test.SequenceExecutionProtocol{}, err
	}
	prjRow, ex, err := getProjectRowByID(s, row.ProjectRowId)
	if err != nil || !ex {
		return test.SequenceExecutionProtocol{}, err
	}
	owner, err := getOwnerRowByID(s, prjRow.OwnerID)
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}
	testID := id.NewTestID(id.NewProjectID(owner, prjRow.Name), sqRow.Name, false)

	time, err := time.ParseDuration(fmt.Sprint(row.OtherNeededTime) + "ns")
	duration := duration.Duration{
		Duration: time,
	}
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}

	preconditionResults, err := getPreconditionsForProtocol(s, row.Id, false)
	sequenceProtocol := test.SequenceExecutionProtocol{
		TestVersion:         id.NewTestVersionID(testID, row.TestVersion),
		ProtocolNr:          row.ProtocolNr,
		UserName:            row.Tester,
		IsAnonymous:         row.IsAnonymous,
		SUTVersion:          row.SUTVersion,
		SUTVariant:          row.SUTVariant,
		ExecutionDate:       row.ExecutionDate,
		Result:              result,
		OtherNeededTime:     duration,
		PreconditionResults: preconditionResults,
	}

	sequenceProtocol, err = addCaseProtocolsToSequence(s, sequenceProtocol, row.Id)
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}
	return sequenceProtocol, nil
}

// addCaseProtocolsToSequence finds all related caseExecutionProtocols and adds their id.ProtocolID to the sequenceExecutionProtocol
func addCaseProtocolsToSequence(s xorm.Interface, sqProtocol test.SequenceExecutionProtocol, rowId int64) (test.SequenceExecutionProtocol, error) {
	caseProtocols, err := listCaseProtocols(s, sqProtocol.TestVersion.TestID, rowId)
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}

	var protocolIDs []id.ProtocolID

	for _, caseProtocol := range caseProtocols {
		protocol := id.NewProtocolID(caseProtocol.TestVersion, caseProtocol.ProtocolNr)
		protocolIDs = append(protocolIDs, protocol)
	}
	sqProtocol.CaseExecutionProtocols = protocolIDs
	return sqProtocol, nil
}

// GetTestVersionProtocols returns a slice of all execution protocols ids for a testVersion
func (prSQL protocolsSQL) GetTestVersionProtocols(testVersionID id.TestVersionID) ([]id.ProtocolID, error) {
	s := prSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return nil, err
	}
	var caseProtocolRows []caseProtocolRow
	var sequenceProtocolRows []sequenceProtocolRow

	if testVersionID.TestID.IsCase() {
		testCaseRow, ex, err := getCaseRowByTestID(s, testVersionID.TestID)
		if err != nil || !ex {
			return nil, err
		}
		err = s.Table(caseProtocolTable).Where("project_row_id = ?", testCaseRow.ProjectID).Where("test_case = ?", testCaseRow.ID).Find(&caseProtocolRows)

		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return nil, err
		}
	} else {
		testSequenceRow, ex, err := getSequenceRowByTestID(s, testVersionID.TestID)
		if err != nil || !ex {
			return nil, err
		}
		err = s.Table(sequenceProtocolTable).Where("project_row_id = ?", testSequenceRow.ProjectID).Where("test_sequence = ?", testSequenceRow.ID).Find(&sequenceProtocolRows)
		if err != nil {
			err = multierror.Append(err, s.Rollback())
			return nil, err
		}
	}

	var protocols []id.ProtocolID

	for _, caseProtocolRow := range caseProtocolRows {
		protocol := id.NewProtocolID(testVersionID, caseProtocolRow.ProtocolNr)
		protocols = append(protocols, protocol)
	}

	for _, sequenceProtocolRow := range sequenceProtocolRows {
		protocol := id.NewProtocolID(testVersionID, sequenceProtocolRow.ProtocolNr)
		protocols = append(protocols, protocol)
	}

	return protocols, s.Commit()
}

func (prSQL protocolsSQL) GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error) {
	// TODO Maybe just remove this? Who even uses this?
	s := prSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return test.CaseExecutionProtocol{}, err
	}
	row := caseProtocolRow{}

	testCaseRow, ex, err := getCaseRowByTestID(s, protocolID.TestVersionID.TestID)
	if err != nil || !ex {
		return test.CaseExecutionProtocol{}, err
	}
	_, err = s.Table(caseProtocolTable).Where("project_row_id = ?", testCaseRow.ProjectID).Where("test_case = ?", testCaseRow.ID).Where("test_version = ?", protocolID.TestVersionID.TestVersion()).Where("protocol_nr = ?", protocolID.Protocol()).Get(&row)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return test.CaseExecutionProtocol{}, err
	}

	caseProtocol, err := buildCaseProtocol(s, row)
	if err != nil {
		return test.CaseExecutionProtocol{}, err
	}
	return caseProtocol, s.Commit()
}

func (prSQL protocolsSQL) GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error) {
	// TODO Maybe just remove this? Who even uses this?
	s := prSQL.e.NewSession()
	defer s.Close()

	if err := s.Begin(); err != nil {
		return test.SequenceExecutionProtocol{}, err
	}
	row := sequenceProtocolRow{}

	testSequenceRow, ex, err := getSequenceRowByTestID(s, protocolID.TestVersionID.TestID)
	if err != nil || !ex {
		return test.SequenceExecutionProtocol{}, err
	}
	_, err = s.Table(caseProtocolTable).Where("project_row_id = ?", testSequenceRow.ProjectID).Where("test_sequence = ?", testSequenceRow.ID).Where("protocol_nr = ?", protocolID.Protocol()).Where("test_version = ?", protocolID.TestVersionID.TestVersion()).Get(&row)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return test.SequenceExecutionProtocol{}, err
	}

	sequenceProtocol, err := buildSequenceProtocol(s, row)
	if err != nil {
		return test.SequenceExecutionProtocol{}, err
	}
	return sequenceProtocol, s.Commit()
}
