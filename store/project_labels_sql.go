// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"fmt"

	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

type projectLabelRow struct {
	ID          int64
	ProjectID   int64
	Name        string
	Description string
	Color       string
}

func labelFromRow(plr projectLabelRow) project.Label {
	return project.Label{
		Name:        plr.Name,
		Description: plr.Description,
		Color:       plr.Color,
	}
}

func rowFromProjectLabel(pl project.Label) projectLabelRow {
	return projectLabelRow{
		Name:        pl.Name,
		Description: pl.Description,
		Color:       pl.Color,
	}
}

func saveProjectLabels(s xorm.Interface, prID int64, npls []project.Label) error {
	oplrs, err := listProjectLabelRows(s, prID)
	if err != nil {
		return err
	}

	var nplrs []projectLabelRow
	for _, npl := range npls {
		nplr := rowFromProjectLabel(npl)
		nplr.ProjectID = prID
		nplrs = append(nplrs, nplr)
	}

	dplrs := projectLabelRowSetMinus(oplrs, nplrs)
	err = deleteProjectLabelRows(s, dplrs...)
	if err != nil {
		return err
	}

	uplrs := projectLabelRowCutSet(oplrs, nplrs)
	err = updateProjectLabelRows(s, uplrs...)
	if err != nil {
		return err
	}

	iplrs := projectLabelRowSetMinus(nplrs, oplrs)
	err = insertProjectLabelRows(s, iplrs...)

	return err
}

func insertProjectLabelRows(s xorm.Interface, plr ...projectLabelRow) error {
	_, err := s.Table(projectLabelTable).Insert(&plr)
	return err
}

func updateProjectLabelRows(s xorm.Interface, plrs ...projectLabelRow) error {
	for _, plr := range plrs {
		err := updateProjectLabelRow(s, &plr)
		if err != nil {
			return err
		}
	}

	return nil
}

func updateProjectLabelRow(s xorm.Interface, plr *projectLabelRow) error {
	aff, err := s.Table(projectLabelTable).ID(plr.ID).Update(plr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteProjectLabelRows(s xorm.Interface, plrs ...projectLabelRow) error {
	var ids []int64
	for _, plr := range plrs {
		ids = append(ids, plr.ID)
	}

	_, err := s.Table(projectLabelTable).In(idField, ids).Delete(&projectLabelRow{})
	return err
}

func listProjectLabels(s xorm.Interface, prID int64) ([]project.Label, error) {
	plrs, err := listProjectLabelRows(s, prID)

	var pls []project.Label
	for _, plr := range plrs {
		pl := labelFromRow(plr)
		pls = append(pls, pl)
	}

	return pls, err
}

func listProjectLabelRows(s xorm.Interface, prID int64) ([]projectLabelRow, error) {
	var plrs []projectLabelRow
	err := s.Table(projectLabelTable).Asc(nameField).Find(&plrs, &projectLabelRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}

	return plrs, nil
}

func lookupProjectLabelRowIDs(s xorm.Interface, prID int64, names ...string) ([]int64, error) {
	var ids []int64
	err := s.Table(projectLabelTable).Distinct(idField).In(nameField, names).In(projectField, prID).Find(&ids)
	if err != nil {
		return nil, err
	}

	return ids, nil
}

func projectLabelRowSetMinus(s1, s2 []projectLabelRow) []projectLabelRow {
	var rs []projectLabelRow
	for _, plr1 := range s1 {
		if !projectLabelRowSetContains(s2, plr1) {
			rs = append(rs, plr1)
		}
	}

	return rs
}

func projectLabelRowCutSet(s1, s2 []projectLabelRow) []projectLabelRow {
	var rs []projectLabelRow
	for _, plr1 := range s1 {
		for _, plr2 := range s2 {
			if plr1.Name == plr2.Name {
				rplr := plr2
				rplr.ID = plr1.ID
				rs = append(rs, rplr)
			}
		}
	}

	return rs
}

func projectLabelRowSetContains(plrs []projectLabelRow, e projectLabelRow) bool {
	for _, plr := range plrs {
		if plr.Name == e.Name {
			return true
		}
	}

	return false
}
