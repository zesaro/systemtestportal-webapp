// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"github.com/go-xorm/xorm"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

type sequenceVersionTesterRow struct {
	ID                  int64
	TestSequenceVersion int64
	UserID              int64
}

func saveTestersForSequenceVersion(s xorm.Interface, svr sequenceVersionRow, nsvts map[string]*user.User) error {
	var uIDs []id.ActorID
	for _, nsvt := range nsvts {
		uIDs = append(uIDs, nsvt.ID())
	}

	urIDs, err := lookupUserRowIDs(s, uIDs...)
	if err != nil {
		return err
	}

	var svtrs []sequenceVersionTesterRow
	for _, urID := range urIDs {
		svtr := sequenceVersionTesterRow{TestSequenceVersion: svr.ID, UserID: urID}
		svtrs = append(svtrs, svtr)
	}

	err = insertSequenceVersionTesterRows(s, svtrs...)
	return err
}

func insertSequenceVersionTesterRows(s xorm.Interface, svtrs ...sequenceVersionTesterRow) error {
	_, err := s.Table(sequenceVersionTesterTable).Insert(&svtrs)
	return err
}

func listTestersForSequenceVersion(s xorm.Interface, svrID int64) (map[string]*user.User, error) {
	trs, err := listUserRowsForSequenceVersion(s, svrID)

	ts := make(map[string]*user.User)
	for _, tr := range trs {
		t := userFromRow(tr)
		ts[t.Name] = t
	}

	return ts, err
}

func listUserRowsForSequenceVersion(s xorm.Interface, svrID int64) ([]userRow, error) {
	var uIDs []int64
	err := s.Table(sequenceVersionTesterTable).Distinct(userField).In(sequenceVersionField, svrID).Find(&uIDs)
	if err != nil {
		return nil, err
	}

	var trs []userRow
	err = s.Table(userTable).In(idField, uIDs).Find(&trs)
	if err != nil {
		return nil, err
	}

	return trs, nil
}
