// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"errors"
	"fmt"
	"sort"
	"strings"

	"github.com/go-xorm/xorm"
	"github.com/hashicorp/go-multierror"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

const (
	errorNoCase = "no test case with name %#v"
)

type casesSQL struct {
	e *xorm.Engine
}

type caseRow struct {
	ID        int64
	ProjectID int64
	Name      string
}

func testCaseFromRow(r caseRow) *test.Case {
	return &test.Case{
		Name: r.Name,
	}
}

func rowFromTestCase(tc *test.Case) caseRow {
	return caseRow{
		Name: tc.Name,
	}
}

func (csSQL casesSQL) List(pID id.ProjectID) ([]*test.Case, error) {
	s := csSQL.e.NewSession()
	defer s.Close()

	err := s.Begin()
	if err != nil {
		return nil, err
	}

	tcs, err := getCases(s, pID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, err
	}

	err = s.Commit()
	if err != nil {
		return nil, err
	}

	sortCases(tcs)

	return tcs, nil
}

func (csSQL casesSQL) Add(testCase *test.Case) error {
	s := csSQL.e.NewSession()
	defer s.Close()

	err := s.Begin()
	if err != nil {
		return err
	}

	err = saveCase(s, testCase)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (csSQL casesSQL) Rename(old, new id.TestID) error {
	s := csSQL.e.NewSession()
	defer s.Close()

	err := s.Begin()
	if err != nil {
		return err
	}

	err = renameCase(s, old, new)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (csSQL casesSQL) Get(tID id.TestID) (*test.Case, bool, error) {
	s := csSQL.e.NewSession()
	defer s.Close()

	err := s.Begin()
	if err != nil {
		return nil, false, err
	}

	tc, ex, err := getCase(s, tID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return nil, false, err
	}

	err = s.Commit()
	if err != nil {
		return nil, false, err
	}

	return tc, ex, nil
}

func (csSQL casesSQL) Delete(tID id.TestID) error {
	s := csSQL.e.NewSession()
	defer s.Close()

	err := s.Begin()
	if err != nil {
		return err
	}

	err = deleteCase(s, tID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return err
	}

	return s.Commit()
}

func (csSQL casesSQL) Exists(tID id.TestID) (bool, error) {
	s := csSQL.e.NewSession()
	defer s.Close()

	err := s.Begin()
	if err != nil {
		return false, err
	}

	_, ex, err := getCaseRowID(s, tID)
	if err != nil {
		err = multierror.Append(err, s.Rollback())
		return false, err
	}

	return ex, s.Commit()
}

func saveCase(s xorm.Interface, tc *test.Case) error {
	prID, err := lookupProjectRowID(s, tc.Project)
	if err != nil {
		return err
	}

	ocr, ex, err := getCaseRow(s, prID, tc.Name)
	if err != nil {
		return err
	}

	ncr := rowFromTestCase(tc)
	ncr.ProjectID = prID
	if !ex {
		err = insertCaseRow(s, &ncr)
	} else {
		ncr.ID = ocr.ID
		err = updateCaseRow(s, &ncr)
	}

	if err != nil {
		return err
	}

	err = saveCaseLabels(s, ncr, tc.Labels)
	if err != nil {
		return err
	}

	return saveVersions(s, ncr, tc.TestCaseVersions)
}

func renameCase(s xorm.Interface, old, new id.TestID) error {
	if old.ProjectID != new.ProjectID {
		return errors.New(`unsupported operation: test cases currently can not be moved between projects`)
	}

	prID, err := lookupProjectRowID(s, old.ProjectID)
	if err != nil {
		return err
	}

	ocr, ex, err := getCaseRow(s, prID, old.Test())
	if err != nil {
		return err
	}

	if !ex {
		return fmt.Errorf(errorNoCase, old.Test())
	}

	ncr := caseRow{ID: ocr.ID, ProjectID: ocr.ProjectID, Name: new.Test()}
	return updateCaseRow(s, &ncr)
}

func insertCaseRow(s xorm.Interface, cr *caseRow) error {
	_, err := s.Table(caseTable).Insert(cr)
	return err
}

func updateCaseRow(s xorm.Interface, cr *caseRow) error {
	aff, err := s.Table(caseTable).ID(cr.ID).Update(cr)
	if err != nil {
		return err
	}

	if aff != 1 {
		return fmt.Errorf(errorNoAffectedRows, aff)
	}

	return nil
}

func deleteCase(s xorm.Interface, tID id.TestID) error {
	crID, err := lookupCaseRowID(s, tID)
	if err != nil {
		return err
	}

	return deleteCaseVersionRows(s, crID)
}

func getCases(s xorm.Interface, pID id.ProjectID) ([]*test.Case, error) {
	prID, err := lookupProjectRowID(s, pID)
	if err != nil {
		return nil, err
	}

	csr, err := listCaseRows(s, prID)
	if err != nil {
		return nil, err
	}

	var cs []*test.Case

	for _, cr := range csr {
		c, ex, err := buildCase(s, pID, cr)
		if err != nil {
			return nil, err
		}

		if ex {
			cs = append(cs, c)
		}
	}

	// sort the cases
	sortCases(cs)

	return cs, nil
}

func getCase(s xorm.Interface, tID id.TestID) (*test.Case, bool, error) {
	prID, err := lookupProjectRowID(s, tID.ProjectID)
	if err != nil {
		return nil, false, err
	}

	cr, ex, err := getCaseRow(s, prID, tID.Test())
	if err != nil {
		return nil, false, err
	}

	if !ex {
		return nil, false, nil
	}

	c, ex, err := buildCase(s, tID.ProjectID, cr)
	if err != nil {
		return nil, false, err
	}

	if !ex {
		return nil, false, nil
	}

	return c, true, nil
}

func buildCase(s xorm.Interface, pID id.ProjectID, cr caseRow) (*test.Case, bool, error) {
	var err error

	c := testCaseFromRow(cr)
	c.Project = pID

	c.TestCaseVersions, err = listCaseVersions(s, cr.ID)
	if err != nil {
		return nil, false, err
	}

	if len(c.TestCaseVersions) == 0 {
		return nil, false, nil
	}

	for i := range c.TestCaseVersions {
		c.TestCaseVersions[i].Case = c.ID()
	}

	c.Labels, err = listCaseLabels(s, cr)
	if err != nil {
		return nil, false, err
	}

	return c, true, nil
}

func lookupCaseRowID(s xorm.Interface, tID id.TestID) (int64, error) {
	crID, ex, err := getCaseRowID(s, tID)

	if err != nil {
		return 0, err
	}

	if !ex {
		return 0, fmt.Errorf(errorNoCase, tID.Test())
	}

	return crID, nil
}

func getCaseRowID(s xorm.Interface, tID id.TestID) (int64, bool, error) {
	row, ex, err := getCaseRowByTestID(s, tID)
	return row.ID, ex, err
}

func getCaseRowByTestID(s xorm.Interface, tID id.TestID) (caseRow, bool, error) {
	prID, err := lookupProjectRowID(s, tID.ProjectID)
	if err != nil {
		return caseRow{}, false, err
	}

	cr, ex, err := getCaseRow(s, prID, tID.Test())
	return cr, ex, err
}

func listCaseRows(s xorm.Interface, prID int64) ([]caseRow, error) {
	var csr []caseRow
	err := s.Table(caseTable).Asc(nameField).Find(&csr, &caseRow{ProjectID: prID})
	if err != nil {
		return nil, err
	}

	return csr, nil
}

func getCaseRow(s xorm.Interface, prID int64, name string) (caseRow, bool, error) {
	cr := caseRow{ProjectID: prID, Name: name}
	ex, err := s.Table(caseTable).Get(&cr)
	if err != nil {
		return caseRow{}, false, err
	}

	if !ex {
		return caseRow{}, false, nil
	}

	return cr, true, nil
}

func sortCases(cs []*test.Case) {
	sort.Slice(cs, func(i, j int) bool {
		cmp := strings.Compare(cs[i].Name, cs[j].Name)
		switch cmp {
		case 0:
			return false
		case 1:
			return false
		case -1:
			return true
		default:
			return false
		}
	})
}
