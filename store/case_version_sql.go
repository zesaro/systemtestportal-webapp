// This file is part of SystemTestPortal.
// Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
//
// SystemTestPortal is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SystemTestPortal is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.



package store

import (
	"time"

	"github.com/go-xorm/xorm"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

type caseVersionRow struct {
	ID          int64
	TestCaseID  int64
	VersionNr   int
	Description string
	Duration    time.Duration

	Message      string
	IsMinor      bool
	CreationDate time.Time
}

func caseVersionFromRow(r caseVersionRow) test.CaseVersion {
	return test.CaseVersion{
		VersionNr:   r.VersionNr,
		Description: r.Description,
		Duration: duration.Duration{
			Duration: r.Duration,
		},

		Message:      r.Message,
		IsMinor:      r.IsMinor,
		CreationDate: r.CreationDate,
	}
}

func rowFromCaseVersion(v test.CaseVersion) caseVersionRow {
	return caseVersionRow{
		VersionNr:   v.VersionNr,
		Description: v.Description,
		Duration:    v.Duration.Duration,

		Message:      v.Message,
		IsMinor:      v.IsMinor,
		CreationDate: v.CreationDate,
	}
}

func saveVersions(s xorm.Interface, cr caseRow, nvs []test.CaseVersion) error {
	ovrs, err := listCaseVersionRows(s, cr.ID)
	if err != nil {
		return err
	}

	// We only need to save versions not already in the database. We can compute the length difference of the slices to
	// determine the number of such elements.
	//
	// Due to the ordering of versions, we can just take the number of elements from the front of the version slice
	vtsc := len(nvs) - len(ovrs)
	for _, v := range nvs[:vtsc] {
		err := saveVersion(s, cr, v)
		if err != nil {
			return err
		}
	}

	return nil
}

func saveVersion(s xorm.Interface, cr caseRow, v test.CaseVersion) error {
	// We know that the version to save is not already present in the database
	vr := rowFromCaseVersion(v)
	vr.TestCaseID = cr.ID

	err := insertVersion(s, &vr)
	if err != nil {
		return err
	}

	err = saveSteps(s, vr.ID, v.Steps)
	if err != nil {
		return err
	}

	err = savePreconditionsCase(s, vr.ID, v.Preconditions)
	if err != nil {
		return err
	}

	err = saveSUTVersionsForCaseVersion(s, cr.ProjectID, vr.ID, v.Versions)
	if err != nil {
		return err
	}

	err = saveTestersForCaseVersion(s, vr, v.Tester)
	return err
}

func insertVersion(s xorm.Interface, vr *caseVersionRow) error {
	_, err := s.Table(caseVersionTable).Insert(vr)
	return err
}

func deleteCaseVersionRows(s xorm.Interface, crID int64) error {
	_, err := s.Table(caseVersionTable).Delete(&caseVersionRow{TestCaseID: crID})
	return err
}

func listCaseVersions(s xorm.Interface, crID int64) ([]test.CaseVersion, error) {
	cvrs, err := listCaseVersionRows(s, crID)
	if err != nil {
		return nil, err
	}

	var cvs []test.CaseVersion
	for _, r := range cvrs {
		var v test.CaseVersion
		v, err = buildCaseVersion(s, r)
		if err != nil {
			return nil, err
		}

		cvs = append(cvs, v)
	}

	return cvs, nil
}

func buildCaseVersion(s xorm.Interface, vr caseVersionRow) (test.CaseVersion, error) {
	var err error
	v := caseVersionFromRow(vr)

	v.Steps, err = listSteps(s, vr.ID)
	if err != nil {
		return test.CaseVersion{}, err
	}

	v.Preconditions, err = getPreconditionsCase(s, vr.ID)
	if err != nil {
		return test.CaseVersion{}, err
	}
	v.Versions, err = listSUTVersionsForCaseVersion(s, vr.ID)
	if err != nil {
		return test.CaseVersion{}, err
	}

	v.Tester, err = listTestersForCaseVersion(s, vr.ID)
	if err != nil {
		return test.CaseVersion{}, err
	}

	return v, nil
}

func listCaseVersionRows(s xorm.Interface, crID int64) ([]caseVersionRow, error) {
	var rows []caseVersionRow

	err := s.Table(caseVersionTable).Desc(versionNumberField).Find(&rows, &caseVersionRow{TestCaseID: crID})
	if err != nil {
		return nil, err
	}

	return rows, nil
}
