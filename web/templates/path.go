/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package templates

// This const contains the paths of the template files
const (
	// Static
	Header = "static/header"
	Navbar = "static/navbar"
	Footer = "static/footer"
	About  = "static/about"

	ContentSidebar   = "static/content-sidebar"
	ContentNoSidebar = "static/content-no-sidebar"

	// Explore
	MenuExplore     = "explore/menu-explore"
	ExploreUsers    = "explore/list-users"
	ExploreProjects = "explore/list-projects"
	ExploreGroups   = "explore/list-groups"

	// Modals
	SignIn            = "modal/sign-in"
	TestStepEdit      = "modal/teststep-edit"
	ManageVersions    = "modal/manage-versions"
	TestCaseSelection = "modal/testcase-selection"
	ErrorModal        = "modal/error"
	ManageLabels      = "modal/manage-labels"
	WrongFile         = "modal/wrong-file"
	DeletePopUp       = "modal/delete-pop-up"
	ExecutionAbortModal = "modal/execution-abort"

	// User
	NewAccount = "creation/new-account"

	// TODOs
	Todo = "todo/todo"

	// Project
	ContentProjectTabs = "tab-project/content-project-tabs"
	NewProject         = "creation/new-project"

	// Execution
	ExecutionStartPage = "tab-project/execution/startpage"
	ExecutionStep      = "tab-project/execution/step"
	ExecutionSummary   = "tab-project/execution/summary"

	// Dashboard
	Dashboard          = "tab-project/dashboard/dashboard"
	DashboardSequences = "tab-project/dashboard/dashboard_sequences"

	// TestCases
	NewTestCase     = "tab-project/testcases/new"
	TestCasesList   = "tab-project/testcases/list"
	ShowTestCase    = "tab-project/testcases/show"
	EditTestCase    = "tab-project/testcases/edit"
	TestCaseHistory = "tab-project/testcases/history"
	PrintTestCases  = "tab-project/testcases/list-print"
	PrintCase       = "tab-project/testcases/show-print"

	// TestSequences
	NewTestSequence     = "tab-project/testsequences/new"
	TestSequencesList   = "tab-project/testsequences/list"
	ShowTestSequence    = "tab-project/testsequences/show"
	EditTestSequence    = "tab-project/testsequences/edit"
	TestSequenceHistory = "tab-project/testsequences/history"
	PrintSequences      = "tab-project/testsequences/list-print"
	PrintSequence       = "tab-project/testsequences/show-print"

	// Protocols
	ProtocolsList         = "tab-project/protocols/list"
	TestCaseProtocols     = "tab-project/protocols/testcase"
	TestSequenceProtocols = "tab-project/protocols/testsequence"
	PrintProtocols        = "tab-project/protocols/list"
	PrintCaseProtocol     = "tab-project/protocols/testcase-print"
	PrintSequenceProtocol = "tab-project/protocols/testsequence-print"

	// Settings
	Settings           = "tab-project/settings/settings"
	SettingsPermission = "tab-project/settings/permission-settings"
	SettingsProject    = "tab-project/settings/project-settings"

	// Members
	Members      = "tab-project/members/members"
	AssignMember = "modal/add-member"
	RemoveMember = "modal/remove-member"

	// Group
	NewGroup = "creation/new-group"

	// Comments
	Comments = "tab-project/comments"
)
