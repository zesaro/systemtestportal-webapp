/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package middleware

import (
	"fmt"
	"net/http"

	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
)

// TestCaseKey is used to retrieve a testcase from a request context.
const TestCaseKey = "testcase"

const (
	nonExistentTestCaseTitle = "The testcase you requested doesn't exist."
	nonExistentTestCase      = "It seems that you requested a test case that doesn't exist anymore."
)

// TestCaseStore provides an interface for retrieving testcases
type TestCaseStore interface {
	// Get returns the test case with the given ID for the given project under the given container
	Get(caseID id.TestID) (*test.Case, bool, error)
}

// Testcase is a middleware that can retrieve a testcase from a request
// it requires the project as well as the container middleware to work.
func Testcase(store TestCaseStore) negroni.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
		tcName, err := getParam(r, TestCaseKey)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		pID, err := getProjectID(r.Context().Value(ProjectKey), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		tc, err := getTestcase(store, id.NewTestID(pID, tcName, true), r)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		AddToContext(r, TestCaseKey, tc)
		next(w, r)
	}
}

func getProjectID(v interface{}, r *http.Request) (id.ProjectID, error) {
	p, ok := v.(*project.Project)
	if ok {
		return p.ID(), nil
	}
	return id.ProjectID{}, internalError(
		fmt.Sprintf("Given project is not a project, but <%+v>", v),
		nil,
		r,
	)
}

func getTestcase(store TestCaseStore, tcID id.TestID, r *http.Request) (*test.Case, error) {
	tc, ok, err := store.Get(tcID)
	if !ok {
		return nil,
			errors.ConstructStd(http.StatusNotFound, nonExistentTestCaseTitle, nonExistentTestCase, r).
				WithLogf("Client request non-existent testcase %+v.", tcID).
				WithStackTrace(1).
				Finish()
	} else if err != nil {
		return nil, internalError(
			fmt.Sprintf("Unable to load testcase with id: %+v.", tcID),
			err,
			r,
		)
	}
	return tc, nil
}
