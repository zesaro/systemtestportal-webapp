/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package update

import (
	"encoding/json"
	"net/http"
	"reflect"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// testCaseEditInput is similar to testCaseInput
// but also contains information for the change
// such as the commit message and whether the change is minor.
type testCaseEditInput struct {
	IsMinor            bool
	InputCommitMessage string
	Data               creation.TestCaseInput
}

// CasePut handles put requests that update existing cases.
func CasePut(t handler.TestCaseUpdater, caseChecker id.TestExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).EditCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}
		newTc, err := updateTestCase(r, t, caseChecker, *c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		httputil.SetHeaderValue(w, httputil.NewName, newTc.ID().Test())
		cm := handler.GetComments(r)
		w.WriteHeader(http.StatusCreated)

		display.ShowCase(c.Project, &newTc, cm, w, r)
	}
}

// getTestCaseEditingInput reads the input from a client request targeted
// at editing a testcase.
func getTestCaseEditingInput(r *http.Request) (*testCaseEditInput, error) {
	input := testCaseEditInput{}
	if err := json.NewDecoder(r.Body).Decode(&input); err != nil {
		return nil, err
	}
	return &input, nil
}

// createNewTestCaseVersion creates a new test case version with the user input and appends
// the new version to the list of test case versions in a test case.
// Returns the testcase with the new version if the input is valid. Otherwise an error is returned
func createNewTestCaseVersion(input testCaseEditInput, tc test.Case) (test.Case, error) {
	changes := input.Data

	dur, err := duration.GetDuration(changes.InputHours, changes.InputMinutes)
	if err != nil {
		return test.Case{}, err
	}

	st := creation.ConstructSteps(changes.InputSteps)

	curVer := len(tc.TestCaseVersions)
	if curVer <= 0 {
		return test.Case{}, handler.InvalidTCVersion()
	}
	preconditions := handlePreconditions(changes.InputTestCasePreconditions)
	tcv := test.NewTestCaseVersion(
		curVer+1,
		input.IsMinor,
		input.InputCommitMessage,
		changes.InputTestCaseDescription,
		preconditions,
		changes.InputTestCaseSUTVersions,
		dur,
		tc.ID(),
	)
	tcv.Steps = st

	// Insert at beginning of slice
	tc.TestCaseVersions = append([]test.CaseVersion{tcv}, tc.TestCaseVersions...)

	return tc, nil
}

func handlePreconditions(input []string) []test.Precondition {
	preconditions := make([]test.Precondition, 0)

	for _, prec := range input {
		preconditions = append(preconditions, test.Precondition{
			Content: prec,
		})
	}
	return preconditions
}

// Error messages of updating a test case
const (
	errCanNotUpdateTestCaseTitle = "Couldn't execute your edit as requested."
	errCanNotUpdateTestCase      = "We were unable to apply the changes you requested for your " +
		"testcase. If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
	errCanNotRenameTestCaseTitle = "Unable to rename test case"
	errCanNotRenameTestCase      = "We were unable to successfully rename your test case. " +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

// updateTestCaseStore applies an update to given testcase (oldTCID) using the new data (tc)
// This will access the storage and finally write the changes.
func updateTestCaseStore(r *http.Request, t handler.TestCaseUpdater, tc test.Case, oldTCID *id.TestID) error {
	if tc.ID() != *oldTCID {
		if err := t.Rename(*oldTCID, tc.ID()); err != nil {
			return errors.ConstructStd(http.StatusInternalServerError,
				errCanNotRenameTestCaseTitle, errCanNotRenameTestCase, r).
				WithLogf("Couldn't rename testcase %v", oldTCID).
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Finish()
		}
	}

	if err := t.Add(&tc); err != nil {
		return errors.ConstructStd(http.StatusInternalServerError,
			errCanNotUpdateTestCaseTitle, errCanNotUpdateTestCase, r).
			WithLogf("Couldn't update testcase %v.", tc).
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Finish()
	}

	return nil
}

// updateTestCase updates a case using the input from a request.
// If only the test case name was changed, no new version of a test case
// is created. Instead only the name is changed in the store.
// Returns the updated test case or an error if the input is invalid or
// the test case could not be updated.

func updateTestCase(r *http.Request, t handler.TestCaseUpdater,
	caseChecker id.TestExistenceChecker, tc test.Case) (test.Case, error) {
	input, err := getTestCaseEditingInput(r)
	if err != nil {
		return test.Case{}, err
	}

	// Save the id of the old case to delete the old entry from the store
	var oldTCID = id.NewTestID(tc.ID().ProjectID, tc.ID().Test(), tc.ID().IsCase())

	var nameChanged = tc.Name != input.Data.InputTestCaseName

	// If name has changed -> Rename and validate the name
	if nameChanged {
		tc, err = handleCaseRename(tc, input, caseChecker, &oldTCID)
		if err != nil {
			return test.Case{}, err
		}
	}

	var metaDataChanged = testCaseMetadataChanged(&input.Data, &tc)

	// If metadata of test case changed, create a new test case version
	if metaDataChanged {
		tc, err = createNewTestCaseVersion(*input, tc)
		if err != nil {
			return test.Case{}, err
		}
	}

	// Update test case store
	if nameChanged || metaDataChanged {
		if err = updateTestCaseStore(r, t, tc, &oldTCID); err != nil {
			return test.Case{}, err
		}
	}

	return tc, nil
}

// handleCaseRename renames and validates the case and then updates the protocol store.
// The function returns the test case and an error if any occurred.
func handleCaseRename(tc test.Case, input *testCaseEditInput, caseChecker id.TestExistenceChecker,
	oldTCID *id.TestID) (test.Case, error) {

	// Rename test case
	tc.Rename(input.Data.InputTestCaseName)

	// Validate the id of the new test case
	if err := tc.ID().Validate(caseChecker); err != nil {
		return test.Case{}, err
	}

	return tc, nil
}

// testCaseMetadataChanged checks whether the test case to update (input)
// has different metadata than the newest version of the existing
// test case (tc). Returns true if there are changes.
// Else return false.
func testCaseMetadataChanged(input *creation.TestCaseInput, tc *test.Case) bool {
	testCaseVersion := tc.TestCaseVersions[0]
	if input.InputTestCaseDescription != testCaseVersion.Description {
		return true
	}
	if !reflect.DeepEqual(handlePreconditions(input.InputTestCasePreconditions), testCaseVersion.Preconditions) {
		return true
	}
	if sutVersionsChanged(input.InputTestCaseSUTVersions, testCaseVersion.Versions) {
		return true
	}
	if timeChanged(input.InputHours, input.InputMinutes, testCaseVersion.Duration) {
		return true
	}
	if stepsChanged(input.InputSteps, testCaseVersion.Steps) {
		return true
	}
	return false
}

// sutVariantsChanged checks whether the variants from the input have changed compared to the
// variants of the existing test case. Returns true if they have changed. Else return false.
func sutVersionsChanged(inputSUTVersions map[string]*project.Version, tcSUTVersions map[string]*project.Version) bool {
	if len(tcSUTVersions) != len(inputSUTVersions) {
		return true
	}
	for inputVersionKey, inputVersion := range inputSUTVersions {
		if tcVar, ok := tcSUTVersions[inputVersionKey]; !ok || tcVar.Name != inputVersion.Name {
			return true
		}
		// Amount of versions in a variant changed
		if len(tcSUTVersions[inputVersionKey].Variants) != len(inputVersion.Variants) {
			return true
		}
		// Check if names of versions changed
		for inputVariantIndex, inputVariant := range inputVersion.Variants {
			if inputVariant.Name != tcSUTVersions[inputVersionKey].Variants[inputVariantIndex].Name {
				return true
			}
		}
	}
	return false
}

// timeChanged checks whether the duration of the input have changed compared to
// the duration of the existing test case. Returns true if they have changed. Else return false.
func timeChanged(hours, minutes int, tcDur duration.Duration) bool {
	h, m, _ := tcDur.ConvertToHHMMSS()
	if h != hours || m != minutes {
		return true
	}
	return false
}

// stepsChanged checks whether the steps of the input have changed compared to
// the steps of the existing test case. Returns true if they have changed. Else return false.
func stepsChanged(inputSteps []creation.InputStep, tcSteps []test.Step) bool {
	if len(inputSteps) != len(tcSteps) {
		return true
	}

	for tcStepIndex, tcStep := range tcSteps {
		if tcStep.Action != inputSteps[tcStepIndex].Actual {
			return true
		}
		if tcStep.ExpectedResult != inputSteps[tcStepIndex].Expected {
			return true
		}
	}

	return false
}
