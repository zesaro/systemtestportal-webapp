/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// This const contains the text for the error message of
// a post request to update the labels of a project,
// a test case or a test sequence
const (
	errCouldNotDecodeLabelsTitle = "Couldn't save labels."
	errCouldNotDecodeLabels      = "We were unable to decode the change to labels " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

// CaseLabelsGet returns the labels of a test case as JSON
func CaseLabelsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Case == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	b, err := json.Marshal(c.Case.Labels)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	_, err = w.Write(b)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
}

// CaseLabelsPost handles the request to update the
// labels of a test case
func CaseLabelsPost(tcs handler.TestCaseAdder) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if err := json.NewDecoder(r.Body).Decode(&c.Case.Labels); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeLabelsTitle, errCouldNotDecodeLabels, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		if err := tcs.Add(c.Case); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

// SequenceLabelsGet returns the labels of a test sequence as JSON
func SequenceLabelsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Sequence == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	b, err := json.Marshal(c.Sequence.Labels)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	_, err = w.Write(b)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
}

// SequenceLabelsPost handles the request to update the
// labels of a test sequence
func SequenceLabelsPost(tss handler.TestSequenceAdder) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if err := json.NewDecoder(r.Body).Decode(&c.Sequence.Labels); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeLabelsTitle, errCouldNotDecodeLabels, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		if err := tss.Add(c.Sequence); err != nil {
			errors.Handle(err, w, r)
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

// ProjectLabelsGet returns the labels of a project as JSON
func ProjectLabelsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil {
		errors.Handle(c.Err, w, r)
		return
	}

	if !c.Project.GetPermissions(c.User).DisplayProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	b, err := json.Marshal(c.Project.Labels)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	_, err = w.Write(b)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
}

// ProjectLabelsPost handles the request to update the
// labels of a project
func ProjectLabelsPost(ps handler.ProjectAdder) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if err := json.NewDecoder(r.Body).Decode(&c.Project.Labels); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeLabelsTitle, errCouldNotDecodeLabels, r).
				WithLog("Couldn't read labels from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		if err := ps.Add(c.Project); err != nil {
			errors.Handle(err, w, r)
			return
		}
		w.WriteHeader(http.StatusOK)
	}
}
