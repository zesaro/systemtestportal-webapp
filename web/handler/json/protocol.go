/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package json

import (
	"encoding/json"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// CaseProtocolsGet serves testcase protocols as json.
func CaseProtocolsGet(l handler.CaseProtocolLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		protocols, err := l.GetCaseExecutionProtocols(c.Case.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		jsonBytes, err := json.Marshal(protocols)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		_, err = w.Write(jsonBytes)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

// SequenceProtocolsGet serves testsequence protocols as json.
func SequenceProtocolsGet(l handler.SequenceProtocolLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		protocols, err := l.GetSequenceExecutionProtocols(c.Sequence.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		jsonBytes, err := json.Marshal(protocols)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		_, err = w.Write(jsonBytes)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}
