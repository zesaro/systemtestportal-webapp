/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
package display

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/dashboard"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// DashboardGet supplies a handler to display the dashboard page
func DashboardGet(l handler.CaseProtocolLister, caseLister handler.TestCaseLister) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		ctxtEntities := handler.GetContextEntities(request)
		// Get the Project
		if ctxtEntities.Project == nil {
			errors.Handle(ctxtEntities.Err, writer, request)
			return
		}

		if !ctxtEntities.Project.GetPermissions(ctxtEntities.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(request), writer, request)
			return
		}

		project := GetProject(ctxtEntities, writer, request)

		caseProtocolList := GetCaseProtocols(project, writer, request)

		cases := GetCases(caseLister, project)

		protocolMap := GetProtocolMap(caseProtocolList, l)

		dashboard := dashboard.NewDashboard(project, cases, protocolMap)

		// Fill the Datamap
		contexts := context.New().
			WithUserInformation(request).
			With(context.Project, project).
			With(context.Dashboard, dashboard)

		tmpl := getProjectDashboardFragment(request)
		handler.PrintTmpl(contexts, tmpl, writer, request)
	}
}

// GetProject returns the current project
func GetProject(ctxtEntities *handler.ContextEntities, writer http.ResponseWriter, request *http.Request) *project.Project {
	project, wasFound, err := store.GetProjectStore().Get(ctxtEntities.Project.ID())
	if !wasFound || err != nil {
		errors.Handle(err, writer, request)
	}
	return project
}

//GetCases returns all cases of the project
func GetCases(caseLister handler.TestCaseLister, project *project.Project) []*test.Case {
	cases, err := caseLister.List(project.ID())
	if err != nil {
		log.Println(err)
	}
	return cases
}

// GetProtocolMap returns a map of test id's and execution protocols
func GetProtocolMap(caseProtocolList []test.CaseExecutionProtocol, l handler.CaseProtocolLister) map[id.TestID][]test.CaseExecutionProtocol {
	protocolMap := make(map[id.TestID][]test.CaseExecutionProtocol)
	for _, caseProtocol := range caseProtocolList {
		if caseProtocol.TestVersion.IsCase() {
			protocolMap[caseProtocol.TestVersion.TestID], _ = l.GetCaseExecutionProtocols(caseProtocol.TestVersion.TestID)
		}
	}
	return protocolMap
}

// GetSequenceProtocols returns test case protocols of the project
func GetCaseProtocols(project *project.Project, writer http.ResponseWriter, request *http.Request) []test.CaseExecutionProtocol {
	caseProtocolList, err := store.GetProtocolStore().GetCaseExecutionProtocolsForProject(project.ID())
	if err != nil {
		errors.Handle(err, writer, request)
	}
	return caseProtocolList
}

func getProjectDashboardFragment(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return getTabDashboardFragment()
	}
	return getTabDashboardTree()
}

func getTabDashboardTree() *template.Template {
	return handler.GetNoSideBarTree().
		Append(templates.ContentProjectTabs).
		Append(templates.Dashboard).
		Get().Lookup(templates.HeaderDef)
}

func getTabDashboardFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.Dashboard).
		Get().Lookup(templates.TabContent)
}
