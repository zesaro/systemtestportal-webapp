/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestTestSequenceGet(t *testing.T) {
	wrongVersionParams := url.Values{}
	wrongVersionParams.Add(httputil.Version, "-1")
	nilCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProjectPrivate,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUserUnauthorized,
		},
	)

	tested := ShowSequenceGet
	handler.Suite(t,
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Nil as testsequence in context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.SimpleRequest(nilCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(nilCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Request with invalid version",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusBadRequest)),
			handler.SimpleRequest(ctx, http.MethodGet, wrongVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, wrongVersionParams),
		),
		handler.CreateTest("No member of private project",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusForbidden)),
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Valid request",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusOK)),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestCreateSequenceGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    handler.DummyUser,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
			middleware.UserKey:    nil,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUserUnauthorized,
			middleware.ProjectKey: handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return CreateSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return CreateSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return CreateSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return CreateSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestEditSequenceGet(t *testing.T) {
	wrongVersionParams := url.Values{}
	wrongVersionParams.Add(httputil.Version, "-1")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      nil,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUser,
		},
	)
	ctxUnauthorizedUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:         handler.DummyUserUnauthorized,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No project in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctxNoProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorizedUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid version number",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, wrongVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, wrongVersionParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.CaseListerMock{}
				return EditSequenceGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestHistorySequenceGet(t *testing.T) {
	nilCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: nil,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.ProjectKey:      handler.DummyProject,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:      handler.DummyProjectPrivate,
			middleware.TestSequenceKey: handler.DummyTestSequence,
			middleware.UserKey:         handler.DummyUserUnauthorized,
		},
	)

	tested := HistorySequenceGet
	handler.Suite(t,
		handler.CreateTest("Invalid context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(handler.EmptyCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Nil as testsequence in context",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusInternalServerError)),
			handler.SimpleRequest(nilCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(nilCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusForbidden)),
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Valid request",
			handler.ExpectResponse(tested, handler.HasStatus(http.StatusOK)),
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
