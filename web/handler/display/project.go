/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// CreateProjectGet is a handler for showing the screen for creating a new project
func CreateProjectGet(w http.ResponseWriter, r *http.Request) {
	contextEntities := handler.GetContextEntities(r)
	if contextEntities.User == nil {
		errors.Handle(contextEntities.Err, w, r)
		return
	}

	tmpl := getNewProjectTree()
	handler.PrintTmpl(context.New().WithUserInformation(r), tmpl, w, r)
}

// getNewProjectTree returns the new project template with all parent templates
func getNewProjectTree() *template.Template {
	return handler.GetNoSideBarTree().
		// New project tree
		Append(templates.NewProject).
		Get().Lookup(templates.HeaderDef)
}

// ProjectSettingsGet is a handler for showing the settings page of a project
func ProjectSettingsGet(w http.ResponseWriter, r *http.Request) {
	c := handler.GetContextEntities(r)
	if c.Project == nil || c.ContainerID == "" {
		errors.Handle(c.Err, w, r)
		return
	}

	// Owner always has the right to access the settings. This prevents the members from
	// locking themselves out from editing the project and the roles
	if !c.Project.IsOwner(c.User) && !c.Project.GetPermissions(c.User).EditProject {
		errors.Handle(handler.UnauthorizedAccess(r), w, r)
		return
	}

	tmpl := getProjectSettingsFragment(r)
	handler.PrintTmpl(context.New().
		WithUserInformation(r).
		With(context.Project, c.Project).
		With(context.DeleteProject, modal.ProjectDeleteMessage), tmpl, w, r)
}

func getProjectSettingsFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabSettingsFragment()
	}
	return getTabSettingsTree()
}

func getTabSettingsTree() *template.Template {
	return handler.GetNoSideBarTree().
		Append(templates.ContentProjectTabs).
		Append(templates.Settings).
		Append(templates.WrongFile).
		Append(templates.SettingsPermission).
		Append(templates.SettingsProject).
		Append(templates.DeletePopUp).
		Get().Lookup(templates.HeaderDef)
}

func getTabSettingsFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.Settings).
		Append(templates.WrongFile).
		Append(templates.SettingsPermission).
		Append(templates.SettingsProject).
		Append(templates.DeletePopUp).
		Get().Lookup(templates.TabContent)
}
