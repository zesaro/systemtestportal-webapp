/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/
package display

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/dashboard"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// DashboardGet supplies a handler to display the dashboard page
func Dashboard_Sequences_Get(l handler.SequenceProtocolLister, sequenceLister handler.TestSequenceLister) http.HandlerFunc {
	return func(writer http.ResponseWriter, request *http.Request) {
		tmpl := getProjectDashboardSequencesFragment(request)
		ctxtEntities := handler.GetContextEntities(request)
		// Get the Project
		if ctxtEntities.Project == nil {
			errors.Handle(ctxtEntities.Err, writer, request)
			return
		}
		project := GetProject(ctxtEntities, writer, request)

		sequenceProtocolList := GetSequenceProtocols(project, writer, request)

		sequences := GetSequences(sequenceLister, project)

		protocolMap := GetProtocolMap_Sequences(sequenceProtocolList, l)

		dashboard := dashboard.NewDashboard_Sequences(project, sequences, protocolMap)

		// Fill the Datamap
		contexts := context.New().
			WithUserInformation(request).
			With(context.Project, project).
			With(context.DashboardSequences, dashboard)

		handler.PrintTmpl(contexts, tmpl, writer, request)
	}
}

//GetCases returns all cases of the project
func GetSequences(sequenceLister handler.TestSequenceLister, project *project.Project) []*test.Sequence {
	sequences, err := sequenceLister.List(project.ID())
	if err != nil {
		log.Println(err)
	}

	if err != nil {
		log.Println(err)
	}

	return sequences
}

// GetProtocolMap returns a map of test id's and execution protocols
func GetProtocolMap_Sequences(sequenceProtocolList []test.SequenceExecutionProtocol, l handler.SequenceProtocolLister) map[id.TestID][]test.SequenceExecutionProtocol {
	protocolMap := make(map[id.TestID][]test.SequenceExecutionProtocol)
	for _, sequenceProtocol := range sequenceProtocolList {
		if !sequenceProtocol.TestVersion.IsCase() {
			protocolMap[sequenceProtocol.TestVersion.TestID], _ = l.GetSequenceExecutionProtocols(sequenceProtocol.TestVersion.TestID)
		}
	}
	return protocolMap
}

// GetSequenceProtocols returns test case protocols of the project
func GetSequenceProtocols(project *project.Project, writer http.ResponseWriter, request *http.Request) []test.SequenceExecutionProtocol {
	sequenceProtocolList, err := store.GetProtocolStore().GetSequenceExecutionProtocolsForProject(project.ID())
	if err != nil {
		errors.Handle(err, writer, request)
	}
	return sequenceProtocolList
}

func getProjectDashboardSequencesFragment(request *http.Request) *template.Template {
	if httputil.IsFragmentRequest(request) {
		return getTabDashboardSequencesFragment()
	}
	return getTabDashboardSequencesTree()
}

func getTabDashboardSequencesTree() *template.Template {
	return handler.GetNoSideBarTree().
		Append(templates.ContentProjectTabs).
		Append(templates.DashboardSequences).
		Get().Lookup(templates.HeaderDef)
}

func getTabDashboardSequencesFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.DashboardSequences).
		Get().Lookup(templates.TabContent)
}
