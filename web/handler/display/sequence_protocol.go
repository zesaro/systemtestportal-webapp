/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// SequenceProtocolsGet serves the page displaying testsequence protocols.
func SequenceProtocolsGet(caseProtocolLister handler.CaseProtocolLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Sequence == nil || c.SequenceProtocol == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		// Get the sequence version belonging to this protocol
		var seqVersion test.SequenceVersion
		for _, version := range c.Sequence.SequenceVersions {
			if version.VersionNr == c.SequenceProtocol.TestVersion.TestVersion() {
				seqVersion = version
				break
			}
		}

		// Get all case protocols of this sequence protocol
		var caseProtocols []test.CaseExecutionProtocol
		for _, caseProtocolID := range c.SequenceProtocol.CaseExecutionProtocols {
			protocol, err := caseProtocolLister.GetCaseExecutionProtocol(caseProtocolID)
			if err != nil {
				errors.Handle(
					errors.ConstructStd(http.StatusInternalServerError,
						errRetrievingProtocols, errRetrievingProtocolsMsg, r).
						WithLog("cannot retrieve case protocols for sequence").
						WithStackTrace(1).
						WithRequestDump(r).
						Finish(), w, r)
			}
			caseProtocols = append(caseProtocols, protocol)
		}

		selType := r.FormValue(httputil.ProtocolType)
		selCase := r.FormValue(httputil.SelectedProtocol)
		selSequence := r.FormValue(httputil.SelectedProtocol)

		tmpl := getTabSequenceProtocolTree()
		if httputil.IsFragmentRequest(r) {
			tmpl = getTabSequenceProtocolFragment()
		}

		ctx := context.New().
			WithUserInformation(r).
			With(context.SelectedTestCase, selCase).
			With(context.SelectedTestSequence, selSequence).
			With(context.SelectedType, selType).
			With(context.Project, c.Project).
			With(context.Protocol, c.SequenceProtocol).
			With(context.TestSequence, c.Sequence).
			With(context.TestSequenceVersion, seqVersion).
			With(context.CaseProtocols, caseProtocols)
		handler.PrintTmpl(ctx, tmpl, w, r)
	}
}

// getTabSequenceProtocolFragment returns only the protocol show test sequence tab template
func getTabSequenceProtocolFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestSequenceProtocols).
		Get().Lookup(templates.TabContent)
}

// getTabSequenceProtocolTree returns the protocol show test sequence tab template with all parent templates
func getTabSequenceProtocolTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test sequence protocols
		Append(templates.TestSequenceProtocols).
		Get().Lookup(templates.HeaderDef)
}
