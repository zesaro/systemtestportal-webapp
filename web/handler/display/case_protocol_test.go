/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package display

import (
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseProtocolsGet(t *testing.T) {
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.ProtocolKey: handler.DummyTestCaseExecutionProtocol,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProjectPrivate,
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.ProtocolKey: handler.DummyTestCaseExecutionProtocol,
		},
	)

	tested := CaseProtocolsGet
	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{
					Protocols: []test.CaseExecutionProtocol{
						{},
					},
				}
				return tested(m), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{
					Protocols: []test.CaseExecutionProtocol{
						{},
					},
				}
				return tested(m), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
				)
			},
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Protocol lister returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{
					Err: errors.New("error retrieving case protocol"),
					Protocols: []test.CaseExecutionProtocol{
						{},
					},
				}
				return tested(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				m := &handler.CaseProtocolListerMock{
					Protocols: []test.CaseExecutionProtocol{
						{},
					},
				}
				return tested(m), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
