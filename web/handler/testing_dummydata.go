/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"bytes"
	"encoding/json"
	"time"

	"log"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
)

// DummyProject contains a project for testing purposes
var DummyProject = &project.Project{
	Name:        "DuckDuckGo.com",
	Description: "Test the online search engine DuckDuckGo.com",
	Visibility:  visibility.Public,
	Owner:       "default",
	Versions: map[string]*project.Version{
		"Chrome": {
			Name: "Chrome",
			Variants: []project.Variant{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Firefox": {
			Name: "Firefox",
			Variants: []project.Variant{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Microsoft Edge": {
			Name: "Microsoft Edge",
			Variants: []project.Variant{
				0: {Name: "1.0.2"},
				1: {Name: "2.0.2"},
				2: {Name: "3.0.1"},
			},
		},
	},
	UserMembers: map[id.ActorID]project.UserMembership{
		id.ActorID(DummyUser.Name): *DummyMembership,
	},
	Roles:        DummyRoles,
	CreationDate: time.Now().UTC().Round(time.Second),
}

// DummyProjectInternal contains an internal project for testing purposes
var DummyProjectInternal = &project.Project{
	Name:        "DuckDuckGo.com",
	Description: "Test the online search engine DuckDuckGo.com",
	Visibility:  visibility.Internal,
	Owner:       "default",
	Versions: map[string]*project.Version{
		"Chrome": {
			Name: "Chrome",
			Variants: []project.Variant{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Firefox": {
			Name: "Firefox",
			Variants: []project.Variant{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Microsoft Edge": {
			Name: "Microsoft Edge",
			Variants: []project.Variant{
				0: {Name: "1.0.2"},
				1: {Name: "2.0.2"},
				2: {Name: "3.0.1"},
			},
		},
	},
	UserMembers: map[id.ActorID]project.UserMembership{
		id.ActorID(DummyUser.Name): *DummyMembership,
	},
	Roles:        DummyRoles,
	CreationDate: time.Now().UTC().Round(time.Second),
}

// DummyProjectPrivate contains a project for testing purposes
var DummyProjectPrivate = &project.Project{
	Name:        "DuckDuckGo.com",
	Description: "Test the online search engine DuckDuckGo.com",
	Visibility:  visibility.Private,
	Owner:       "default",
	Versions: map[string]*project.Version{
		"Chrome": {
			Name: "Chrome",
			Variants: []project.Variant{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Firefox": {
			Name: "Firefox",
			Variants: []project.Variant{
				0: {Name: "1.0.1"},
				1: {Name: "2.0.1"},
				2: {Name: "3.0.5"},
			},
		},
		"Microsoft Edge": {
			Name: "Microsoft Edge",
			Variants: []project.Variant{
				0: {Name: "1.0.2"},
				1: {Name: "2.0.2"},
				2: {Name: "3.0.1"},
			},
		},
	},
	UserMembers: map[id.ActorID]project.UserMembership{
		id.ActorID(DummyUser.Name): *DummyMembership,
	},
	Roles:        DummyRoles,
	CreationDate: time.Now().UTC().Round(time.Second),
}

// DummyTestCase is a test case for testing purposes
var DummyTestCase = &test.Case{
	Name: "Test Case 1",
	TestCaseVersions: []test.CaseVersion{
		{
			VersionNr:   1,
			Description: "First Test Case",
			Versions:    map[string]*project.Version{},
			Preconditions: []test.Precondition{
				test.Precondition{
					Id:          1,
					TestVersion: 1,
					Content:     "Test",
				},
			},
			Steps: []test.Step{
				{
					Index:          1,
					Action:         "Open the application",
					ExpectedResult: "The main screen should show",
				},
				{
					Index:          2,
					Action:         "Open the login page",
					ExpectedResult: "The login page should show",
				},
				{
					Index:          3,
					Action:         "Login with the test login",
					ExpectedResult: "The login should succeed",
				},
			},
		},
	},
	Project: DummyProject.ID(),
}

var DummyTestCaseExecutionProtocol = &test.CaseExecutionProtocol{
	TestVersion: id.NewTestVersionID(DummyTestCase.TestCaseVersions[0].ID().TestID, 1),
	ProtocolNr:  1,

	SUTVersion: "Desktop",
	SUTVariant: "v1018",

	ExecutionDate: time.Now().UTC().Round(time.Second),

	StepProtocols: []test.StepExecutionProtocol{
		{
			ObservedBehavior: "The search results are viewed.",
			Result:           test.Pass,
			NeededTime:       duration.NewDuration(0, 5, 0),
		},
		{
			ObservedBehavior: "The sidebar opens.",
			Result:           test.Pass,
			NeededTime:       duration.NewDuration(0, 5, 0),
		},
		{
			ObservedBehavior: "The icons didn't disappear.",
			Result:           test.Fail,
			NeededTime:       duration.NewDuration(0, 5, 0),
		},
	},

	Result:          test.Fail,
	OtherNeededTime: duration.NewDuration(0, 5, 0),
}

// DummyTestCaseSUTVersions is a test case for testing purposes
// that contains sut-versions
var DummyTestCaseSUTVersions = &test.Case{
	Name: "Test Case 1",
	TestCaseVersions: []test.CaseVersion{
		{
			Description: "First Test Case",
			Versions: map[string]*project.Version{
				"Chrome": {
					Name: "Chrome",
					Variants: []project.Variant{
						0: {Name: "1.0.1"},
						1: {Name: "2.0.1"},
						2: {Name: "3.0.5"},
					},
				},
				"Firefox": {
					Name: "Firefox",
					Variants: []project.Variant{
						0: {Name: "1.0.1"},
						1: {Name: "2.0.1"},
						2: {Name: "3.0.5"},
					},
				},
				"Microsoft Edge": {
					Name: "Microsoft Edge",
					Variants: []project.Variant{
						0: {Name: "1.0.2"},
						1: {Name: "2.0.2"},
						2: {Name: "3.0.1"},
					},
				},
			},
			Steps: []test.Step{
				{
					Index:          1,
					Action:         "Open the application",
					ExpectedResult: "The main screen should show",
				},
				{
					Index:          2,
					Action:         "Open the login page",
					ExpectedResult: "The login page should show",
				},
				{
					Index:          3,
					Action:         "Login with the test login",
					ExpectedResult: "The login should succeed",
				},
			},
		},
	},
	Project: DummyProject.ID(),
}

// DummyUser is a user for testing purposes
var DummyUser = &user.User{
	DisplayName: "DisplayName",
	Name:        "acc_name",
	EMail:       "test@example.com",
}

// DummyUserUnauthorized is a user that
// is is not authorized to send requests
// because he is not a member of a project
var DummyUserUnauthorized = &user.User{
	DisplayName: "DisplayName",
	Name:        "unauthorized",
	EMail:       "un@authorized.com",
}

// DummyMembership represents a membership that is used for testing purposes
// The user has the role "Owner" so he ca
var DummyMembership = &project.UserMembership{
	User:        "acc_name",
	Role:        "Owner",
	MemberSince: time.Now().UTC().Round(time.Second),
}

// DummyRoles are dummy roles for testing purposes
var DummyRoles = map[project.RoleName]*project.Role{
	"Owner": {
		Name: "Owner",
		Permissions: project.Permissions{
			project.DisplayPermissions{
				DisplayProject: true,
			},
			project.ExecutionPermissions{
				Execute: true,
			},
			project.CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			project.SequencePermissions{
				CreateSequence:    true,
				EditSequence:      true,
				DeleteSequence:    true,
				AssignSequence:    true,
				DuplicateSequence: true,
			},
			project.MemberPermissions{
				EditMembers: true,
			},
			project.SettingsPermissions{
				EditProject:     true,
				DeleteProject:   true,
				EditPermissions: true,
			},
		},
	},
	"Supervisor": {
		Name: "Supervisor",
		Permissions: project.Permissions{
			DisplayPermissions: project.DisplayPermissions{
				DisplayProject: true,
			},
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: true,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    true,
				EditSequence:      true,
				DeleteSequence:    true,
				DuplicateSequence: true,
				AssignSequence:    true,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: true,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     true,
				DeleteProject:   true,
				EditPermissions: true,
			},
		},
	},
	"Manager": {
		Name: "Manager",
		Permissions: project.Permissions{
			DisplayPermissions: project.DisplayPermissions{
				DisplayProject: true,
			},
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: true,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    true,
				EditCase:      true,
				DeleteCase:    true,
				AssignCase:    true,
				DuplicateCase: true,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    true,
				EditSequence:      true,
				DeleteSequence:    true,
				DuplicateSequence: true,
				AssignSequence:    true,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: false,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     false,
				DeleteProject:   false,
				EditPermissions: false,
			},
		},
	},
	"Tester": {
		Name: "Tester",
		Permissions: project.Permissions{
			DisplayPermissions: project.DisplayPermissions{
				DisplayProject: true,
			},
			ExecutionPermissions: project.ExecutionPermissions{
				Execute: true,
			},
			CasePermissions: project.CasePermissions{
				CreateCase:    false,
				EditCase:      false,
				DeleteCase:    false,
				AssignCase:    false,
				DuplicateCase: false,
			},
			SequencePermissions: project.SequencePermissions{
				CreateSequence:    false,
				EditSequence:      false,
				DuplicateSequence: false,
				DeleteSequence:    false,
				AssignSequence:    false,
			},
			MemberPermissions: project.MemberPermissions{
				EditMembers: false,
			},
			SettingsPermissions: project.SettingsPermissions{
				EditProject:     false,
				DeleteProject:   false,
				EditPermissions: false,
			},
		},
	},
}

// DummyGroup is a group for testing purposes
var DummyGroup = &group.Group{
	Name:        "Dummy Group Name",
	Description: "Desc",
	Visibility:  visibility.Public,
}

var dummyTestSequenceVersion = test.SequenceVersion{
	VersionNr:     1,
	Description:   "-",
	Preconditions: []test.Precondition{},
	SequenceInfo: test.SequenceInfo{
		Versions:      map[string]*project.Version{},
		DurationHours: 0,
		DurationMin:   0,
	},
	Cases: []test.Case{
		*DummyTestCase,
		*DummyTestCase,
	},
	CreationDate: time.Now().Round(time.Second),
	IsMinor:      false,
	Message:      "-",
}

// DummyTestSequence is a sequence for testing purposes
var DummyTestSequence = &test.Sequence{
	Name:   "test-sequence-1",
	Labels: []project.Label{},
	SequenceVersions: []test.SequenceVersion{
		dummyTestSequenceVersion,
	},
}

var DummyTestSequenceExecutionProtocol = &test.SequenceExecutionProtocol{
	TestVersion: id.NewTestVersionID(DummyTestSequence.SequenceVersions[0].ID().TestID, 1),
	ProtocolNr:  1,

	SUTVersion: "Desktop",
	SUTVariant: "v1018",

	CaseExecutionProtocols: []id.ProtocolID{
		{},
		{},
	},

	ExecutionDate: time.Now().UTC().Round(time.Second),
}

// DummyTestSequenceJSON is sequence in json for testing purposes
var DummyTestSequenceJSON string

func init() {
	b := bytes.NewBufferString("")
	err := json.NewEncoder(b).Encode(DummyTestSequence)
	if err != nil {
		log.Fatalf("Could not encode dummy test sequence as json.\n%v", DummyTestSequence)
	}
	DummyTestSequenceJSON = b.String()
}
