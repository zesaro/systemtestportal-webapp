/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package assignment

import (
	"net/http"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/todo"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// CasePut handles the assignment of a test case to multiple testers
func CasePut(us middleware.UserRetriever, todoAdder handler.TODOListAdder, todoGetter handler.TODOListGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).AssignCase {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		testers := getTesters(us, w, r)

		c.Case.SetTestersOfTestCase(testers)

		if err := createTodoForTesters(r, todoGetter, todoAdder, c, testers, todo.Assignment, todo.Case); err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}

const (
	errNoSuchRefType    = "invalid reference type"
	errNoSuchRefTypeMsg = "the given reference type is not supported"
)

// createTodoForTesters creates tod o-items for every tester.
//
// The refType defines the type of the entity that is referenced
// by the to do-item.
//
// Returns an error if the to do-list for a test cannot be retrieved or
// if the updated to do-list cannot be saved.
func createTodoForTesters(r *http.Request, todoGetter handler.TODOListGetter, todoAdder handler.TODOListAdder,
	contextEntities *handler.ContextEntities, testers []*user.User, todoType todo.Type, refType todo.ReferenceType) error {

	// Create a to do-item for every tester
	for _, tester := range testers {
		todoList, err := todoGetter.Get(tester.Name)
		if err != nil {
			return err
		}

		var refID string
		switch refType {
		case todo.Case:
			refID = contextEntities.Case.Name
		case todo.Sequence:
			refID = contextEntities.Sequence.Name
		default:
			return errors.ConstructStd(http.StatusInternalServerError, errNoSuchRefType, errNoSuchRefTypeMsg, r).
				WithStackTrace(1).
				Finish()
		}

		todoList = todoList.AddItem(contextEntities.User.ID(), contextEntities.Project.ID(), todoType, refType, refID, time.Time{})

		if err := todoAdder.Add(*todoList); err != nil {
			return err
		}
	}

	return nil
}
