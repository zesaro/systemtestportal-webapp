/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

// This file contains the parameter keys for the information that is
// sent between a client and the server during an execution.

const keyStepNr = "step"
const keyCaseNr = "case"
const keyAllSubObjects = "SubObjects"
const keySUTVersion = "inputSUTVersion"
const keySUTVariant = "inputSUTVariant"
const keyProject = "Project"
const keyTestObject = "TestObject"
const keyTestObjectVersion = "TestObjectVersion"
const keyObservedBehavior = "inputTestStepActualResult"
const keyComment = "notes"
const keyResult = "result"
const keyAction = "Action"
const keyExpectedResult = "ExpectedResult"
const keyHours = "hours"
const keyMinutes = "minutes"
const keySeconds = "seconds"
const keyEstimatedMinutes = "EstimatedMin"
const keyEstimatedHours = "EstimatedHours"
const keyProgress = "ProgressBarCurrent"
const keyProgressPercent = "ProgressBarPercent"
const keyProgressTotal = "ProgressBarTotal"
const keyIsSeq = "isSequence"
const keyIsInSeq = "isInSequence"
const keyIsAnonymous = "isAnonymous" // Whether the username is shown in the protocol
