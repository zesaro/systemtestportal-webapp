/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

// SequenceSession is the session that stores sequence data during execution of
// a testsequence.
type SequenceSession interface {
	SequenceSessionSetter
	//RemoveCurrentSequenceProtocol removes the current sequence protocol from the session.
	RemoveCurrentSequenceProtocol(w http.ResponseWriter, r *http.Request) error
	SequenceSessionGetter
	TimeSession
}

// SequenceSessionGetter interface for retrieving testsequence protocols.
type SequenceSessionGetter interface {
	//GetCurrentSequenceProtocol returns the protocol to the currently running sequence execution to the given request.
	//If there is no sequence execution running, the function will return nil, nil
	//If an error occurs nil and the error will be returned
	GetCurrentSequenceProtocol(r *http.Request) (*test.SequenceExecutionProtocol, error)
}

// SequenceSessionSetter is used to set the managed testsequence protocol during requests.
type SequenceSessionSetter interface {
	//SetCurrentSequenceProtocol saves the given protocol to the session.
	//After this call, you can get the current sequence protocol via the GetCurrentSequenceProtocol-function
	SetCurrentSequenceProtocol(w http.ResponseWriter, r *http.Request,
		protocol *test.SequenceExecutionProtocol) error
}

// SequenceProtocolStore interface for storing testsequence protocols.
type SequenceProtocolStore interface {
	SequenceProtocolAdder
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(projectID, sequenceID string) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(projectID, sequenceID string, protocolID int) (test.SequenceExecutionProtocol, error)
}

// SequenceProtocolAdder interface for storing testsequence protocols.
type SequenceProtocolAdder interface {
	// AddSequenceProtocol adds the given protocol to the store
	AddSequenceProtocol(r *test.SequenceExecutionProtocol, sequenceVersion test.SequenceVersion) (err error)
}

// SequenceStarPageGet serves the start page for a testsequence execution.
func SequenceStarPageGet(time TimeSession, caseGetter CaseProtocolGetter,
	getter SequenceSessionGetter) http.HandlerFunc {
	printer := &sequenceExecutionPrinter{getter, caseGetter, time}
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tsv, err := handler.GetTestSequenceVersion(r, c.Sequence)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		printer.printStartPage(w, r, *tsv)
	}
}

// SequenceExecutionPost returns a handler capable of handling every request during the execution
// of a sequence.
func SequenceExecutionPost(caseProtocolLister, sequenceProtocolLister test.ProtocolLister,
	caseProtocolStore CaseProtocolStore, sequenceProtocolAdder SequenceProtocolAdder,
	caseSession CaseSession, sequenceSession SequenceSession, tcg handler.TestCaseGetter, tsg middleware.TestSequenceStore) http.HandlerFunc {

	t := &sessionTimer{caseSession}
	cp := caseExecutionPrinter{
		caseSession,
		sequenceSession,
		caseSession,
	}

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.User == nil || c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tsv, err := handler.GetTestSequenceVersion(r, c.Sequence)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		caseNr := getFormValueInt(r, keyCaseNr)
		stepNr := getFormValueInt(r, keyStepNr)
		caseCount := len(tsv.Cases)

		var progress = sequenceProgress{}
		if err := progress.Init(r); err != nil {
			errors.Handle(err, w, r)
			return
		}

		if caseNr > 0 && caseNr <= caseCount {
			middleware.AddToContext(r, middleware.TestCaseKey, &tsv.Cases[caseNr-1])
			CaseExecutionPost(caseProtocolLister, caseProtocolStore, caseSession, sequenceSession,
				sequenceSession, sequenceProtocolAdder, &progress, tsv, tcg).ServeHTTP(w, r)
			return
		}

		switch {
		case caseNr <= 0 && stepNr == 0:
			handleSequenceStartPage(w, r, sequenceProtocolLister, t, sequenceSession, cp, tsv, tsg)
		case caseNr == 0 && stepNr == 1:
			// Execution finished showing sequence
			http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
		case caseNr == caseCount+1:
			http.Redirect(w, r, ".?fragment=true", http.StatusSeeOther)
		default:
			executionPageNotFound(w, r)
		}
	}
}

func handleSequenceStartPage(w http.ResponseWriter, r *http.Request,
	protocolLister test.ProtocolLister, timer timer,
	sequenceSessionSetter SequenceSessionSetter, caseExecutionPrinter caseExecutionPrinter,
	testSequenceVersion *test.SequenceVersion, sequenceGetter middleware.TestSequenceStore) {

	c := handler.GetContextEntities(r)
	sutVariant := getFormValueString(r, keySUTVariant)
	sutVersion := getFormValueString(r, keySUTVersion)
	if sutVariant == "" || sutVersion == "" {
		errors.ConstructStd(http.StatusBadRequest, emptySUTTitle, emptySUT, r).
			WithLog("client sent empty sut-variant or empty sut-version").
			WithStackTrace(2).
			WithRequestDump(r).
			Respond(w)
		return
	}

	time, err := timer.updateTime(w, r)
	if err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveStartPage, r).
			WithLog("Error while trying to create and save current protocol.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Respond(w)
		return
	}

	testSequence, ex, err := sequenceGetter.Get(testSequenceVersion.ID().TestID)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	if !ex {
		errors.Handle(fmt.Errorf("couldn't find related test case"), w, r)
		return
	}
	var preconditionResultStrings []string
	jsonStringPreconditions := r.FormValue(httputil.PreconditionResults)
	err = json.Unmarshal([]byte(jsonStringPreconditions), &preconditionResultStrings)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	var preconditionResults = make([]test.PreconditionResult, 0)
	for index, result := range preconditionResultStrings {
		item := test.PreconditionResult{
			Precondition: testSequence.SequenceVersions[len(testSequence.SequenceVersions)-testSequenceVersion.ID().TestVersion()].Preconditions[index],
			Result:       result,
		}
		preconditionResults = append(preconditionResults, item)
	}

	isAnonymous := getFormValueBool(r, keyIsAnonymous)

	// Create and store protocol.
	prt, err := test.NewSequenceExecutionProtocol(protocolLister, preconditionResults, testSequenceVersion.ID(),
		sutVariant, sutVersion, time, c.User.Name, isAnonymous)
	if err == nil {
		err = sequenceSessionSetter.SetCurrentSequenceProtocol(w, r, &prt)
	}
	if err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveStartPage, r).
			WithLog("Error while trying to create and save current protocol.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Respond(w)
		return
	}

	caseNr := getFormValueInt(r, keyCaseNr)
	tc := testSequenceVersion.Cases[caseNr]
	tcv := tc.TestCaseVersions[0]

	caseExecutionPrinter.printStartPage(w, r, tc, tcv, 2, totalWorkSteps(*testSequenceVersion))
}

func handleSequenceSummaryPage(w http.ResponseWriter, r *http.Request,
	timer timer, sequenceProtocolAdder SequenceProtocolAdder, sequenceSession SequenceSession,
	caseExecutionPrinter caseExecutionPrinter, sequenceExecutionPrinter sequenceExecutionPrinter,
	progress progressMeter, testSequenceVersion *test.SequenceVersion) {

	sequenceProtocol, err := sequenceSession.GetCurrentSequenceProtocol(r)
	if sequenceProtocol == nil {
		errors.Handle(err, w, r)
		return
	}

	caseProtocol, err := caseExecutionPrinter.GetCurrentCaseProtocol(r)
	if caseProtocol == nil {
		errors.Handle(err, w, r)
		return
	}
	sequenceProtocol.AddCaseExecutionProtocol(*caseProtocol)

	err = sequenceSession.SetCurrentSequenceProtocol(w, r, sequenceProtocol)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	caseNr := getFormValueInt(r, keyCaseNr)
	tc := testSequenceVersion.Cases[caseNr-1]

	if caseNr == len(testSequenceVersion.Cases) { // End of sequence execution
		sequenceExecutionPrinter.printSummaryPage(w, r, *testSequenceVersion)
		saveSequenceProtocol(r, w, timer, sequenceProtocolAdder, sequenceSession, testSequenceVersion)
	} else { // Print start page of next case
		tc = testSequenceVersion.Cases[caseNr]
		tcv := tc.TestCaseVersions[0]
		caseExecutionPrinter.printStartPage(w, r, tc, tcv, progress.Get(), progress.Max())
	}
}

// saveSequenceProtocol handles the saving of the sequences protocol
// and shows an error modal if something goes wrong
func saveSequenceProtocol(r *http.Request, w http.ResponseWriter, timer timer,
	sequenceProtocolAdder SequenceProtocolAdder, sequenceSession SequenceSession, sequenceVersion *test.SequenceVersion) { // Save sequence protocol
	prt, err := sequenceSession.GetCurrentSequenceProtocol(r)
	if prt == nil || err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveSummaryPage, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	if err := sequenceProtocolAdder.AddSequenceProtocol(prt, *sequenceVersion); err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveSummaryPage, r).
			WithLog("Error while trying to save protocol into store.").
			WithCause(err).
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	if err := sequenceSession.RemoveCurrentSequenceProtocol(w, r); err != nil {
		err := errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save current protocol.").Finish()
		errors.Log(err)
		return
	}

	if err := timer.resetTime(w, r); err != nil {
		errors.Handle(err, w, r)
		return

	}
}
