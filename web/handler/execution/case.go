/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// CaseSession is used to manage the session for testcases during execution.
type CaseSession interface {
	CaseSessionUpdater
	//RemoveCurrentCaseProtocol removes the current case protocol from the session.
	RemoveCurrentCaseProtocol(w http.ResponseWriter, r *http.Request) error
	TimeSession
}

// CaseSessionUpdater is used to update the testcase currently contained within a session.
type CaseSessionUpdater interface {
	CaseSessionGetter
	CaseSessionSetter
}

// CaseSessionGetter is used to get the testcase currently contained within a session.
type CaseSessionGetter interface {
	//GetCurrentCaseProtocol returns the protocol to the currently running case execution to the given request.
	//If there is no case execution running, the function will return nil, nil
	//If an error occurs nil and the error will be returned
	GetCurrentCaseProtocol(r *http.Request) (*test.CaseExecutionProtocol, error)
}

// CaseSessionSetter is used to set the managed testcase during a session.
type CaseSessionSetter interface {
	//SetCurrentCaseProtocol saves the given protocol to the session.
	//After this call, you can get the current case protocol via the GetCurrentCaseProtocol-function
	SetCurrentCaseProtocol(w http.ResponseWriter, r *http.Request, protocol *test.CaseExecutionProtocol) error
}

// CaseProtocolCleaner is used to clean the testcase session after a new protocol has
// been successfully added.
type CaseProtocolCleaner interface {
	CaseSessionGetter
	//RemoveCurrentCaseProtocol removes the current case protocol from the session.
	RemoveCurrentCaseProtocol(w http.ResponseWriter, r *http.Request) error
}

// CaseProtocolStore interface for storing testcase protocols.
type CaseProtocolStore interface {
	CaseProtocolAdder
	CaseProtocolGetter
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error)
}

// CaseProtocolAdder is used to add new testcase protocols to the store.
type CaseProtocolAdder interface {
	// AddCaseProtocol adds the given protocol to the store
	AddCaseProtocol(r *test.CaseExecutionProtocol, testCaseVersion test.CaseVersion) (err error)
}

// CaseProtocolGetter is used to get testcase protocols from the store.
type CaseProtocolGetter interface {
	// GetCaseExecutionProtocol gets the protocol with the given id for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
}

// CaseStartPageGet serves the start page for testcase executions.
func CaseStartPageGet(cs CaseSession, ss SequenceSessionGetter) http.HandlerFunc {
	printer := caseExecutionPrinter{cs, ss, cs}
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcv, err := handler.GetTestCaseVersion(r, c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		printer.printStartPage(w, r, *c.Case, *tcv, 1, len(tcv.Steps)+3)
	}
}

// CaseExecutionPost handles all post requests during a testcase execution. It is a meta handler
// that further calls it's sub handlers.
func CaseExecutionPost(protocolLister test.ProtocolLister, caseProtocolStore CaseProtocolStore,
	caseSession CaseSession, sequenceSessionGetter SequenceSessionGetter,
// Params for sequence summary page
	sequenceSession SequenceSession, sequenceProtocolAdder SequenceProtocolAdder,
	progress progressMeter, testSequenceVersion *test.SequenceVersion, testCaseGetter handler.TestCaseGetter) http.HandlerFunc {

	t := sessionTimer{caseSession}
	p := caseExecutionPrinter{
		caseSession,
		sequenceSessionGetter,
		caseSession,
	}
	sp := sequenceExecutionPrinter{
		sequenceSession,
		caseProtocolStore,
		caseSession,
	}

	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil || c.User == nil || c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).Execute {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcv, err := handler.GetTestCaseVersion(r, c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		// The passed progress is nil if this is the execution of a case and it needs to be
		// initialized as caseProgress. If this is the execution of a sequence, the passed
		// progress is a sequenceProgress.
		if progress == nil {
			progress = &caseProgress{}
		}
		if err := progress.Init(r); err != nil {
			errors.Handle(err, w, r)
			return
		}

		currentStepNr := getFormValueInt(r, keyStepNr)
		totalStepsNr := len(tcv.Steps)

		switch {
		case currentStepNr == 0:
			handleCaseStartPage(w, r, protocolLister, &t, caseSession, &p, tcv, progress, testCaseGetter)
		case currentStepNr > 0 && currentStepNr < totalStepsNr+1:
			handleCaseStepPage(w, r, &t, caseSession, &p, tcv, progress)
		case currentStepNr == totalStepsNr+1:
			handleCaseSummaryPage(w, r, &t, caseSession, caseProtocolStore,
				sequenceSession, sequenceProtocolAdder, p, sp, progress, testSequenceVersion, testCaseGetter)
		default:
			executionPageNotFound(w, r)
		}
	}
}

func handleCaseStartPage(w http.ResponseWriter, r *http.Request,
	protocolLister test.ProtocolLister, timer timer, caseSessionSetter CaseSessionSetter,
	caseExecutionPrinter *caseExecutionPrinter, testCaseVersion *test.CaseVersion, progress progressMeter, caseGetter handler.TestCaseGetter) {

	sutVersion := getFormValueString(r, keySUTVersion)
	sutVariant := getFormValueString(r, keySUTVariant)

	contextEntities := handler.GetContextEntities(r)

	// If this is a sequence execution, get the sut-variant/sut-version
	// from the sequence protocol
	if sutVersion == "" || sutVariant == "" {
		sep, _ := caseExecutionPrinter.GetCurrentSequenceProtocol(r)
		sutVariant = sep.SUTVariant
		sutVersion = sep.SUTVersion
	}

	time, err := timer.updateTime(w, r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	testCase, ex, err := caseGetter.Get(testCaseVersion.ID().TestID)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	if !ex {
		errors.Handle(fmt.Errorf("couldn't find related test case"), w, r)
		return
	}

	var preconditionResultStrings []string
	jsonStringPreconditions := r.FormValue(httputil.PreconditionResults)
	err = json.Unmarshal([]byte(jsonStringPreconditions), &preconditionResultStrings)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	var preconditionResults = make([]test.PreconditionResult, 0)
	for index, result := range preconditionResultStrings {
		item := test.PreconditionResult{
			Precondition: testCase.TestCaseVersions[len(testCase.TestCaseVersions)-testCaseVersion.ID().TestVersion()].Preconditions[index],
			Result:       result,
		}
		preconditionResults = append(preconditionResults, item)
	}

	isAnonymous := getFormValueBool(r, keyIsAnonymous)

	caseProtocol, err := test.NewCaseExecutionProtocol(protocolLister, preconditionResults, testCaseVersion.ID(),
		sutVariant, sutVersion, time, len(testCaseVersion.Steps), contextEntities.User.Name, isAnonymous)
	if err == nil {
		err = caseSessionSetter.SetCurrentCaseProtocol(w, r, &caseProtocol)
	} else {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveStartPage, r).
			WithLog("Error while trying to create and save current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	caseExecutionPrinter.printStepPage(w, r, *testCaseVersion, 1, progress.Get(), progress.Max())
}

func handleCaseStepPage(w http.ResponseWriter, r *http.Request,
	timer timer, caseSessionUpdater CaseSessionUpdater,
	caseExecutionPrinter *caseExecutionPrinter, testCaseVersion *test.CaseVersion,
	progress progressMeter) {

	currentCaseProtocol, err := caseSessionUpdater.GetCurrentCaseProtocol(r)
	if currentCaseProtocol == nil || err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveStep, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Respond(w)
		return
	}

	observedBehavior := getFormValueString(r, keyObservedBehavior)
	comment := getFormValueString(r, keyComment)
	stepNr := getFormValueInt(r, keyStepNr)

	time, err := timer.updateTime(w, r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	result, ok := getFormValueResult(r, keyResult)
	if !ok {
		errors.ConstructStd(http.StatusBadRequest,
			failedSave, unableToSaveStep, r).
			WithLog("Client send an invalid request.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	currentCaseProtocol.SaveStep(stepNr, observedBehavior, result, comment, time)
	if err := caseSessionUpdater.SetCurrentCaseProtocol(w, r, currentCaseProtocol); err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveStep, r).
			WithLog("Error while trying to save current protocol.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Respond(w)
		return
	}

	// Case is finished -> show summary page
	if stepNr == len(testCaseVersion.Steps) {
		caseExecutionPrinter.printSummaryPage(w, r, *testCaseVersion, progress.Get(), progress.Max())
		return
	}

	caseExecutionPrinter.printStepPage(w, r, *testCaseVersion, stepNr+1, progress.Get(), progress.Max())
}

func handleCaseSummaryPage(w http.ResponseWriter, r *http.Request,
	timer timer, caseProtocolCleaner CaseProtocolCleaner, caseProtocolAdder CaseProtocolAdder,
// Params for the sequence summary page
	sequenceSession SequenceSession, sequenceProtocolAdder SequenceProtocolAdder,
	caseExecutionPrinter caseExecutionPrinter, sequenceExecutionPrinter sequenceExecutionPrinter,
	progress progressMeter, testSequenceVersion *test.SequenceVersion, testCaseGetter handler.TestCaseGetter) {

	comment := getFormValueString(r, keyComment)

	time, err := timer.updateTime(w, r)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}

	result, ok := getFormValueResult(r, keyResult)
	if !ok {
		errors.ConstructStd(http.StatusBadRequest,
			failedSave, unableToSaveStep, r).
			WithLog("Client send an invalid request.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	//Get Current Protocol
	prt, _ := caseProtocolCleaner.GetCurrentCaseProtocol(r)
	if prt == nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveSummaryPage, r).
			WithLog("Error while trying to get current protocol.").
			WithStackTrace(1).
			WithRequestDump(r).
			Respond(w)
		return
	}

	//Before saving the protocol we have to sum up the test step times and add it to the current time
	for _, v := range prt.StepProtocols {
		time = time.Add(v.NeededTime)
	}

	//Save Data to protocol and protocol to store
	prt.Finish(result, comment, time)
	tc, ex, err := testCaseGetter.Get(prt.TestVersion.TestID)
	if err != nil {
		errors.Handle(err, w, r)
		return
	}
	if !ex {
		errors.Handle(fmt.Errorf("Couldn't find test case"), w, r)
		return
	}

	tcv := tc.TestCaseVersions[len(tc.TestCaseVersions)-prt.TestVersion.TestVersion()]
	err = caseProtocolAdder.AddCaseProtocol(prt, tcv)
	if err != nil {
		errors.ConstructStd(http.StatusInternalServerError,
			failedSave, unableToSaveSummaryPage, r).
			WithLog("Error while trying to save protocol into store.").
			WithStackTrace(1).
			WithCause(err).
			WithRequestDump(r).
			Respond(w)
		return
	}

	if sequenceSession != nil {
		if seqPr, _ := sequenceSession.GetCurrentSequenceProtocol(r); seqPr != nil {
			handleSequenceSummaryPage(w, r, timer, sequenceProtocolAdder, sequenceSession,
				caseExecutionPrinter, sequenceExecutionPrinter, progress, testSequenceVersion)
		}
	}

	//Remove Protocol from Session
	if err = caseProtocolCleaner.RemoveCurrentCaseProtocol(w, r); err != nil {
		e := errors.ConstructWithStackTrace(http.StatusInternalServerError,
			"Error while trying to save current protocol.").Finish()
		errors.Log(e)
		return
	}

	if sequenceSession == nil {
		// If the case execution finished,
		// redirect back to case if everything worked fine
		url := strings.TrimSuffix(r.URL.RawPath, "execute")
		url = url + "?fragment=true"
		http.Redirect(w, r, url, http.StatusSeeOther)

	}
}
