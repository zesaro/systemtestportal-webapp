/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"

	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestCaseStartGet(t *testing.T) {
	// Invalid case version in request parameters
	invVersionParams := url.Values{}
	invVersionParams.Add(httputil.Version, "invalidversion")

	validVersionParams := url.Values{}
	validVersionParams.Add(httputil.Version, "1")

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  nil,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)
	ctxUnauthorized := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUserUnauthorized,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{}
				ss := &handler.SequenceSessionGetterMock{}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 0),
				)
			},
			handler.EmptyRequest(http.MethodGet),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{}
				ss := &handler.SequenceSessionGetterMock{}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 0),
				)
			},
			handler.SimpleRequest(ctxUnauthorized, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorized, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Invalid case version",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{}
				ss := &handler.SequenceSessionGetterMock{}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 0),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, invVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, invVersionParams),
		),
		handler.CreateTest("No project in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{}
				ss := &handler.SequenceSessionGetterMock{}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 0),
				)
			},
			handler.SimpleRequest(ctxNoProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Sequence protocol is nil",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{}
				ss := &handler.SequenceSessionGetterMock{SeqExecProtocol: nil}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Duration is nil",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{}
				ss := &handler.SequenceSessionGetterMock{SeqExecProtocol: &test.SequenceExecutionProtocol{}}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				cs := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				ss := &handler.SequenceSessionGetterMock{SeqExecProtocol: &test.SequenceExecutionProtocol{}}
				return CaseStartPageGet(cs, ss), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(cs, 0),
					handler.HasCalls(ss, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, validVersionParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, validVersionParams),
		),
	)
}

// TestCaseExecutionPost tests the CaseExecutionPost function.
// Tests for start page post, step page post and summary page post
// are separate.
func TestCaseExecutionPost(t *testing.T) {
	// Params for testing the response executionPageNotFound
	paramsInvalidStep := url.Values{}
	paramsInvalidStep.Add(keyStepNr, "-1")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctxNoProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  nil,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	ctxNoCase := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: nil,
			middleware.UserKey:     handler.DummyUser,
		},
	)
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     nil,
		},
	)
	ctxUnauthorized := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUserUnauthorized,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Empty context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(protocolLister, 0),
						handler.HasCalls(caseProtocolStore, 0),
						handler.HasCalls(caseSession, 0),
						handler.HasCalls(sequenceSessionGetter, 0),
					)
			},
			handler.EmptyRequest(http.MethodPost),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("Unauthorized user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusForbidden),
						handler.HasCalls(protocolLister, 0),
						handler.HasCalls(caseProtocolStore, 0),
						handler.HasCalls(caseSession, 0),
						handler.HasCalls(sequenceSessionGetter, 0),
					)
			},
			handler.SimpleRequest(ctxUnauthorized, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxUnauthorized, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No project in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(protocolLister, 0),
						handler.HasCalls(caseProtocolStore, 0),
						handler.HasCalls(caseSession, 0),
						handler.HasCalls(sequenceSessionGetter, 0),
					)
			},
			handler.SimpleRequest(ctxNoProject, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoProject, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No case in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(protocolLister, 0),
						handler.HasCalls(caseProtocolStore, 0),
						handler.HasCalls(caseSession, 0),
						handler.HasCalls(sequenceSessionGetter, 0),
					)
			},
			handler.SimpleRequest(ctxNoCase, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoCase, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(protocolLister, 0),
						handler.HasCalls(caseProtocolStore, 0),
						handler.HasCalls(caseSession, 0),
						handler.HasCalls(sequenceSessionGetter, 0),
					)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodPost, handler.NoParams),
			handler.SimpleFragmentRequest(ctxNoUser, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("Invalid step nr",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}}}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusNotFound),
						handler.HasCalls(protocolLister, 0),
						handler.HasCalls(caseProtocolStore, 0),
						handler.HasCalls(caseSession, 0),
						handler.HasCalls(sequenceSessionGetter, 0),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsInvalidStep),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsInvalidStep),
		),
	)
}

// TestCaseStartPost tests the post request of a case start page
func TestCaseStartPost(t *testing.T) {
	// Params for a valid case start page post request
	paramsStartPage := url.Values{}
	paramsStartPage.Add(keyStepNr, "0")
	paramsStartPage.Add(keySUTVariant, "SUT-Variant")
	paramsStartPage.Add(keySUTVersion, "SUT-Version")
	paramsStartPage.Add("preconditions", "[\"fulfilled\"]")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Cannot create and save protocol",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{
					Err: errors.New("Cannot save new protocol"),
				}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStartPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStartPage),
		),
		handler.CreateTest("Case start page POST valid",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStartPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStartPage),
		),
	)
}

func TestCaseStepPost(t *testing.T) {
	// Params for a valid case step page post request
	paramsStepPage := url.Values{}
	paramsStepPage.Add(keyStepNr, "1")
	paramsStepPage.Add(keyResult, "NotAssessed")

	// Params for a valid case step after which the
	// summary page is displayed
	paramsStepPageSummary := url.Values{}
	paramsStepPageSummary.Add(keyStepNr, "3")
	paramsStepPageSummary.Add(keyResult, "NotAssessed")

	// Params with an invalid result
	paramsStepPageInvalidResult := url.Values{}
	paramsStepPageInvalidResult.Add(keyStepNr, "3")
	paramsStepPageInvalidResult.Add(keyResult, "InvalidResult")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid result",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}},
							},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStepPageInvalidResult),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStepPageInvalidResult),
		),
		handler.CreateTest("Cannot retrieve current case protocol from session",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}},
							},
							Err: errors.New("Cannot get current case protocol"),
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStepPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStepPage),
		),
		handler.CreateTest("Cannot save current case protocol to session",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}},
							},
						},
						CaseSessionSetterMock: handler.CaseSessionSetterMock{
							Err: errors.New("Cannot save current protocol to session"),
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStepPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStepPage),
		),
		handler.CreateTest("Valid case step page POST valid to next step",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}},
							},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStepPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStepPage),
		),
		handler.CreateTest("Valid case step page POST to summary page",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}, {}, {}},
							},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsStepPageSummary),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsStepPageSummary),
		),
	)
}

func TestCaseSummaryPost(t *testing.T) {
	// Params for a valid case summary page post request
	paramsSummaryPage := url.Values{}
	paramsSummaryPage.Add(keyStepNr, "4")
	paramsSummaryPage.Add(keyResult, "NotAssessed")
	paramsSummaryPage.Add("preconditions", "[\"fulfilled\"]")

	paramsInvalidResult := url.Values{}
	paramsInvalidResult.Add(keyStepNr, "4")
	paramsInvalidResult.Add(keyResult, "InvalidResult")
	paramsSummaryPage.Add("preconditions", "[\"fulfilled\"]")

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
			middleware.UserKey:     handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid result",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}, {}, {}, {}},
							},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusBadRequest),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsInvalidResult),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsInvalidResult),
		),
		handler.CreateTest("Case protocol is nil",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: nil,
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsSummaryPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsSummaryPage),
		),
		handler.CreateTest("Can't add case protocol to store",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{
					CaseProtocolAdderMock: handler.CaseProtocolAdderMock{
						Err: errors.New("Cannot add protocol to store"),
					},
				}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}, {}, {}, {}},
							},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsSummaryPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsSummaryPage),
		),
		handler.CreateTest("Can't remove case protocol from session",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}, {}, {}, {}},
							},
						},
					},
					Err: errors.New("Cannot remove case protocol from session"),
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						// Although the protocol could not be removed from the session,
						// the user sees a status ok (200)
						handler.HasStatus(http.StatusOK),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsSummaryPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsSummaryPage),
		),
		handler.CreateTest("Case summary page POST valid",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				protocolLister := &handler.ProtocolListerMock{}
				caseProtocolStore := &handler.CaseProtocolStoreMock{}
				caseSession := &handler.CaseSessionMock{
					TimeSessionMock: handler.TimeSessionMock{Duration: &duration.Duration{}},
					CaseSessionUpdaterMock: handler.CaseSessionUpdaterMock{
						CaseSessionGetterMock: handler.CaseSessionGetterMock{
							CaseExecutionProtocol: &test.CaseExecutionProtocol{
								TestVersion:   id.NewTestVersionID(id.NewTestID(id.ProjectID{}, "test", true), 1),
								StepProtocols: []test.StepExecutionProtocol{{}, {}, {}, {}},
							},
						},
					},
				}
				sequenceSessionGetter := &handler.SequenceSessionGetterMock{}
				caseGetter := &handler.CaseGetterMock{}
				return CaseExecutionPost(protocolLister, caseProtocolStore, caseSession, sequenceSessionGetter,
						nil, nil, nil, nil, caseGetter), handler.Matches(
						handler.HasStatus(http.StatusSeeOther),
					)
			},
			handler.SimpleRequest(ctx, http.MethodPost, paramsSummaryPage),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, paramsSummaryPage),
		),
	)
}
