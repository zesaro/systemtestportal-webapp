/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// getProjectTabPageByName returns the template with the given
// name either as fragment only or the with all its parent
// fragments, depending on the isFrag parameter
func getProjectTabPageByName(r *http.Request, templateName string) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getProjectTabFragmentByName(templateName)
	}
	return getProjectTabTreeByName(templateName)
}

// getProjectTabFragmentByName returns only the tab template with the given name
func getProjectTabFragmentByName(templateName string) *template.Template {
	return handler.GetBaseTree().
		Append(templateName).
		Append(templates.ExecutionAbortModal).
		Get().Lookup(templates.TabContent)
}

// getProjectTabTreeByName returns tab template with the given name with all parent templates
func getProjectTabTreeByName(templateName string) *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab case edit tree
		Append(templateName).
		Append(templates.ExecutionAbortModal).
		Get().Lookup(templates.HeaderDef)
}
