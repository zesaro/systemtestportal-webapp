/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package execution

import (
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
)

// progressMeter is a struct that stores progress.
type progressMeter interface {
	// Init initializes the meter with values given by
	// the request.
	Init(r *http.Request) error
	// Adds the given number to the progress of this meter.
	Add(progress int)
	// Get gets the current progress of the meter.
	Get() int
	// Max gets the cap for the progress.
	Max() int
}

// totalWorkSteps counts the total amount of pages that a user
// has to click through when executing a sequence.
func totalWorkSteps(tsv test.SequenceVersion) int {
	result := tsv.CountIncludedSteps() // all steps in the cases
	result += 2 * len(tsv.Cases)       // start and summary page of every case
	result += 3                        // start and summary page of the sequence + summary page needs to be confirmed
	return result
}

// totalWorkStepsUpTo counts the full amount of pages that a user has to click through
// when executing a sequence up to given case number.
func totalWorkStepsUpTo(tsv test.SequenceVersion, maxCaseNr int) int {
	if maxCaseNr < 0 {
		return 0
	} else if maxCaseNr == 0 {
		return 1
	}
	result := tsv.CountIncludedStepsUpTo(maxCaseNr - 1)
	result += 2 * maxCaseNr
	result++
	return result
}
