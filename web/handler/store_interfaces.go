/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"gitlab.com/stp-team/systemtestportal-webapp/domain/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/group"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/todo"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// TestCaseAdder provides everything needed to add a testcase to a storage.
type TestCaseAdder interface {
	// Add stores a test case for the given project under the given container
	Add(testCase *test.Case) error
}

// TestCaseDeleter provides everything needed to remove a testcase to a storage.
type TestCaseDeleter interface {
	// Delete removes the test case with the given ID for the given project under the given container
	Delete(caseID id.TestID) error
}

// TestCaseRenamer provides a function for renaming a test case
type TestCaseRenamer interface {
	// Rename changes the name of a given test case to the new name
	Rename(old, new id.TestID) error
}

// TestCaseUpdater contains every method needed to update a testcase.
type TestCaseUpdater interface {
	TestCaseDeleter
	TestCaseAdder
	TestCaseRenamer
}

// CommentAdder is used to store a comment to the store
type CommentAdder interface {
	// Add stores a comment for the given test case, project and container
	Add(c *comment.Comment) error
}

// ProjectAdder is used to add projects to the storage.
type ProjectAdder interface {
	// Add stores a new project. The first argument is used to identify the container for the project
	Add(*project.Project) error
}

// GroupAdder is used to add groups to the storage.
type GroupAdder interface {
	// Add stores a group in the system
	Add(group *group.Group) error
}

// TestSequenceAdder is used to add testsequences to the storage.
type TestSequenceAdder interface {
	// Add stores a test sequence for the given project under the given container
	Add(testSequence *test.Sequence) error
}

// TestCaseGetter is used to get testcases from the store.
type TestCaseGetter interface {
	// Get returns the test case with the given ID for the given project under the given container
	Get(caseID id.TestID) (*test.Case, bool, error)
}

// TestCaseLister is used to list all testcases for a specific project.
type TestCaseLister interface {
	// List returns a list of test cases for the given project under the given container
	List(projectID id.ProjectID) ([]*test.Case, error)
}

//ProjectRoleLister is used to list all roles for a specific project.
type ProjectRoleLister interface {
	// List returns a list of roles for the given project under the given container
	List(projectID id.ProjectID) ([]*project.Role, error)
}

// GroupLister is used to list all groups in the system.
type GroupLister interface {
	// List returns a list of groups in the system
	List() ([]*group.Group, error)
}

// ProjectLister is used to list all projects in the system.
type ProjectLister interface {
	// ListForOwner returns the projects for an owner
	ListForOwner(id.ActorID) ([]*project.Project, error)
	// ListForMember returns all projects where the actor is a member
	ListForMember(id.ActorID) ([]*project.Project, error)
	// ListForActor returns all projects that the actor has access to
	ListForActor(id.ActorID) ([]*project.Project, error)
	// ListAll returns all projects in the system
	ListAll() ([]*project.Project, error)
	// ListPublic returns all public projects in the system
	ListPublic() ([]*project.Project, error)
	// ListInternal returns all internal projects in the system
	ListInternal() ([]*project.Project, error)
	// ListPrivate returns all private projects for an actor
	ListPrivate(id.ActorID) ([]*project.Project, error)
}

// UserLister is used to list all users in the system.
type UserLister interface {
	// List returns a list of users in the system
	List() ([]*user.User, error)
}

// TO DOListAdder is used to add a to do list to the storage
type TODOListAdder interface {
	// Add saves a to do-list to the system
	Add(list todo.List) error
}

// TO DOListGetter is used to retrieve to do-lists from the store
type TODOListGetter interface {
	// Get returns the to do-list of a user. If the user does not have one,
	// an empty list is returned
	Get(username string) (*todo.List, error)
}

// TestSequenceLister is used to list all testsequences for a specific project.
type TestSequenceLister interface {
	// List returns a list of test sequences for the given project under the given container
	List(projectID id.ProjectID) ([]*test.Sequence, error)
}

// TestSequenceGetter returns the test.Sequence for the testId
type TestSequenceGetter interface {
	Get(testId id.TestID) (*test.Sequence, bool, error)
}

// ProtocolCaseRenamer is used to handle renames.
type ProtocolCaseRenamer interface {
	// HandleCaseRename updates the protocol store after a test case has been renamed
	HandleCaseRename(old, new id.TestID) error
}

// ProjectDeleter is used to delete the reference of an existing project from storage
type ProjectDeleter interface {
	// Delete removes an existing project. The first argument is used to identify the container
	// and the second to identify the project
	Delete(projectID id.ProjectID) error
}

// TestSequenceUpdater is used to update testsequences in the storage.
type TestSequenceUpdater interface {
	TestSequenceAdder
	TestSequenceRenamer
	TestSequenceDeleter
}

// TestSequenceRenamer provides a function for renaming a test case
type TestSequenceRenamer interface {
	// Rename changes the name of a given test sequence to the new name
	Rename(old, new id.TestID) error
}

// SequenceRenameHandler defines an interface clients need to fulfill to handle renames of test sequences
type SequenceRenameHandler interface {
	// HandlerSequenceRename allows dependents to handle renames of test sequences
	HandleSequenceRename(old, new id.TestID) error
}

// TestSequenceDeleter is used to delete testsequences from the storage.
type TestSequenceDeleter interface {
	// Delete removes the testsequence with the given ID for the given project under the given container
	Delete(sequenceID id.TestID) error
}

// CaseProtocolLister is used to list testcase execution protocols.
type CaseProtocolLister interface {
	// GetCaseExecutionProtocols gets the protocols for the testcase with given id,
	// which is part of the project with given id.
	GetCaseExecutionProtocols(testCaseID id.TestID) ([]test.CaseExecutionProtocol, error)
	GetCaseExecutionProtocol(protocolID id.ProtocolID) (test.CaseExecutionProtocol, error)
}

// SequenceProtocolLister is used to list testsequence execution protocols.
type SequenceProtocolLister interface {
	// GetSequenceExecutionProtocols gets the protocols for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocols(sequenceID id.TestID) ([]test.SequenceExecutionProtocol, error)
	// GetSequenceExecutionProtocol gets the protocol with the given id for the testsequence with given id,
	// which is part of the project with given id.
	GetSequenceExecutionProtocol(protocolID id.ProtocolID) (test.SequenceExecutionProtocol, error)
}

type ProjectRoleUpdater interface {
	ProjectRoleAdder
	ProjectRoleDeleter
}

type ProjectRoleAdder interface {
	Add(role *project.Role) error
}

type ProjectRoleDeleter interface {
	Delete(role *project.Role) error
}
