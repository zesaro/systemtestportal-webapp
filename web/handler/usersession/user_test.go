/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package usersession

import (
	"net/http"
	"testing"

	"net/http/httptest"
	"net/url"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
)

// SessionHandlerMock mocks a SessionHandler
type SessionHandlerMock struct {
	returnUser      *user.User
	StartForCalls   int
	EndForCalls     int
	GetCurrentCalls int
	startForError   error
	endForError     error
	getCurrentError error
}

// StartFor is a mock method
func (s *SessionHandlerMock) StartFor(w http.ResponseWriter, r *http.Request, u *user.User) error {
	s.StartForCalls++
	return s.startForError
}

// EndFor is a mock method
func (s *SessionHandlerMock) EndFor(w http.ResponseWriter, r *http.Request, u *user.User) error {
	s.EndForCalls++
	return s.endForError
}

// GetCurrent is a mock method
func (s *SessionHandlerMock) GetCurrent(r *http.Request) (*user.User, error) {
	s.GetCurrentCalls++
	return s.returnUser, s.getCurrentError
}

// configSet holds a test configuration.
type configSet struct {
	session *SessionHandlerMock
	auth    *handler.AuthMock
}

// testSet holds a test assumptions  applied to a test configuration.
type testSet struct {
	email           string
	password        string
	statusCode      int
	givenIdentifier string
	givenPassword   string
	StartForCalls   int
	EndForCalls     int
}

// NewSessionMock creates a new mock for a SessionHandler
func NewSessionMock(returnedUser *user.User, startForError error,
	endForError error, getCurrentError error) *SessionHandlerMock {
	return &SessionHandlerMock{
		returnedUser,
		0,
		0,
		0,
		startForError,
		endForError,
		getCurrentError,
	}
}

// tom is an example user for test purposes.
var simpleTom = user.New("T0m", "tom", "tom@example.com")
var tom = user.PasswordUser{
	Password: "1234",
	User:     simpleTom,
}

// gilbert is an example user for test purposes.
var simpleGilbert = user.New("Gilbert", "g1lb3rt", "gil@bert.com")
var gilbert = user.PasswordUser{
	Password: "13",
	User:     simpleGilbert,
}

// TestSignin_ServeHTTP test the sign in handler.
func TestSignin_ServeHTTP(t *testing.T) {
	t.Parallel()
	var tests = []struct {
		name string
		cs   configSet
		ts   testSet
	}{
		{
			"Valid sign in.",
			configSet{
				NewSessionMock(nil, nil, nil, nil),
				handler.NewAuthMock(&tom.User, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusOK,
				tom.EMail,
				tom.Password,
				1,
				0,
			},
		},
		{
			"Already signed in.",
			configSet{
				NewSessionMock(&tom.User, nil, nil, nil),
				handler.NewAuthMock(nil, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusBadRequest,
				"",
				"",
				0,
				0,
			},
		},
		{
			"Can't start session.",
			configSet{
				NewSessionMock(nil, handler.ErrTest, nil, nil),
				handler.NewAuthMock(nil, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusInternalServerError,
				tom.EMail,
				tom.Password,
				1,
				0,
			},
		},
		{
			"Can't get current session.",
			configSet{
				NewSessionMock(nil, nil, nil, handler.ErrTest),
				handler.NewAuthMock(nil, nil, true),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusInternalServerError,
				"",
				"",
				0,
				0,
			},
		},
		{
			"Error authenticating user.",
			configSet{
				NewSessionMock(nil, nil, nil, nil),
				handler.NewAuthMock(nil, handler.ErrTest, false),
			},
			testSet{
				tom.EMail,
				tom.Password,
				http.StatusInternalServerError,
				tom.EMail,
				tom.Password,
				0,
				0,
			},
		},
		{
			"Invalid sign in credentials.",
			configSet{
				NewSessionMock(nil, nil, nil, nil),
				handler.NewAuthMock(nil, nil, false),
			},
			testSet{
				tom.Name,
				tom.Password,
				http.StatusUnauthorized,
				tom.Name,
				tom.Password,
				0,
				0,
			},
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			testSigninSingleServeHTTP(t, test.cs, test.ts)
		})
	}
}

// testSigninSingleServeHTTP tests a single test case for the sign in handler.
func testSigninSingleServeHTTP(t *testing.T, cs configSet, ts testSet) {
	l := NewSigninHandler(cs.session, cs.auth)
	w := httptest.NewRecorder()
	r := generateRequest(t, ts.email, ts.password)
	l.ServeHTTP(w, r)
	result := w.Result()
	if result.StatusCode != ts.statusCode {
		t.Errorf("Wrong status code. Expected %d but was %d.", ts.statusCode, result.StatusCode)
		t.Errorf("Failed for: %+v", ts)
	}
	checkCredentials(t, ts.givenIdentifier, cs.auth.GivenIdentifier, ts)
	checkCredentials(t, ts.givenPassword, cs.auth.GivenPassword, ts)
	checkCalls(t, "Handler.EndFor", ts.EndForCalls, cs.session.EndForCalls, ts)
	checkCalls(t, "Handler.StartFor", ts.StartForCalls, cs.session.StartForCalls, ts)
}

// generateRequest generates a signin request with given credentials.
func generateRequest(t *testing.T, e string, pw string) *http.Request {
	r, err := http.NewRequest(http.MethodPost, "/signin", nil)
	if err != nil {
		t.Fatalf("Unable to create request: %s", err)
	}
	form := url.Values{}
	form.Add("inputIdentifier", e)
	form.Add("inputPassword", pw)
	r.Form = form
	return r
}

// TestSignout_ServeHTTP test the signout handlers serve function.
func TestSignout_ServeHTTP(t *testing.T) {
	t.Parallel()
	var tests = []struct {
		name          string
		s             *SessionHandlerMock
		startForCalls int
		endForCalls   int
		status        int
	}{
		{
			"Normal sign out.",
			NewSessionMock(&gilbert.User, nil, nil, nil),
			0,
			1,
			http.StatusOK,
		},
		{
			"Error getting session.",
			NewSessionMock(nil, nil, nil, handler.ErrTest),
			0,
			0,
			http.StatusInternalServerError,
		},
		{
			"No signed in user.",
			NewSessionMock(nil, nil, handler.ErrTest, nil),
			0,
			0,
			http.StatusBadRequest,
		},
		{
			"Error ending session.",
			NewSessionMock(&tom.User, nil, handler.ErrTest, nil),
			0,
			1,
			http.StatusInternalServerError,
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			l := NewSignoutHandler(test.s)
			w := httptest.NewRecorder()
			r := httptest.NewRequest(http.MethodPost, "/signin", nil)
			l.ServeHTTP(w, r)
			result := w.Result()
			if result.StatusCode != test.status {
				t.Errorf("Wrong status code. Expected %d but was %d.", test.status, result.StatusCode)
				t.Errorf("Failed for: Test: %+v with session mock %+v", test, test.s)
			}
			checkCalls(t, "Handler.EndFor", test.endForCalls, test.s.EndForCalls, test)
			checkCalls(t, "Handler.StartFor", test.startForCalls, test.s.StartForCalls, test)
		})
	}
}

// checkCredentials checks if a credential passed to Auth Matches the expectation.
// If not an error is passed to testing.
func checkCredentials(t *testing.T, exp string, act string, ts ...interface{}) {
	if exp != act {
		t.Errorf("Credentials passed to Auth don't match expectations. "+
			"Expected %s but was %s.", exp, act)
		t.Errorf("Failed for test case: %+v", ts)
	}
}

// checkCalls checks if the amount of calls to a method are as expected and creates an error message
// if not.
func checkCalls(t *testing.T, method string, exp int, act int, ts ...interface{}) {
	if exp != act {
		t.Errorf("Wrong amount of calls to %s. Expected %d but was %d.", method, exp, act)
		t.Errorf("Failed for test case: %+v", ts)
	}
}
