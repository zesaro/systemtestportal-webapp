/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

// ProjectsGet serves the page that is used to explore projects.
func ProjectsGet(p handler.ProjectLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var projectList []*project.Project
		contextEntities := handler.GetContextEntities(r)

		// If no user is logged in, only show public projects
		if contextEntities.User == nil {
			publicProjects, err := p.ListPublic()
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
			projectList = append(projectList, publicProjects...)
		} else {
			// If user is logged-in, show public, internal and the
			// private projects the logged-in user has access to
			actorProjects, err := p.ListForActor(contextEntities.User.ID())
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
			projectList = append(projectList, actorProjects...)
		}

		tmpl := getExploreProjectsTree()
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Projects, projectList), tmpl, w, r)
	}
}

// getExploreProjectsTree returns the list projects template with all parent templates
func getExploreProjectsTree() *template.Template {
	return handler.GetSideBarTree().
	// Explore projects tree
		Append(templates.ExploreProjects).
		Get().Lookup(templates.HeaderDef)
}
