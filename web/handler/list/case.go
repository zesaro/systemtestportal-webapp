/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/color"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/modal"
)

// CasesGet simply serves a page that is used to list testcases,
func CasesGet(lister handler.TestCaseLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Project == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		tcs, err := lister.List(c.Project.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		filter := r.FormValue(httputil.Filter)

		tcs, filt, err := filterCases(filter, tcs, c.Project)

		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		tmpl := getTestCaseListFragment(r)
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.Project, c.Project).
			With(context.Filtered, filt).
			With(context.TestCases, tcs).
			With(context.LabelColors, color.GetLabelColors()).
			With(context.DeleteLabels, modal.LabelDeleteMessage), tmpl, w, r)
	}
}

func filterCases(filter string, cases []*test.Case, proj *project.Project) ([]*test.Case, []project.Label, error) {
	var filters []project.Label

	// return immediately if there are no filters
	if filter == "[]" || filter == "" || !IsJSON(filter) {
		return cases, filters, nil
	}

	// get applied filters from JSON string
	var values []string
	bytes := []byte(filter)

	err := json.Unmarshal(bytes, &values)

	if err != nil {
		return cases, filters, err
	}

	// turn string array into project.Label array
	for _, f := range values {
		label, _ := proj.GetLabelByName(f)
		filters = append(filters, project.Label{
			Name:  f,
			Description: label.Description,
			Color: label.Color,
		})
	}

	i := 0
	for _, c := range cases {
		if containsLabels(filters, &c.Labels) {
			cases[i] = c
			i++
		}
	}
	cases = cases[:i]

	return cases, filters, nil
}

// IsJSON checks whether a given string is in JSON format
func IsJSON(str string) bool {
	var js json.RawMessage
	return json.Unmarshal([]byte(str), &js) == nil
}

// getTestCaseListFragment returns either only the test case list fragment or the fragment with all parent templates,
// depending of the "fragment" parameter in the request
func getTestCaseListFragment(r *http.Request) *template.Template {
	if httputil.IsFragmentRequest(r) {
		return getTabTestCasesListFragment()
	}
	return getTabTestCasesListTree()
}

// getTabTestCasesListTree returns the test case list tab template with all parent templates
func getTabTestCasesListTree() *template.Template {
	return handler.GetNoSideBarTree().
		// Project tabs tree
		Append(templates.ContentProjectTabs).
		// Tab test case list tree
		Append(templates.TestCasesList, templates.ManageLabels, templates.DeletePopUp).
		Get().Lookup(templates.HeaderDef)
}

// getTabTestCasesListFragment returns only the test case list tab template
func getTabTestCasesListFragment() *template.Template {
	return handler.GetBaseTree().
		Append(templates.TestCasesList, templates.ManageLabels, templates.DeletePopUp).
		Get().Lookup(templates.TabContent)
}
