/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package list

import (
	"net/http"
	"net/url"
	"testing"

	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestSequencesGet(t *testing.T) {
	filterParams := url.Values{}
	filterParams.Add(httputil.Filter, "[\"label\"]")
	invalidFilterParams := url.Values{}
	invalidFilterParams.Add(httputil.Filter, "invalid")
	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)
	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: handler.DummyProject,
		},
	)
	ctxPrivateProject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:    handler.DummyUserUnauthorized,
			middleware.ProjectKey: handler.DummyProjectPrivate,
		},
	)

	handler.Suite(t,
		handler.CreateTest("Invalid context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusInternalServerError),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(invalidCtx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(invalidCtx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Test filter",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, filterParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, filterParams),
		),
		handler.CreateTest("Test invalid filter",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, invalidFilterParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, invalidFilterParams),
		),
		handler.CreateTest("No member of project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusForbidden),
						handler.HasCalls(&mock, 0),
					)
			},
			handler.SimpleRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctxPrivateProject, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Normal sequence",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				mock := handler.SequenceListerMock{}
				return SequencesGet(&mock),
					handler.Matches(
						handler.HasStatus(http.StatusOK),
						handler.HasCalls(&mock, 1),
					)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
			handler.SimpleFragmentRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}
