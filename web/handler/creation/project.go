/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package creation

import (
	"net/http"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
)

// The error-messages for creating a project
const (
	errCanNotAddProjectTitle = "Couldn't create project."
	errCanNotAddProject      = "We are sorry but we were unable to create the project as you requested." +
		"If you believe this is a bug please contact us via our " + handler.IssueTracker + "."
)

// ProjectPost is used to save a new project in the system
func ProjectPost(pa handler.ProjectAdder, projectChecker id.ProjectExistenceChecker) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		pn := r.FormValue(httputil.ProjectName)
		pd := r.FormValue(httputil.ProjectDescription)
		pvs := r.FormValue(httputil.ProjectVisibility)
		pv, err := visibility.StringToVis(pvs)
		if err != nil {
			errors.Handle(handler.InvalidVisibility(), w, r)
			return
		}

		c := handler.GetContextEntities(r)
		if c.User == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		p := project.NewProject(pn, c.User.ID(), pd, pv)
		if vErr := p.ID().Validate(projectChecker); vErr != nil {
			errors.Handle(vErr, w, r)
			return
		}

		// Add owner as a member with the role "Supervisor"
		p.UserMembers[c.User.ID()] = project.UserMembership{
			User:        c.User.ID(),
			Role:        "Owner",
			MemberSince: time.Now().UTC().Round(time.Second),
		}

		err = pa.Add(&p)
		if err != nil {
			errors.ConstructStd(http.StatusInternalServerError,
				errCanNotAddProjectTitle, errCanNotAddProject, r).
				WithLog("Unable to add project to store.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		// The header values are used to set the url with javascript
		httputil.SetHeaderValue(w, httputil.NewName, p.Name)
		httputil.SetHeaderValue(w, httputil.SignedInUser, c.User.Name)
	}
}
