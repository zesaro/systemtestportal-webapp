/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package todo

import (
	"encoding/json"
	"html/template"
	"net/http"

	"gitlab.com/stp-team/systemtestportal-webapp/web/context"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/templates"
)

const (
	noSignedInUser    = "No signed-in user"
	noSignedInUserMsg = "You have to be signed-in to see your TODOs"
)

const (
	errCouldNotDecodeTodoItems    = "Could not update todos"
	errCouldNotDecodeTodoItemsMsg = "We were unable to decode the change to the todos " +
		"send in your request. This ist most likely a bug. If you want please " +
		"contact us via our " + handler.IssueTracker + "."
)

// ListGet displays the list with the todos of the signed-in user.
func ListGet(todoListGetter handler.TODOListGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		contextEntities := handler.GetContextEntities(r)
		if contextEntities.User == nil {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, noSignedInUser, noSignedInUserMsg, r).
					WithLog("todo list not available because no user is signed-in").
					WithStackTrace(1).
					Finish(), w, r)
			return
		}

		todoList, err := todoListGetter.Get(contextEntities.User.Name)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		template := getTODOTree()
		handler.PrintTmpl(context.New().
			WithUserInformation(r).
			With(context.TodoList, *todoList),
			template, w, r)
	}
}

// getTODOTree returns the template for the TODOs of a user
func getTODOTree() *template.Template {
	return handler.GetNoSideBarTree().Append(templates.Todo).Get().Lookup(templates.HeaderDef)
}

// ItemPut sets a todo item as done
func ItemPut(todoListGetter handler.TODOListGetter, todoListAdder handler.TODOListAdder) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		contextEntities := handler.GetContextEntities(r)
		if contextEntities.User == nil {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, noSignedInUser, noSignedInUserMsg, r).
					WithLog("cant update todos because no user is signed-in").
					WithStackTrace(1).
					Finish(), w, r)
			return
		}

		var itemIndex int
		if err := json.NewDecoder(r.Body).Decode(&itemIndex); err != nil {
			errors.ConstructStd(http.StatusBadRequest,
				errCouldNotDecodeTodoItems, errCouldNotDecodeTodoItemsMsg, r).
				WithLog("Couldn't read todo items from request.").
				WithStackTrace(1).
				WithCause(err).
				WithRequestDump(r).
				Respond(w)
			return
		}

		todoList, err := todoListGetter.Get(contextEntities.User.Name)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		for i, item := range todoList.TODOs {
			if item.Index == itemIndex {
				todoList.TODOs[i].Done = true
				break
			}
		}

		// Save updated list
		if err = todoListAdder.Add(*todoList); err != nil {
			errors.Handle(err, w, r)
			return
		}
	}
}
