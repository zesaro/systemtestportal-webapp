/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package todo

import (
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/todo"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func TestListGet(t *testing.T) {
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: nil,
		},
	)

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	handler.Suite(t,
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{}
				return ListGet(todoListGetterMock), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Getter returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{
					Err:      errors.New("error getting todo-list"),
					TodoList: &todo.List{},
				}
				return ListGet(todoListGetterMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
		handler.CreateTest("Valid request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{
					TodoList: &todo.List{},
				}
				return ListGet(todoListGetterMock), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.SimpleRequest(ctx, http.MethodGet, handler.NoParams),
		),
	)
}

func TestItemPut(t *testing.T) {
	ctxNoUser := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: nil,
		},
	)

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey: handler.DummyUser,
		},
	)

	invalidBody := "INVALIDNR"

	body := "1"

	handler.Suite(t,
		handler.CreateTest("No user in context",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{}
				todoListAdderMock := handler.TODOListAdderMock{}
				return ItemPut(todoListGetterMock, todoListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.SimpleRequest(ctxNoUser, http.MethodPut, handler.NoParams),
		),
		handler.CreateTest("Invalid body",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{}
				todoListAdderMock := handler.TODOListAdderMock{}
				return ItemPut(todoListGetterMock, todoListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusBadRequest),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, invalidBody),
		),
		handler.CreateTest("Getter returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{
					Err: errors.New("error getting todo list for user"),
				}
				todoListAdderMock := handler.TODOListAdderMock{}
				return ItemPut(todoListGetterMock, todoListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Adder returns error",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{
					TodoList: &todo.List{
						Username: "",
						TODOs: []todo.Item{
							{
								Index: 1,
							},
						},
					},
				}
				todoListAdderMock := handler.TODOListAdderMock{
					Err: errors.New("error adding todo-list to store"),
				}
				return ItemPut(todoListGetterMock, todoListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusInternalServerError),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
		handler.CreateTest("Valid request",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				todoListGetterMock := handler.TODOListGetterMock{
					TodoList: &todo.List{
						Username: "",
						TODOs: []todo.Item{
							{
								Index: 1,
							},
						},
					},
				}
				todoListAdderMock := handler.TODOListAdderMock{}
				return ItemPut(todoListGetterMock, todoListAdderMock), handler.Matches(
					handler.HasStatus(http.StatusOK),
				)
			},
			handler.NewRequest(ctx, http.MethodPut, handler.NoParams, body),
		),
	)
}
