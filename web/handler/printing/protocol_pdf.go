/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package printing

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"strconv"
	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/web/errors"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"

	"github.com/jung-kurt/gofpdf"
)

// ProtocolCasePdf creates and serves a pdf for a case protocol
func ProtocolCasePdf(l handler.CaseProtocolLister) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Case == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		protocols, err := l.GetCaseExecutionProtocols(c.Case.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		r.ParseForm()

		protocolNr := int64(0)
		if r.FormValue(httputil.ProtocolNr) != "" {
			protocolNr, err = strconv.ParseInt(r.FormValue(httputil.ProtocolNr), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
		}

		testVersion := int64(0)
		if r.FormValue(httputil.TestVersion) != "" {
			testVersion, err = strconv.ParseInt(r.FormValue(httputil.TestVersion), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}

		}

		var protocol test.CaseExecutionProtocol
		for _, prot := range protocols {
			if prot.ProtocolNr == int(protocolNr) && prot.TestVersion.TestVersion() == int(testVersion) {
				protocol = prot
				break
			}
		}

		// check whether protocol is empty or not
		if reflect.DeepEqual(protocol, test.CaseExecutionProtocol{}) {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, "Protocol not found", "We couldn't find the requested protocol", r).
					WithLog("Invalid parameters sent by client.").
					WithStackTrace(2).
					Finish(), w, r)
			return
		}

		buf, err := buildPdfForCaseProtocol(protocol, *c.Case)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		filename := c.Case.ID().Project() + "-" + c.Case.ID().Test() + "-" + strconv.Itoa(int(testVersion)) + "-" + strconv.Itoa(int(protocolNr)) + ".pdf"
		filename = removeCommas(filename)

		w.Header().Set("Content-Disposition", "attachment; filename="+filename)
		w.Header().Set("Content-Type", "application/pdf")

		io.Copy(w, buf)
	}
}

// ProtocolSequencePdf creates and serves a pdf for a sequence protocol
func ProtocolSequencePdf(l handler.SequenceProtocolLister, lc handler.CaseProtocolLister, tg handler.TestCaseGetter) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		c := handler.GetContextEntities(r)
		if c.Sequence == nil {
			errors.Handle(c.Err, w, r)
			return
		}

		if !c.Project.GetPermissions(c.User).DisplayProject {
			errors.Handle(handler.UnauthorizedAccess(r), w, r)
			return
		}

		protocols, err := l.GetSequenceExecutionProtocols(c.Sequence.ID())
		if err != nil {
			errors.Handle(err, w, r)
			return
		}
		r.ParseForm()

		protocolNr := int64(0)
		if r.FormValue(httputil.ProtocolNr) != "" {
			protocolNr, err = strconv.ParseInt(r.FormValue(httputil.ProtocolNr), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
		}

		testVersion := int64(0)
		if r.FormValue(httputil.TestVersion) != "" {
			testVersion, err = strconv.ParseInt(r.FormValue(httputil.TestVersion), 10, 32)
			if err != nil {
				errors.Handle(err, w, r)
				return
			}
		}

		var protocol test.SequenceExecutionProtocol
		for _, prot := range protocols {
			if prot.ProtocolNr == int(protocolNr) && prot.TestVersion.TestVersion() == int(testVersion) {
				protocol = prot
				break
			}
		}

		// check whether protocol is empty or not
		if reflect.DeepEqual(protocol, test.SequenceExecutionProtocol{}) {
			errors.Handle(
				errors.ConstructStd(http.StatusBadRequest, "Protocol not found", "We couldn't find the requested protocol", r).
					WithLog("Invalid parameters sent by client.").
					WithStackTrace(2).
					Finish(), w, r)
			return
		}

		buf, err := buildPdfForSequenceProtocol(protocol, *c.Sequence, lc, tg)
		if err != nil {
			errors.Handle(err, w, r)
			return
		}

		filename := c.Sequence.ID().Project() + "-" + c.Sequence.ID().Test() + "-" + strconv.Itoa(int(testVersion)) + "-" + strconv.Itoa(int(protocolNr)) + ".pdf"
		// replace commas to prevent multiple content dispositions
		filename = removeCommas(filename)

		w.Header().Set("Content-Disposition", "attachment; filename="+filename)
		w.Header().Set("Content-Type", "application/pdf")

		io.Copy(w, buf)
	}
}

// buildPdfForCaseProtocol returns an io.Reader object for a pdf containing the protocol test.CaseExecutionProtocol information for the testCase test.Case
func buildPdfForCaseProtocol(protocol test.CaseExecutionProtocol, testCase test.Case) (io.Reader, error) {
	pdf := gofpdf.New("P", "mm", "A4", "")

	setUpDocument(pdf)
	addProtocolInfoCase(pdf, protocol, testCase)
	addCaseInfo(pdf, protocol, testCase)
	addCaseProtocolResults(pdf, protocol, testCase)

	buf := new(bytes.Buffer)
	err := pdf.Output(buf)
	if err != nil {
		return buf, err
	}
	return buf, nil
}

// buildPdfForSequenceProtocol returns an io.Reader object for a pdf containing the protocol test.SequenceExecutionProtocol information for the testSequence test.Sequence
func buildPdfForSequenceProtocol(protocol test.SequenceExecutionProtocol, testSequence test.Sequence, lc handler.CaseProtocolLister, tg handler.TestCaseGetter) (io.Reader, error) {
	pdf := gofpdf.New("P", "mm", "A4", "")

	setUpDocument(pdf)
	addProtocolInfoSequence(pdf, protocol, testSequence)
	addSequenceInfo(pdf, protocol, testSequence)
	addSequenceProtocolResults(pdf, protocol, testSequence, lc, tg)

	buf := new(bytes.Buffer)
	err := pdf.Output(buf)
	if err != nil {
		return buf, err
	}
	return buf, nil
}

func setUpDocument(pdf *gofpdf.Fpdf) {
	// reference margin, 1cm
	margin := 28.35 / (72.0 / 25.4)
	pdf.SetMargins(2.5*margin, margin, 2.5*margin)
	pdf.SetFooterFunc(func() {
		// Position at 1.5 cm from bottom
		pdf.SetY(-15)
		// Arial italic 8
		pdf.SetFont("Arial", "I", 8)
		pdf.CellFormat(0, 10, "systemtestportal.org", "", 0, "L", false, 0, "systemtestportal.org")
		pdf.CellFormat(0, 10, fmt.Sprintf("Page %d", pdf.PageNo()),
			"", 0, "C", false, 0, "")
	})
	pdf.AddPage()
}

func addProtocolInfoCase(pdf *gofpdf.Fpdf, protocol test.CaseExecutionProtocol, testCase test.Case) {
	pdf.SetFont("Arial", "B", 20)
	string := protocol.TestVersion.TestID.Test() + ", Version: " + strconv.Itoa(protocol.TestVersion.TestVersion()) + "\n"
	pdf.Write(10, string)
	pdf.SetFont("Arial", "B", 14)
	string = "Project: " + protocol.TestVersion.Project() + "\n"
	pdf.Write(8, string)
	string = "Result: " + protocol.Result.String() + "\n"
	pdf.Write(8, string)
	string = "Comment: " + protocol.Comment + "\n"
	pdf.Write(8, string)
	pdf.SetFont("Arial", "", 12)
	string = "ProtocolNr: " + strconv.Itoa(protocol.ProtocolNr) + "\n"
	pdf.Write(6, string)
	string = "Executed on: " + protocol.ExecutionDate.String() + "\n"
	pdf.Write(6, string)
	string = "Tester: " + protocol.UserName + "\n"
	pdf.Write(6, string)
	string = "SUTVersion: " + protocol.SUTVersion + "\n"
	pdf.Write(6, string)
	string = "SUTVariant: " + protocol.SUTVariant + "\n\n"
	pdf.Write(6, string)

}

func addCaseInfo(pdf *gofpdf.Fpdf, protocol test.CaseExecutionProtocol, testCase test.Case) {
	pdf.SetFont("Arial", "B", 14)
	pdf.Write(8, "Test Case Description:"+"\n")
	pdf.SetFont("Arial", "", 10)
	pdf.Write(6, testCase.TestCaseVersions[len(testCase.TestCaseVersions)-protocol.TestVersion.TestVersion()].Description+"\n")
	pdf.SetFont("Arial", "B", 14)
	pdf.Write(8, "Test Case Preconditions:"+"\n")
	for _, preconditionResult := range protocol.PreconditionResults {
		pdf.SetFont("Arial", "", 10)
		pdf.Write(6, "- "+preconditionResult.Precondition.Content)
		pdf.SetFont("Arial", "I", 10)
		pdf.Write(6, " ("+preconditionResult.Result+")\n")
	}
	pdf.SetFont("Arial", "B", 14)
}

func addCaseProtocolResults(pdf *gofpdf.Fpdf, protocol test.CaseExecutionProtocol, testCase test.Case) {
	pdf.Write(8, "Test Step Results:"+"\n")

	for index, testStepProtocol := range protocol.StepProtocols {
		pdf.SetFont("Arial", "B", 12)
		pdf.Write(6, strconv.Itoa(index+1)+". ")
		pdf.Write(6, testCase.TestCaseVersions[len(testCase.TestCaseVersions)-protocol.TestVersion.TestVersion()].Steps[index].Action+"\n")
		pdf.SetFont("Arial", "", 10)
		pdf.Write(6, "Result: "+testStepProtocol.Result.String()+"\n")
		pdf.Write(6, "Expected: "+testCase.TestCaseVersions[len(testCase.TestCaseVersions)-protocol.TestVersion.TestVersion()].Steps[index].ExpectedResult+"\n")
		pdf.Write(6, "Observed: "+testStepProtocol.ObservedBehavior+"\n")
	}
}

func addProtocolInfoSequence(pdf *gofpdf.Fpdf, protocol test.SequenceExecutionProtocol, testSequence test.Sequence) {
	pdf.SetFont("Arial", "B", 20)
	string := protocol.TestVersion.TestID.Test() + ", Version: " + strconv.Itoa(protocol.TestVersion.TestVersion()) + "\n"
	pdf.Write(10, string)
	pdf.SetFont("Arial", "B", 14)
	string = "Project: " + protocol.TestVersion.Project() + "\n"
	pdf.Write(8, string)
	string = "Result: " + protocol.Result.String() + "\n"
	pdf.Write(8, string)
	pdf.SetFont("Arial", "", 12)
	string = "ProtocolNr: " + strconv.Itoa(protocol.ProtocolNr) + "\n"
	pdf.Write(6, string)
	string = "Executed on: " + protocol.ExecutionDate.String() + "\n"
	pdf.Write(6, string)
	string = "Tester: " + protocol.UserName + "\n"
	pdf.Write(6, string)
	string = "SUTVersion: " + protocol.SUTVersion + "\n"
	pdf.Write(6, string)
	string = "SUTVariant: " + protocol.SUTVariant + "\n\n"
	pdf.Write(6, string)

}

func addSequenceInfo(pdf *gofpdf.Fpdf, protocol test.SequenceExecutionProtocol, testSequence test.Sequence) {
	pdf.SetFont("Arial", "B", 14)
	pdf.Write(8, "Test Sequence Description:"+"\n")
	pdf.SetFont("Arial", "", 10)
	pdf.Write(6, testSequence.SequenceVersions[len(testSequence.SequenceVersions)-protocol.TestVersion.TestVersion()].Description+"\n")
	pdf.SetFont("Arial", "B", 14)
	pdf.Write(8, "Test Sequence Preconditions:"+"\n")
	pdf.SetFont("Arial", "", 10)
	for _, preconditionResult := range protocol.PreconditionResults {
		pdf.SetFont("Arial", "", 10)
		pdf.Write(6, "- "+preconditionResult.Precondition.Content)
		pdf.SetFont("Arial", "I", 10)
		pdf.Write(6, " ("+preconditionResult.Result+")\n")
	}
	pdf.SetFont("Arial", "B", 14)
}

func addSequenceProtocolResults(pdf *gofpdf.Fpdf, protocol test.SequenceExecutionProtocol, testSequence test.Sequence, caseProtocolLister handler.CaseProtocolLister, caseGetter handler.TestCaseGetter) error {
	for _, testCaseProtocolID := range protocol.CaseExecutionProtocols {
		testCaseProtocol, err := caseProtocolLister.GetCaseExecutionProtocol(testCaseProtocolID)
		if err != nil {
			return err
		}
		testCase, ex, err := caseGetter.Get(testCaseProtocol.TestVersion.TestID)
		if err != nil {
			return err
		}
		if !ex {
			return fmt.Errorf("Test Case not found")
		}
		pdf.AddPage()
		pdf.SetFont("Arial", "B", 20)
		addProtocolInfoCase(pdf, testCaseProtocol, *testCase)
		addCaseInfo(pdf, testCaseProtocol, *testCase)
		addCaseProtocolResults(pdf, testCaseProtocol, *testCase)
	}
	return nil
}

func removeCommas(filename string) string {
	return strings.Replace(filename, ",", "", -1)
}
