/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package export

import (
	"testing"
	"net/url"
	"gitlab.com/stp-team/systemtestportal-webapp/web/httputil"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"net/http"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
	"reflect"
	"io"
)

func TestProjectJsonExport(t *testing.T) {

	params := url.Values{}
	params.Add(httputil.ProjectName, "1")

	invalidCtx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey: nil,
		},
	)

	ctx := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.ProjectKey:  handler.DummyProject,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	ctxPrivatePrject := handler.SimpleContext(
		map[interface{}]interface{}{
			middleware.UserKey:     handler.DummyUserUnauthorized,
			middleware.ProjectKey:  handler.DummyProjectPrivate,
			middleware.TestCaseKey: handler.DummyTestCase,
		},
	)

	handler.Suite(t, handler.CreateTest("Empty context",
		func() (http.HandlerFunc, []handler.ResponseMatcher) {
			p := &handler.CaseListerMock{}
			return ProjectJson(p), handler.Matches(
				handler.HasStatus(http.StatusForbidden),
				handler.HasCalls(p, 0),
			)
		},
		handler.EmptyRequest(http.MethodGet),
		handler.SimpleFragmentRequest(invalidCtx, http.MethodPost, handler.NoParams),
		),
		handler.CreateTest("No member of private project",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.CaseListerMock{}
				return ProjectJson(p), handler.Matches(
					handler.HasStatus(http.StatusForbidden),
					handler.HasCalls(p, 0),
				)
			},
			handler.SimpleRequest(ctxPrivatePrject, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctxPrivatePrject, http.MethodPost, params),
		),
		handler.CreateTest("Normal case",
			func() (http.HandlerFunc, []handler.ResponseMatcher) {
				p := &handler.CaseListerMock{}
				return ProjectJson(p), handler.Matches(
					handler.HasStatus(http.StatusOK),
					handler.HasCalls(p, 1),
				)
			},
			handler.SimpleRequest(ctx, http.MethodPost, params),
			handler.SimpleFragmentRequest(ctx, http.MethodPost, params),
		),

	)
}

func TestBuildJsonForProject(t *testing.T) {

	p := &handler.CaseListerMock{}
	mockProject := &dummydata.Projects[0]
	mockCase := GetCases(p, mockProject)

	reader, err := buildJsonForProject(mockProject, mockCase)

	if err != nil {
		t.Errorf("Error while building json" + err.Error())
	}
	if reflect.DeepEqual(reader, new(io.Reader)) {
		t.Errorf("Expected populated io.Reader, however it is empty")
	}
}
