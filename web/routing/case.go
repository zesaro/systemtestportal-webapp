/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/store/comment"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/assignment"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/creation"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/deletion"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/duplication"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/execution"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/printing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/update"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
	"gitlab.com/stp-team/systemtestportal-webapp/web/sessions"
)

func registerCasesHandler(pg *contextGroup, n *negroni.Negroni, tcs store.Cases) {
	tcStore := store.GetCaseStore()

	tcsg := wrapContextGroup(pg.NewContextGroup(Cases))

	tcsg.HandlerGet(Show,
		n.With(negroni.WrapFunc(list.CasesGet(tcStore))))
	tcsg.HandlerGet(New,
		n.With(negroni.WrapFunc(display.CreateCaseGet)))
	tcsg.HandlerGet(Save,
		n.With(negroni.Wrap(defaultRedirect("../"))))
	tcsg.HandlerGet(JSON,
		n.With(negroni.WrapFunc(json.CasesGet(tcStore))))
	tcsg.HandlerGet(Print,
		n.With(negroni.WrapFunc(printing.CasesListGet(tcStore))))

	tcsg.HandlerPost(Save,
		n.With(negroni.WrapFunc(creation.CasePost(tcStore, tcStore))))

	registerCaseHandler(tcsg, n, tcs)
}

func registerCaseHandler(tcsg *contextGroup, n *negroni.Negroni, tcs store.Cases) {
	us := store.GetUserStore()

	session := sessions.GetSessionStore()
	protocol := store.GetProtocolStore()

	tcStore := store.GetCaseStore()
	cs := comment.GetStore()
	todoStore := store.GetTodoStore()

	tcg := wrapContextGroup(tcsg.NewContextGroup(VarCase))

	tcg.HandlerGet(Show, n.With(middleware.Testcase(tcStore),
		middleware.Comments(cs, true), negroni.WrapFunc(display.ShowCaseGet)))
	tcg.HandlerPut(Show, n.With(middleware.Testcase(tcStore),
		middleware.Comments(cs, true), negroni.WrapFunc(creation.CaseCommentPut(cs))))
	tcg.HandlerGet(Edit,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(display.EditCaseGet)))
	tcg.HandlerGet(History,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(display.HistoryCaseGet)))
	tcg.HandlerGet(JSON,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(json.CaseGet)))
	tcg.HandlerGet(Print,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(printing.CaseGet)))

	tcg.HandlerPost(Duplicate,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(duplication.CasePost(tcStore, tcStore))))

	tcg.HandlerPut(Update,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(update.CasePut(tcStore, tcStore))))
	tcg.HandlerPut(Tester,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(assignment.CasePut(us, todoStore, todoStore))))

	tcg.HandlerDelete("",
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(deletion.CaseDelete(tcStore))))

	registerCaseExecuteHandler(tcg, n, tcs, session, protocol)
	registerCaseLabelsHandler(tcg, n, tcs)
}

func registerCaseExecuteHandler(tcg *contextGroup, n *negroni.Negroni,
	tcs middleware.TestCaseStore, session *sessions.Store, protocol store.Protocols) {

	tcg.HandlerGet(Execute,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(execution.CaseStartPageGet(session, session))))
	tcg.HandlerPost(Execute,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(execution.CaseExecutionPost(protocol, protocol, session,
			session, nil, nil, nil, nil, tcs))))
}

func registerCaseLabelsHandler(tcg *contextGroup, n *negroni.Negroni, tcs store.Cases) {

	tcg.HandlerGet(Labels,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(json.CaseLabelsGet)))
	tcg.HandlerPost(Labels,
		n.With(middleware.Testcase(tcs), negroni.WrapFunc(json.CaseLabelsPost(tcs))))
}
