/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

import (
	"github.com/urfave/negroni"
	"gitlab.com/stp-team/systemtestportal-webapp/store"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/display"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/json"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/list"
	"gitlab.com/stp-team/systemtestportal-webapp/web/handler/printing"
	"gitlab.com/stp-team/systemtestportal-webapp/web/middleware"
)

func registerProtocolsHandler(pg *contextGroup, n *negroni.Negroni,
	tcStore store.Cases, tsStore store.Sequences, protocolStore store.Protocols) {

	prtg := wrapContextGroup(pg.NewContextGroup(Protocols))

	prtg.HandlerGet(Show,
		n.With(negroni.WrapFunc(list.ProtocolsGet(tcStore, tsStore))))
	prtg.HandlerGet(Print,
		n.With(negroni.WrapFunc(printing.ProtocolsListGet(tcStore, tsStore))))

	registerCaseProtocolsHandler(prtg, n, tcStore, protocolStore)
	registerSequenceProtocolsHandler(prtg, n, tsStore, protocolStore, tcStore)
	registerProtocolsJSONHandler(pg, n, tcStore, tsStore, protocolStore)
}

func registerCaseProtocolsHandler(prtg *contextGroup, n *negroni.Negroni, caseStore store.Cases, protocolStore store.Protocols) {

	prtg.HandlerGet(Cases+Print,
		n.With(negroni.WrapFunc(printing.CaseProtocolsGet)))
	prtg.HandlerPost(Cases+"/:testcase/:protocol"+PrintPdf,
		n.With(middleware.Testcase(caseStore), negroni.WrapFunc(printing.ProtocolCasePdf(protocolStore))))
	prtg.HandlerGet(Cases+"/:testcase/:protocol",
		n.With(middleware.Testcase(caseStore), middleware.CaseProtocol(protocolStore),
			negroni.WrapFunc(display.CaseProtocolsGet(protocolStore))))
}

func registerSequenceProtocolsHandler(prtg *contextGroup, n *negroni.Negroni,
	tsStore middleware.TestSequenceStore, protocolStore store.Protocols, caseStore middleware.TestCaseStore) {

	prtg.HandlerGet(Sequences+Print,
		n.With(negroni.WrapFunc(printing.SequenceProtocolsGet)))
	prtg.HandlerPost(Sequences+"/:testsequence/:protocol"+PrintPdf,
		n.With(middleware.TestSequence(tsStore), negroni.WrapFunc(printing.ProtocolSequencePdf(protocolStore, protocolStore, caseStore))))
	prtg.HandlerGet(Sequences+"/:testsequence/:protocol",
		n.With(middleware.TestSequence(tsStore), middleware.SequenceProtocol(protocolStore),
			negroni.WrapFunc(display.SequenceProtocolsGet(protocolStore))))
}

func registerProtocolsJSONHandler(pg *contextGroup, n *negroni.Negroni,
	tcStore middleware.TestCaseStore, tsStore middleware.TestSequenceStore, protocolStore store.Protocols) {

	pg.HandlerGet("/protocols/testcases/:testcase",
		n.With(middleware.Testcase(tcStore), negroni.WrapFunc(json.CaseProtocolsGet(protocolStore))))
	pg.HandlerGet("/protocols/testsequences/:testsequence",
		n.With(middleware.TestSequence(tsStore), negroni.WrapFunc(json.SequenceProtocolsGet(protocolStore))))
}
