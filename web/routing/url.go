/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package routing

// The different static parts a url can be made of
const (
	Register = "/register"
	SignIn   = "/signin"
	SignOut  = "/signout"

	About = "/about"

	Explore = "/explore"

	Dashboard = "/dashboard"
	Todos     = "/todos"

	Permission = "/permission"

	Projects = "/projects"
	Users    = "/users"
	Groups   = "/groups"

	Cases     = "/testcases"
	Sequences = "/testsequences"
	Protocols = "/protocols"
	Members   = "/members"
)

// The actions that can be taken on an item
const (
	Show               = "/"
	New                = "/new"
	Edit               = "/edit"
	Update             = "/update"
	Duplicate          = "/duplicate"
	History            = "/history"
	Execute            = "/execute"
	Save               = "/save"
	Print              = "/print"
	PrintPdf           = "/pdf"
	JSON               = "/json"
	Info               = "/info"
	Versions           = "/versions"
	Settings           = "/settings"
	PermissionSettings = "/settings/permission"
	Labels             = "/labels"
	Roles              = "/roles"
	Add                = "/add"
	Remove             = "/remove"
	Tester             = "/tester"
	SequenceDashboard  = "/sequences"
	Export			   = "/export"
)

// The variables a url part can be
const (
	VarContainer = "/:container"
	VarProject   = "/:project"
	VarSequence  = "/:testsequence"
	VarCase      = "/:testcase"
)
