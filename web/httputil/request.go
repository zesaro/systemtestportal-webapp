/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package httputil

import (
	"fmt"
	"net/http"
	"net/http/httputil"
	"strconv"
)

// The parameter keys of a request
const (
	// User
	Identifier  = "inputIdentifier"
	Password    = "inputPassword"
	DisplayName = "inputDisplayName"
	UserName    = "inputUserName"
	Email       = "inputEmail"

	// TODOs
	Todos = "todos"

	// Project
	ProjectName        = "inputProjectName"
	ProjectDescription = "inputProjectDesc"
	ProjectImage       = "inputProjectLogo"
	ProjectVisibility  = "optionsProjectVisibility"
	ProjectRoles       = "inputProjectRoles"

	Version = "version"
	Filter  = "filter"

	Label = "inputTestSequenceLabels"

	// TestCases
	TestCaseName = "inputTestCaseName"
	TestVersion  = "testVersion"

	// TestSequences
	TestSequenceName          = "inputTestSequenceName"
	TestSequenceDescription   = "inputTestSequenceDescription"
	TestSequencePreconditions = "inputTestSequencePreconditions"
	TestSequenceLabels        = "inputTestSequenceLabels"
	TestSequenceTestCase      = "inputTestSequenceTestCase"
	NewTestCases              = "newTestcases"
	CommitMessage             = "inputCommitMessage"
	IsMinor                   = "isMinor"

	// Members
	Members = "members"
	Roles   = "roles"

	Member = "member"
	Role   = "role"

	// Assignment
	NewTesters = "newtesters"

	// Protocols
	ProtocolType        = "type"
	SelectedProtocol    = "selected"
	PreconditionResults = "preconditions"
	ProtocolNr          = "protocolNr"

	// Group
	GroupName        = "inputGroupName"
	GroupDescription = "inputGroupDesc"
	GroupVisibility  = "inputGroupVisibility"

	// Comment
	CommentText = "commentText"

	Fragment = "fragment"
)

// DumpResponse is like DumpRequest but for responses.
func DumpResponse(r *http.Response) string {
	d, err := httputil.DumpResponse(r, true)
	if err != nil {
		return fmt.Sprintf("%+v", r)
	}
	return string(d)
}

// DumpRequest dumps given request into printable string.
func DumpRequest(r *http.Request) string {
	d, err := httputil.DumpRequest(r, true)
	if err != nil {
		return fmt.Sprintf("%+v", r)
	}
	return string(d)
}

// IsFragmentRequest checks if the request contains a parameter "fragment".
// If the parameter is one of 1, t, T, TRUE, true or True the function returns true,
// else it returns false
func IsFragmentRequest(r *http.Request) bool {
	if r == nil {
		return false
	}
	if frag := r.FormValue(Fragment); frag != "" {
		isFrag, err := strconv.ParseBool(frag)
		if err == nil {
			return isFrag
		}
	}
	return false
}
