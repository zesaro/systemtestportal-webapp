/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package context

// The keys for the context
const (
	Version = "version"

	// TODOs
	TodoList = "TodoList"

	// Dashboard
	Dashboard          = "Dashboard"
	DashboardSequences = "DashboardSequences"

	// Explore
	Projects = "Projects"
	Groups   = "Groups"
	Users    = "Users"

	// Project
	Project = "Project"

	TestCases     = "TestCases"
	TestSequences = "TestSequences"

	// Dashboard
	Variants = "Variants"
	Versions = "Versions"
	// TestCase
	TestCase        = "TestCase"
	TestCaseVersion = "TestCaseVersion"
	DurationHours   = "DurationHours"
	DurationMin     = "DurationMin"
	DurationSec     = "DurationSec"

	// TestSequence
	TestSequence        = "TestSequence"
	TestSequenceVersion = "TestSequenceVersion"
	SequenceInfo        = "SequenceInfo"

	// Protocols
	SelectedTestCase     = "SelectedTestCase"
	SelectedTestSequence = "SelectedTestSequence"
	SelectedType         = "SelectedType"
	Protocol             = "Protocol"
	CaseProtocols        = "CaseProtocols" // The case protocols of a sequence

	// Comments
	Comments = "Comments"

	//Various
	Filtered = "Filtered"

	//Members
	Member    = "Members"
	NonMember = "NonMembers"
	Owner     = "Owner"

	//Labels
	LabelColors = "LabelColors"

	//Delete Modals
	DeleteLabels       = "DeleteLabels"
	DeleteTestCase     = "DeleteTestCase"
	DeleteTestSequence = "DeleteTestSequence"
	DeleteProject      = "DeleteProject"

	//Settings
	Roles = "Roles"
)

// Reserved keys for user information
const (
	SignedInKey           = "SignedIn"
	UserKey               = "User"
	MemberKey             = "Member"
	ProjectPermissionsKey = "ProjectPermissions"
)
