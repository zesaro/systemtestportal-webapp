/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

// Generate help modal content by copying all buttons-, input- and select elements.
function generateHelp(where) {
    const controls = $("button, input, select").not(':hidden');
    where.find(controls).each(function(i, obj) {
        let c = $(obj).clone();
        const info = $('<td>').text(c.attr("title"));
        const content = $('<td>');
        if (c.attr("type") === "radio") {
            c = $( "#" + c.attr('id') ).parent().clone();
            let input = c.find("input");
            input.attr("name", input.attr("name") + "_help");
            input.attr("id", input.attr("id") + "_help")
        }
        if (c.attr("type") === "checkbox") {
            // do nothing
        } else {
            c.removeAttr("id onClick title href");
            content.append(c);
            const row = $('<tr>');
            row.append(content);
            row.append(info);
            $('.help-table').append(row);
        }
    });
}
