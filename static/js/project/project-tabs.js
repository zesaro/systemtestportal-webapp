/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/util/common.js");
$.getScript("/static/js/util/ajax.js");

/** Add click listeners to tabs and project image */
function initializeTabClickListener() {

    // Dashboard
    $("#tabButtonDashboard, #menuButtonDashboard, #projectImage").on("click", event => requestTab(event, "dashboard"));

    // Test Cases
    $("#tabButtonTestCases, #menuButtonTestCases").on("click", event => requestTab(event, "testcases"));

    // Test Sequences
    $("#tabButtonTestSequences, #menuButtonTestSequences").on("click", event => requestTab(event, "testsequences"));

    // Test Protocols
    $("#tabButtonProtocols, #menuButtonProtocols").on("click", event => requestTab(event, "protocols"));

    // Members
    $("#tabButtonMembers, #menuButtonMembers").on("click", event => requestTab(event, "members"));

    // Settings
    $("#tabButtonSettings, #menuButtonSettings").on("click", event => requestTab(event, "settings"));

}

/**
 * Updates menu highlight on tab/menu swap
 * @param tab {string} menu entry to highlight
 */
function updateMenu(tab) {

    $(".tab-collapse-menu .dropdown-item.active").removeClass("active");

    let menu = null;
    if (tab === "testcases")
        menu = $("#menuButtonTestCases");
    else if (tab === "testsequences")
        menu = $("#menuButtonTestSequences");
    else if (tab === "protocols")
        menu = $("#menuButtonProtocols");
    else if (tab === "members")
        menu = $("#menuButtonMembers");
    else if (tab === "dashboard")
        menu = $("#menuButtonDashboard");
    else if (tab === "settings")
        menu = $("#menuButtonSettings");

    if (menu != null)
        menu.addClass("active");
}

/**
 * Updates tab highlight on tab/menu swap
 * @param tab {string} the tab to highlight
 */
function updateTabs(tab) {

    $(".nav-tabs .nav-link.active").removeClass("active");

    let newTab = null;
    if (tab === "testcases")
        newTab = $("#tabButtonTestCases");
    else if (tab === "testsequences")
        newTab = $("#tabButtonTestSequences");
    else if (tab === "protocols")
        newTab = $("#tabButtonProtocols");
    else if (tab === "members")
        newTab = $("#tabButtonMembers");
    else if (tab === "dashboard")
        newTab = $("#tabButtonDashboard");
    else if (tab === "settings")
        newTab = $("#tabButtonSettings");

    if (newTab != null)
        newTab.addClass("active");
}

/** jQuery document.ready */
$(() => {

    let url = currentURL().segments[3];
    updateMenu(url);
    updateTabs(url);

    $('.timeago').each(function (i, obj) {

        const timeAgo = $(".timeago");
        const timeString = timeAgo.text().replace(" ", 'T').replace(" ", '').replace(" CET", '');

        timeAgo.text(timeString);
        timeAgo.attr("datetime", timeString);

        $("time.timeago").timeago();

    });
});