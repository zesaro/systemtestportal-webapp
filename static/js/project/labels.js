/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

$.getScript("/static/js/project/testcases.js");

// showLabels rebuilds the label list.
function showLabels(source, containerID) {
    var container = $(containerID);
    container.empty();

    if (source.length > 0) {
        // Add the selected labels to testSequenceLabels
        $.each(source, function (index, value) {
            var label = document.createElement("span");
            label.setAttribute("class", "badge badge-primary");
            label.setAttribute("id", value.Name);
            label.setAttribute("style", "background-color: #" + value.Color);
            label.setAttribute("data-toggle", "tooltip");
            label.setAttribute("title", value.Description);
            label.textContent = value.Name;

            container.append(label);
        });
        assignButtonsTestCase();
    } else {
        container.append($('<span class=\"text-muted\">No Labels</span>&nbsp;\n'));
    }
}

//rebuilds the filterBadge label list
function showFilterLabels(source, containerID, location) {
    var container = $(containerID);
    container.empty();

    if (source.length > 0) {
        // Add the selected labels to testSequenceLabels
        $.each(source, function (index, value) {
            var label = document.createElement("span");
            label.setAttribute("class", "badge badge-primary filterBadge clickIcon");
            label.setAttribute("data-toggle", "tooltip");
            label.setAttribute("id", value.Name);
            label.setAttribute("title", value.Description);
            label.setAttribute("style", "background-color: #" + value.Color + ";");
            label.textContent = value.Name;

            container.append(label);
            container.append(" ");
        });
        if (location.endsWith("testcases")) {
            $(".filterBadge").click(filterTestCaseListWithBadge);
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        } else if (location.endsWith("testsequences")) {
            $(".filterBadge").click(filterTestSequenceListWithBadge);
            $('[data-toggle="tooltip"]').tooltip({
                trigger: 'hover'
            });
        } else {
            setUserErrorMessage("Could not correctly rebuild the label list.");
        }
    } else {
        container.append($('<span class=\"text-muted\">No Labels</span>&nbsp;\n'));
    }
}