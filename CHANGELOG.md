# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## Downloads

Releases of the SystemTestPortal can be downloaded from the [tags](https://gitlab.com/stp-team/systemtestportal-webapp/tags).
Nightly builds can be found on the [FTP Server of the University of Stuttgart](ftp://ftp.informatik.uni-stuttgart.de/pub/se/systemtestportal/).

## [v1.4.0 07/07/2018]

### Added
- Added the Permission settings page, where you can edit the permissions of existing roles, add new roles and delete roles
- TODOs for users. Todos are created when assigning test-cases and test-sequences
- Added option to anonymously execute test cases and test sequence
- Projects can now be exported to json

### Changed
- You need to be logged in now, to add a new user to the STP
- Removed RAM build option
- Change case/sequence execution layout
- Redesign of lable editing dialog
- Improved example database with test protocols

### Fixed
- An error, when you create a testsequence with the same testcase added multiple times

## [v1.3.0 28/06/2018] 

### Added
- Preconditions are now itemized
- Added label editing functionality
- Test Sequence protocols can now be exported to pdf
- Test Case protocols can now be exported to pdf
- Added the ability to reorder testcases of a testsequence when editing a testsequence
- Added tooltip to labels, which shows the description of the label
- Added a delete confirmation pop-up when deleting labels
- Allow the tester to choose between three levels (fulfilled, partially fulfilled, not fulfilled) for each precondition before starting a test execution
- Added confirm dialog on abort events when executing tests

### Changed
- Changed result "Passed with Comment" to "Partially Successful"
- Redesigned the Manage Label dialog
- Limited the label length to 2000 characters

### Fixed
-cell tooltips are now displayed correctly after changing the variant


## [v1.2.0 16/06/2018]

### Added
- Clicking on a test case cell on the test case dashboard now links the related test case page
- Clicking on a test sequence cell on the test sequence dashboard now links the related test sequence page
- Testcase and Testsequence protocols are now saved persistent in the database
- Added basic tooltip to dashboard results, which shows tester, date, comment and result of a test case
- Visibility settings for projects
- Support for storing and display project images
- Clicking on a test case result cell on the test case dashboard now links to the related test case protocol
- Added highlighting of the selected label color in the label creation
- Select role of user when adding a member to a project
- The role of a member is now shown in the member tab
- Added testsequence dashboard, you can switch between testcase and testsequence dashboard via button
- Added label editing functionality
- If you have the permission you can now edit the role of members on the members page.

### Changed
- Limited the label length to 50 characters to prevent formatting and layout problems
- Invert Version and Variant

### Fixed
- Issue #472 in which it was not possible to add versions to variants after saving a variant without a version
- Refreshing start page in test-sequence execution did not show the user as signed-in (Issue #348)
- Users can now delete variants and versions
- Adding a test cases multiple times to the same sequence is now possible 
- A bug in which the selected label color did not reset after adding a new label

### Removed
- Removed help button from settings and dashboard page, since there is nothing to do for now


## [v1.1-beta 29/05/2018]

### Added
- Pause Button now changes icon when clicking it
- Added Dropdown-Menu for dashboards, where you can switch between variants
- The Executioner of "Test cases" and "Test sequences" now gets saved and shown in Protocols.

### Fixed
- The delete buttons at "Manage variants and versions"
- Issue #354 in which test step deletion button only worked on the edge of the button


## [v1.1-alpha - 21/05/2018]

### Added
- Backend and frontend support for colored labels in "Test Cases".
- Backend and frontend support for colored labels in "Test Sequences".
- Added a basic and simple dashboard for "Testcases"

### Changed
- Improve usability of the manage variants and versions dialog
- Use the persistent sqlite version as default, to build with the volatile ram version now use ```go build --tags 'ram' <path-to-/cmd/stp>```

### Fixed
- Issue #407 in which labels were no longer clickable after pressing "Save Settings"

## [v1.0.0-rc4 - 07/05/2018]

### Added
- Backend support for User Roles and User Premissions

### Fixed
- Issue #363 which prevented users from saving test cases
- Fixed some displaying on mobile devices

## [v1.0.0-rc3 - 06/05/2018]

### Changed
- Filter test cases/test sequences with multiple labels

### Fixed
- Fixed an issue where the testsequence summary page would show incorrect times
- Fixed "Enter-key" for textfields
- When editing, adding sut-versions to a case that contained no sut-versions caused the server to panic. This is fixed now
- Adding more test-cases to a test-sequence on edit now works as intended
- Replace empty dropdown-menu with selected variant+version in start-pages of test-cases when executing a test-sequence
- Fix updating the selected sut-variants and sut-versions when editing a test-case
- Generate the sequence info (sut-versions / duration) before displaying them, fixing it being empty when using the sqlite version and the user being unable to execute a test sequence
- Adding and removing members from a project when using the persistent 'sqlite' version


## [v1.0.0-rc2] - 16/04/2018

### Fixed
- Fix execution of test-sequences. Previously only the first case could be executed
- Clicking on an older version in the history of a test now requests the correct url
- Clicking on the version of a test in a test-protocol now requests the correct url
- Show new-project button on explore-projects page if you are signed in
- When creating new project, the owner is added to the members and can add/remove members
- Fix computing the protocol id for newly created protocols

## [v1.0.0-rc1] - 31/03/2018

### Added
- Projects have members now

### Fixed
- Fix bug where an empty test step was created for every manually entered test step on test case creation

## [v1.0.0-beta] - 27/03/2018

### Added
- Show the needed time for testing in the summary page of a text execution
- Add Architectural Decision Records (https://adr.github.io/)
- Test for handlers
- Test plan to manually test the SystemTestPortal

### Changed
- Only renaming a test does not create a new test version anymore because there would not be any visible changes in the history of a test
- Url encoding for projects and tests
- Remember which protocol was selected
- By default, the support for sqlite is disabled due to issues with broken binaries (See #223)

### Fixed
- Fix binaries not starting anymore (See #223)
- Fix registration of users
- Fix disappearing protocols after renaming a test case
- Fix a bug that did not allow the name of a test case being changed back to its previous name
- Show correct results of executed test sequences in protocols

### Known issues
- Test sequences without any applicable sut-versions can't be executed (#239)
- During an test sequence execution the start page of each test case shows an empty dropdown instead of the selected sut-variant + sut-version (#236)

## [v0.11.0] - 13/02/2018

### Added
- Add persistent storage
- Add docker image

### Fixed
- Fix test sequence protocols showing "Invalid Date" for execution date
- Fix listing of projects. Now all projects can be listed on the explore page
- Fix wording of error when test sequence cannot be updated. The error does not show "Can not update test case" anymore.

### Known issues
- Registering a user does not work
- Renaming a project does not work correctly
- Protocols of test sequences show result as "Not assessed" altough there is a result
- The urls are sometimes encoded wrong

## [v0.10.0] - 30/01/2018

### Added
- Add comments for test cases and test sequences
- Add settings for projects
- Improved GUI

### Fixed
- Automatically update the list of sut-versions with the text when adding a sut-version to a project containing no sut-versions
- Only update the labels when clicking "save" instead of updating them when the modal closes
- Fix missing characters in the protocol title (e.g. a missing "ß")
- Trying to sign in with a wrong password now shows a full error page

### Security
- Add hashing of passwords in the database

### Known issues
- Protocols of test sequences contain the wrong test case protocols
- Not all projects are listed in the explore tab. Only the projects of the "default" user are listed
- Renaming or deleting a test prevents its protocols from being listed

## [v0.9.0] - 23/01/2018

### Added
- Add a print dialog for the list of test cases and test sequences
- Add a print dialog for the detailed view of test cases and test sequences
- Add a print dialog for test cases and test sequences with free input forms to execute the tests with pen and paper
- Add option to delete a variant of the sut
- Add labels for test cases and test sequences

### Changed
- Move hardcoded strings (paths, parameters, keys) to constants
- Wrap up ids of projects, groups, tests, users

## [v0.8.0] - 08/01/2018

### Added
- Add a timer to track the time needed for a test execution
- Add system-under-test variants in addition to the existing system-under-test-versions
- Use https://github.com/dimfeld/httptreemux as a router for the requests

### Fixed
- Fix whitespace in the textarea of the commit-message when editing tests

## [v0.6.0] - 24/11/2017

This is the first public release of the SystemTestPortal. Basic features such as the creation of projects, test cases and test sequences and the execution are implemented.
Additionally, there is a basic concept of users and permission management. Some features are restricted to signed-in users.

### Added
- Simple registration process
- Features limited to signed-in users (e.g. creating tests)
- Add creation of single tests and compound test sequences
- Add editing of tests and test sequences
- Add history for tests. Older versions of a test can be shown but not edited
- Add execution for tests and test sequences
- Add protocols to show results of executed tests and test sequences
