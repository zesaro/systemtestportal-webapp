{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "content"}}
<form id="newGroupForm" action="save" novalidate>
    <div id="modalPlaceholder"></div>
    <div class="form-group">
        <label for="inputGroupName">Group name</label>
        <input type="text" class="form-control" id="inputGroupName" name="inputGroupName"
               aria-describedby="GroupNameHelp" placeholder="Enter Group name">
        <small id="GroupNameHelp" class="form-text text-muted">
            This name is used wherever the group is mentioned.
        </small>
    </div>
    <div class="form-group ">
        <label for="inputGroupDesc">Group description</label>
        <textarea class="form-control" id="inputGroupDesc" name="inputGroupDesc"
                  aria-describedby="GroupDescHelp" rows="3"
                  placeholder="Describe your group in a few words."></textarea>
        <small id="GroupDescHelp" class="form-text text-muted">
            This description is used in the explore section.
        </small>
    </div>
    <fieldset class="form-group" data-toggle="tooltip" data-placement="left"
              title="" data-original-title="This feature will be available in a future version">
        <label>Visibility</label>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optionsGroupVisibility"
                       id="optionPublic" value="public" checked>
                <b>public</b> &mdash; public groups are visible to everyone
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optionsGroupVisibility"
                       id="optionInternal" value="internal">
                <b>internal</b> &mdash; internal groups are visible to all signed in users
            </label>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input" name="optionsGroupVisibility"
                       id="optionPrivate" value="private">
                <b>private</b> &mdash; private groups are only visible to members of these groups
            </label>
        </div>
    </fieldset>
    <p  data-toggle="tooltip" data-placement="left"
        title="" data-original-title="This feature will be functional in a future version">
        <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#nameSelection" aria-expanded="false" aria-controls="nameSelection">
            Select Members <i id="select-name-collapse-toggler" class="fa fa-caret-down" aria-hidden="true"></i>
        </button>
    </p>
    <div class="collapse" id="nameSelection">
        <div class="card">
            <div class="card-body">
                <div role="search" class="search-form mb-3">
                    <label class="sr-only">Search</label>
                    <div class="input-group full-width">
                        <input type="search" name="s" class="search-field form-control"
                               placeholder="Search">
                        <span class="input-group-btn">
                            <button class="search-submit btn btn-outline-primary disabled" disabled>
                                Search
                            </button>
                        </span>
                    </div>
                </div>
                {{ range .Users }}
                <div class="form-check">
                    <label class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input">
                        <span class="custom-control-indicator"></span>
                        <span class="custom-control-description">{{ .DisplayName }}</span>
                    </label>
                </div>
                {{ end }}
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-success mt-4">Create Group</button>

    <script src="/static/assets/js/vendor/jquery.validate.min.js"></script>
    <script src="/static/js/explore/new-group.js"></script>
    <script type='text/javascript'>

    </script>
</form>
{{end}}