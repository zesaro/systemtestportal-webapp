{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "modal-member-assignment"}}
<div class="modal fade" id="modal-member-assignment" tabindex="-1" role="dialog"
     aria-labelledby="modal-member-assignment-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-member-assignment-label">Select new members</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        {{ $roles := .Project.Roles }}
            <div class="modal-body" id="member-assignment">
            {{range .NonMembers }}
                <div class="row">
                    <div class="col">
                        <div class="form-check checkbox-list-element">
                            <label class="custom-control custom-checkbox">
                                <input id="{{ .Name }}" type="checkbox" class="custom-control-input memberSelector"
                                       name="{{ .Name }}">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description">{{ .DisplayName }}</span>
                            </label>
                        </div>
                    </div>
                    <div class="col">
                        <select class="bootstrap-select roleSelector" title="Select Role">
                        {{ range $index, $role := $roles }}
                        {{ if ne (.Name) ("Owner")}}
                            <option value="{{ $index }}" selected="selected">{{ .Name }}</option>
                        {{end}}
                        {{ end }}
                        </select>
                    </div>
                </div>
            {{end}}
            </div>
            <div class="modal-footer">
                <button id="buttonAssignMember" type="button" class="btn btn-primary">Add member</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{end}}