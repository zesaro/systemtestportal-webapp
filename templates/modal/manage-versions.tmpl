{{define "modal-manage-versions"}}
<div class="modal fade" id="modal-manage-versions" tabindex="-1" role="dialog" aria-labelledby="modal-manage-versions-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-manage-versions-label"><i class="fa fa-wrench" aria-hidden="true"></i> Manage Versions and Versions</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <wrapper>
                <div class="modal-body" style="width: 50%;float: left;border-right: 1px solid #e9ecef;">
                    <h5>Versions</h5>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputProjectVersion" placeholder="e.g. Windows 10" aria-label="e.g. Windows 10">
                                <span class="input-group-btn">
                                    <button id="buttonAddProjectVersion" class="btn btn-secondary" type="button">Add</button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <br>
                    <ul id="versionList" class="list-group">
                    </ul>
                </div>
                <div class="modal-body" style="width: 50%;float: right;border-left: 1px solid #e9ecef;">
                    <h5>Variants</h5>
                    <div class="row">
                        <div class="col-12">
                            <div class="input-group">
                                <select id="projectVersionSelect" class="form-control custom-select" onchange="showVariants();">
                                    <option value="none">Select a version...</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="collapse"  id="collapseVariants">
                        <div class="row">
                            <div class="col-12">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="inputProjectVariant" placeholder="e.g. 1.0.3" aria-label="e.g. 1.0.3">
                                    <span class="input-group-btn">
                                    <button id="buttonAddProjectVariant" class="btn btn-secondary" type="button">Add</button>
                                </span>
                                </div>
                            </div>
                        </div>
                        <br>
                        <ul id="variantList" class="list-group">
                        </ul>
                    </div>
                </div>
            </wrapper>
            <div class="modal-footer">
                <button id="buttonSaveVersions" type="button" class="btn btn-primary">Save settings</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

    <script src="/static/js/util/common.js"></script>
    <script type='text/javascript'>

        var xmlhttp = new XMLHttpRequest();
        // selectedVersion is the key of the currently selected version in the drop down menu as string
        var selectedVersion;

        //project version data
        var projectVariantData;
        var urlVariants = getProjectURL().appendSegment("versions").toString();

        xmlhttp.onreadystatechange = function() {
            if (this.readyState === 4 && this.status === 200) {
                projectVariantData = JSON.parse(this.responseText);
                fillVersions(projectVariantData, "#projectVersionSelect");
            }
        };
        xmlhttp.open("GET", urlVariants, true);
        xmlhttp.send();

        // fill Versions gets a map with versions and adds them to the drop down menu with the selectorID
        function fillVersions(versionMap, selectorID) {
            $('#inputTestCaseSUTVersions').empty();
            $("#versionList").empty();
            $.each(versionMap, function(key,version) {
                addVersion(version, selectorID);
            });
        }

        // addVersion adds a version to the drop down menu with versions
        function addVersion(version, selectorID) {
            var selector = $(selectorID);
            var selectedValue = selector.val();
            selector.append($("<option></option>")
                    .attr("value",version.Name)
                    .text(version.Name));
            selector.value = selectedValue;
            //list
            var list = $('#versionList');
            var listElement = ($("<li></li>")
                 .attr("class","list-group-item")
                 .attr("style","display:flex;")
                 .html('<span style="width:50%;">'+version.Name+'</span>'));
            var deleteButton = ($("<button></button>")
                 .attr("class","btn btn-danger ml-auto list-line-item btn-sm buttonDeleteVersion")
                 .attr("id","del-var-" + version.Name)
                 .attr("type","button")
                 .html('<i class="fa fa-trash-o" aria-hidden="true"></i>\n' +
                         '<span class="d-none d-sm-inline"> Delete</span>'));
                 deleteButton.click(function (event) { buttonDeleteVersion(event); });
            var versionDeleteConfirmButton = ($("<button></button>")
                 .attr("class","btn btn-danger ml-auto d-none list-line-item btn-sm buttonDeleteVersionConfirm")
                 .attr("id","delConfirm-var-" + version.Name)
                 .attr("type","button")
                 .html('<i class="fa fa-check" aria-hidden="true"></i>\n' +
                         '<span class="d-none d-sm-inline"> Confirm delete</span>'));
            versionDeleteConfirmButton.click(function (event) { buttonDeleteVersionConfirm(event); });
            listElement.append(deleteButton);
            listElement.append(versionDeleteConfirmButton);
            list.append(listElement);
        }



        /* Adding a handler for the enter key to add a version*/
        $("#inputProjectVersion").keydown(function(event){
            if(event.keyCode === 13){
                event.preventDefault();
                $("#buttonAddProjectVersion").click();
            }
        });

        /* attach a handler to the add test cases form submit button */
        $("#buttonAddProjectVersion").click(function(event){
            var versionKey = $("#inputProjectVersion").val().trim();
            if (projectVariantData === null) {
                projectVariantData = {};
            }
            if((versionKey !== "")&&!(versionKey in projectVariantData)) {
                projectVariantData[versionKey] = JSON.parse('{"Name":"'+versionKey+'","Variants":"'+[]+'"}');
                projectVariantData[versionKey].Variants = [];
                addVersion({Name:versionKey,Variants:{}}, "#projectVersionSelect");
                // Clear text field
                $("#inputProjectVersion").val("");
                // Select newly added version in dropdown menu
            } else {
                //Notify that version already exists.
            }
        });

        // buttonDeleteVersion manages the change of the delete-button,
        // to confirm the delete-intent of the user
        function buttonDeleteVersion(event) {
            $(event.currentTarget).next().removeClass("d-none");
            $(event.currentTarget).addClass("d-none");
        }

        // deleteTestStep removes the test step-list element from the GUI
        function buttonDeleteVersionConfirm(event) {
            var pressedButton = $(event.currentTarget);
            var idToDelete = pressedButton.attr("id").slice(15);

            deleteVersion(idToDelete);
            pressedButton.parent().remove();
        }

        function deleteVersion(version) {
            delete projectVariantData[version];
            var selector = $('#projectVersionSelect');
            selector.find("option[value='" + version + "']").remove();
            selector.value = selector.children[0];
            showVariants();
        }


        //Variants

        // Shows the variants of the currently selected version
        function showVariants() {
            var selectedValue = $("#projectVersionSelect").val();
            selectedVersion = selectedValue;

            if(selectedValue === "none") {
                $('#collapseVariants').collapse('hide');
            } else {
                $('#collapseVariants').collapse('show');
                $("#variantList").empty();
                populateVariants(selectedValue);
            }
        }

        // Adds variants to the list with variants
        function populateVariants(selectedVersionKey) {
            var version = projectVariantData[selectedVersionKey];
            if (version.Variants === null){
                version.Variants = [];
            }
            if (version.Variants.length > 0) {
                $.each(version.Variants, function(key,variant) {
                    addVariant(variant);
                });
            }
        }

        // adds a variant to the list with variants
        // Parameter variant has type {Name: "VariantXYZ"}
        function addVariant(variant) {
            var list = $('#variantList');
            var listElement = ($("<li></li>")
                .attr("class","list-group-item")
                .attr("style","display:flex;")
                .html('<span style="width:50%;">'+variant.Name+'</span>'));
            var deleteButton = ($("<button></button>")
                .attr("class","btn btn-danger ml-auto list-line-item btn-sm buttonDeleteVariant")
                .attr("id","del-ver-" + variant.Name)
                .attr("type","button")
                .html('<i class="fa fa-trash-o" aria-hidden="true"></i>\n' +
                    '<span class="d-none d-sm-inline"> Delete</span>'));
            deleteButton.click(function (event) { buttonDeleteVariant(event); });
            var variantDeleteConfirmButton = ($("<button></button>")
                .attr("class","btn btn-danger ml-auto d-none list-line-item btn-sm buttonDeleteVariantConfirm")
                .attr("id","delConfirm-ver-" + variant.Name)
                .attr("type","button")
                .html('<i class="fa fa-check" aria-hidden="true"></i>\n' +
                    '<span class="d-none d-sm-inline"> Confirm delete</span>'));
            variantDeleteConfirmButton.click(function (event) { buttonDeleteVariantConfirm(event); });
            listElement.append(deleteButton);
            listElement.append(variantDeleteConfirmButton);
            list.append(listElement);
        }

        // buttonDeleteTestStepConfirm manages the change of the delete-button,
        // to confirm the delete-intent of the user
        function buttonDeleteVariant(event) {
            $(event.currentTarget).next().removeClass("d-none");
            $(event.currentTarget).addClass("d-none");
        }

        // deleteTestStep removes the test step-list element from the GUI
        function buttonDeleteVariantConfirm(event) {
            var pressedButton = $(event.currentTarget);
            var idToDelete = pressedButton.attr("id").slice(15);
            deleteVariant(projectVariantData[selectedVersion].Variants, idToDelete);
            pressedButton.parent().remove();
        }

        function deleteVariant(variantList, idToDelete) {
            variantList.forEach(function(ver, index) {
                if (ver.Name === idToDelete) {
                    variantList.splice(index, 1);
                }
            });
        }

        /* Adding a handler for the enter key to add a variant*/
        $("#inputProjectVariant").keydown(function(event){
            if(event.keyCode === 13){
                event.preventDefault();
                $("#buttonAddProjectVariant").click();
            }
        });

        /* attach a handler to the add test cases form submit button */
        $("#buttonAddProjectVariant").click(function() {
            var variantKey = $("#inputProjectVariant").val().trim();
            if((variantKey !== "")&&(selectedVersion !== "none")&&!containsVariant({Name:variantKey}, projectVariantData[selectedVersion].Variants)) {
                var variantObject = JSON.parse('{"Name":"'+variantKey+'"}');
                projectVariantData[selectedVersion].Variants.push(variantObject);
                addVariant(variantObject);
                $("#inputProjectVariant").val("")
            } else {
                //Notify that version already exists.
            }
        });

        /* attach a handler to the add test cases form submit button */
        $("#buttonSaveVersions").click(function(event) {
            var xhr = new XMLHttpRequest();
            xhr.open("POST", urlVariants, true);
            xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

            // send the collected data as JSON
            xhr.send(JSON.stringify(projectVariantData));

            xhr.onloadend = function () {
                if (this.status === 200) {
                    $("#modal-manage-versions").modal('hide');
                } else {
                    console.log(this.response);
                }
            };

            if(Object.keys(projectVariantData).length > 0) {
                $("#someversions").removeClass("d-none");
                $("#noversion").addClass("d-none");
            } else {
                $("#someversions").addClass("d-none");
                $("#noversion").removeClass("d-none");
            }
        });



    </script>
</div>
{{end}}