{{/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/}}

{{define "tab-content"}}
<div class="tab-card card" id="tabTestProtocols">
    <nav class="navbar navbar-light action-bar p-3">
        <div class="input-group flex-nowrap">
            <button class="btn btn-secondary mr-2" id="buttonBack">
                <i class="fa fa-long-arrow-left" aria-hidden="true"></i>
                <span class="d-none d-sm-inline"> Back</span>
            </button>
            <button class="btn btn-primary" id="buttonPdf">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                <span class="d-none d-sm-inline">Print to PDF</span>
            </button>
        </div>
    </nav>
    <div class="row tab-side-bar-row">
        <div class="col-md-9 p-3">
            <h4 class="mb-3">
                <span id="contentTestCaseResult" class="text-muted">
                    <i
                    {{ if eq .Protocol.Result 1 }}
                    class="fa fa-check-circle text-success" title="Passed"
                    {{ else if eq .Protocol.Result 2 }}
                    class="fa fa-info-circle text-warning" title="Partially Successful"
                    {{ else if eq .Protocol.Result 3 }}
                    class="fa fa-times-circle text-danger" title="Failed"
                    {{ else }}
                    class="fa fa-question-circle text-secondary" title="Not Assessed"
                    {{ end }}
                    aria-hidden="true" data-toggle="tooltip" data-placement="bottom">
                    </i>
                </span>
                <span id="protocolName">Protocol of {{ .TestCase.Name }}</span>
            </h4>
        {{ if ne .Protocol.Comment "" }}
            <div id="contentTestCaseNotesContainer" class="form-group">
                <label><strong>Notes</strong></label>
                <p id="contentTestCaseNotes" class="text-muted">
                {{ .Protocol.Comment }}
                </p>
            </div>
        {{ end }}
            <div class="form-group">
                <label><strong>Test Case Description</strong></label>
                <p id="contentTestCaseDescription" class="text-muted">
                {{ .TestCaseVersion.Description }}
                </p>
            </div>
            <div class="form-group">
                <label><strong>Test Case Preconditions</strong></label>
                <ul id="contentPreconditions" class="list-group">
                {{ if eq (len .Protocol.PreconditionResults) 0 }}
                    <li class="list-group-item preconditionItem">
                        <span>No Preconditions</span>
                    </li>
                {{ end }}
                {{ range .Protocol.PreconditionResults }}
                    <li class="list-group-item preconditionItem">
                        <span>{{ .Precondition.Content }}</span>
                        <span><strong>({{ .Result }})</strong></span>
                    </li>
                {{ end }}
                </ul>
            </div>
            <div class="form-group">
                <label><strong>Test Step Results</strong></label>
                <ul class="list-group" id="testStepsResultAccordion">
                {{ $caseVersion := .TestCaseVersion }}
                {{ range $index, $protocol := .Protocol.StepProtocols }}
                    <li class="list-group-item">
                        <a>
                            <i
                            {{ if eq .Result 1 }}
                            class="fa fa-check-circle text-success" title="Passed"
                            {{ else if eq .Result 2 }}
                            class="fa fa-info-circle text-warning" title="Partially Successful"
                            {{ else if eq .Result 3 }}
                            class="fa fa-times-circle text-danger" title="Failed"
                            {{ else }}
                            class="fa fa-question-circle text-secondary" title="Not Assessed"
                            {{ end }}
                            aria-hidden="true" data-toggle="tooltip" data-placement="bottom">
                            </i>
                        </a>
                    {{ (index $caseVersion.Steps $index).Action }}
                        <a data-toggle="collapse" aria-expanded="false"
                           data-parent="#testStepsResultAccordion" class="float-right"
                           href="#testStepsResultAccordion{{ $index }}"
                           aria-controls="#testStepsResultAccordion{{ $index }}">
                            <i class="fa fa-chevron-down d-print-none" aria-hidden="true"></i>
                        </a>
                        <div class="collapse" role="tabpanel" id="testStepsResultAccordion{{ $index }}">
                            <table>
                                <tr>
                                    <td>Expected:</td>
                                    <td>{{ (index $caseVersion.Steps $index).ExpectedResult }}</td>
                                </tr>
                                <tr>
                                    <td>Observed:</td>
                                    <td>{{ .ObservedBehavior }}</td>
                                </tr>
                            {{ if not (eq .Comment "") }}
                                <tr>
                                    <td>Notes:</td>
                                    <td>{{ .Comment }}</td>
                                </tr>
                            {{ end }}
                            </table>
                        </div>
                    </li>
                {{ end }}
                </ul>
            </div>
        </div>
        <div class="col-md-3 p-3 tab-side-bar">
            <div class="form-row">
                <div class="col-12">
                    <strong>Execution Date</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestCaseExecutionDate" class="text-muted">
                        <time class="timeago">
                        {{ .Protocol.ExecutionDate }}
                        </time>
                    </p>
                </div>
            {{ if not .Protocol.IsAnonymous }}
                <div class="col-12" id="divTestCaseTester">
                    <strong>Tester</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestCaseTester" class="text-muted">
                    {{ .Protocol.UserName }}
                    </p>
                </div>
            {{ end }}
                <div class="col-12">
                    <strong>System Version</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestCaseSUTVersion" class="text-muted">
                    {{ .Protocol.SUTVersion }}
                    </p>
                </div>
                <div class="col-12">
                    <strong>System Variant</strong>
                </div>
                <div class="col-12">
                    <p id="contentTestCaseSUTVariant" class="text-muted">
                    {{ .Protocol.SUTVariant }}
                    </p>
                </div>
                <div class="col-12">
                    <strong>Case Version</strong>
                </div>
                <div class="col-12">
                    <a id="contentTestVersion" class="cursor-clickable"
                       href="/{{ .Project.Owner }}/{{ .Project.Name }}/testcases/{{ .TestCase.Name }}?version={{ .TestCaseVersion.VersionNr }}"
                       onclick="openTestVersion(this, '/{{ .Project.Owner }}/{{ .Project.Name }}/testcases/{{ .TestCase.Name }}', {{ .TestCaseVersion.VersionNr }})">{{ .Protocol.TestVersion.TestVersion }}</a>
                </div>
            </div>
        </div>
    </div>
    <script src="/static/js/project/testprotocols.js"></script>
    <script>
        assignListeners();
    </script>
</div>
{{end}}


