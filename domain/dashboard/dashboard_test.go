/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package dashboard

import (
	"testing"
	"gitlab.com/stp-team/systemtestportal-webapp/store/dummydata"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/test"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"
)

func TestNewDashboard(t *testing.T) {

	proj, cases, protocolsmap := generateTestData()

	dashboard := NewDashboard(&proj, cases, protocolsmap)

	if (dashboard.Project.Name != "DuckDuckGo.com") {
		t.Errorf("Wrong project")
	}

	if (cases[0].Name != "Change Theme" || cases[1].Name != "Search for Websites") {
		t.Errorf("Wrong test cases")
	}

	proj = project.Project{}
	cases = make([]*test.Case, 0)
	protocolsmap = make(map[id.TestID][]test.CaseExecutionProtocol)

	dashboard = NewDashboard(&proj, cases, protocolsmap)
	if (len(dashboard.ProtocolMap) != 0) {
		t.Errorf("Shouldn't load anything")
	}
}

func TestSortFunctions(t *testing.T) {
	dashboardElements, variants, versions := generateTestDataForSorting()

	sortDashboardElements(dashboardElements)
	sortVariants(variants)
	sortVersions(versions)

	if dashboardElements[0].Variant.Name != "test" {
		t.Errorf("Sorting of dashboard elements went wrong, expected %v got %v", "test", dashboardElements[0].Variant.Name)
	}

	if variants[0].Name != "a" {
		t.Errorf("Sorting of dashboard elements went wrong, expected %v got %v", "a", variants[0].Name)
	}

	if variants[len(variants)-1].Name != "z" {
		t.Errorf("Sorting of dashboard elements went wrong, expected %v got %v", "z", variants[len(variants)].Name)
	}

	if versions[0].Name != "a" {
		t.Errorf("Sorting of dashboard elements went wrong, expected %v got %v", "a", versions[0].Name)
	}

	if versions[len(versions)-1].Name != "z" {
		t.Errorf("Sorting of dashboard elements went wrong, expected %v got %v", "z", versions[len(versions)].Name)
	}
}

func generateTestData() (project.Project, []*test.Case, map[id.TestID][]test.CaseExecutionProtocol) {
	//Loads DuckDuckGo project
	proj := dummydata.Projects[0]

	//Loads DuckDuckGo test cases
	cases := make([]*test.Case, 0)

	for _, c := range dummydata.Cases {
		if c.Project == proj.ID() {
			i := c
			cases = append(cases, &i)
		}
	}

	//Loads protocols for testcases
	protocolsmap := make(map[id.TestID][]test.CaseExecutionProtocol)

	for _, c := range cases {
		protlist := make([]test.CaseExecutionProtocol, 0)
		for _, prot := range dummydata.CaseProtocols {
			if (c.ID() == prot.TestVersion.TestID) {
				protlist = append(protlist, prot)
			}
		}
		protocolsmap[c.ID()] = protlist
	}

	return proj, cases, protocolsmap
}

func generateTestDataForSorting() ([]DashboardElement, []project.Variant, []*project.Version) {
	var dashboardElements []DashboardElement
	var variants []project.Variant
	var versions []*project.Version

	variants = append(variants, project.Variant{Name: "z"})
	variants = append(variants, project.Variant{Name: "test"})
	variants = append(variants, project.Variant{Name: "test"})
	variants = append(variants, project.Variant{Name: "a"})

	versions = append(versions, &project.Version{Name: "z"})
	versions = append(versions, &project.Version{Name: "test"})
	versions = append(versions, &project.Version{Name: "test"})
	versions = append(versions, &project.Version{Name: "a"})

	dashboardElements = append(dashboardElements, DashboardElement{Variant: project.Variant{Name: "test2"}})
	dashboardElements = append(dashboardElements, DashboardElement{Variant: project.Variant{Name: "test"}})
	dashboardElements = append(dashboardElements, DashboardElement{Variant: project.Variant{Name: "test"}})

	return dashboardElements, variants, versions
}
