/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"reflect"
	"testing"
)

func TestNewLabel(t *testing.T) {
	type labelInput struct {
		name, description, color string
	}

	labels := []struct {
		in   labelInput
		want Label
	}{
		{labelInput{},
			Label{
				Name:        "",
				Description: "",
				Color:       "",
			},
		},
		{labelInput{
			name:        "TestWithoutBlanks",
			description: "Description of TestWithoutBlanks",
			color:       "007bff",
		},
			Label{
				Name:        "TestWithoutBlanks",
				Description: "Description of TestWithoutBlanks",
				Color:       "007bff",
			},
		},
		{labelInput{
			name:        "TestWithBlanks",
			description: "Description of TestWithBlanks",
			color:       "007bff",
		},
			Label{
				Name:        "TestWithBlanks",
				Description: "Description of TestWithBlanks",
				Color:       "007bff",
			},
		},
	}

	for _, lb := range labels {
		got := NewLabel(lb.in.name, lb.in.description, lb.in.color)

		if !reflect.DeepEqual(got, lb.want) {
			t.Errorf("NewLabel(%s, %s) = \n%v \nwant \n%v", lb.in.name, lb.in.description, got, lb.want)
		}
	}
}
