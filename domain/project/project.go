/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package project

import (
	"time"

	"strings"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/visibility"
	"golang.org/x/crypto/openpgp/errors"
)

// The Project struct contains information used to describe a project in the system
// Image is a base64 encoded string
type Project struct {
	Name         string
	Description  string
	Image        string
	Visibility   visibility.Visibility
	CreationDate time.Time
	Owner        id.ActorID
	Versions     map[string]*Version
	Labels       []Label
	UserMembers  map[id.ActorID]UserMembership
	Roles        map[RoleName]*Role
}

// NewProject creates a new project
func NewProject(name string, owner id.ActorID, description string, visibility visibility.Visibility) Project {
	name = strings.TrimSpace(name)
	description = strings.TrimSpace(description)
	creationDate := time.Now().UTC().Round(time.Second)

	p := Project{
		Name:         name,
		Description:  description,
		Visibility:   visibility,
		CreationDate: creationDate,
		Owner:        owner,

		Versions:    map[string]*Version{},
		UserMembers: map[id.ActorID]UserMembership{},
		Roles:       defaultRoles,
	}

	return p
}

// ItemName returns a projects name
func (p Project) ItemName() string {
	return p.Name
}

// ID returns a projects id
func (p Project) ID() id.ProjectID {
	return id.NewProjectID(p.Owner, p.Name)
}

// ItemVisibility returns a projects visibility
func (p Project) ItemVisibility() visibility.Visibility {
	return p.Visibility
}

// AddMember adds the given user to the project
// It creates a UserMembership with the given
// role name.
func (p Project) AddMember(member id.ActorID, role RoleName) {
	p.UserMembers[member] = NewUserMembership(member, role)
}

//RemoveMember removes the given user of the project
func (p Project) RemoveMember(member *user.User) {
	delete(p.UserMembers, id.ActorID(member.Name))
}

//updates the role of a member
func (p Project) UpdateMember(member *user.User, role RoleName) {
	p.UserMembers[member.ID()] = p.UserMembers[member.ID()].UpdateRole(role)
}

// IsOwner checks whether the user is owner of
// the project. Returns false if the project
// or the user is nil or if the user is not
// the owner.
func (p *Project) IsOwner(u *user.User) bool {
	if p == nil || u == nil {
		return false
	}
	return p.Owner.Actor() == u.Name
}

// GetLabelByName gets a label of a project by name
func (p Project) GetLabelByName(labelName string) (Label, error) {
	labels := p.Labels
	if len(labels) == 0 {
		return Label{}, errors.InvalidArgumentError("Project contains no labels")
	}
	for _, label := range labels {
		if label.Name == labelName {
			return label, nil
		}
	}
	return Label{}, errors.InvalidArgumentError("Label doesn't exist in project")
}
