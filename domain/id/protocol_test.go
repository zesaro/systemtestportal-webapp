/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package id

import (
	"fmt"
	"math"
	"reflect"
	"testing"
)

func TestNewTestProtocolID(t *testing.T) {
	owner := NewActorID("testUser")
	project := NewProjectID(owner, "Project-1")
	tc := NewTestID(project, "TestCase", true)
	tcv := NewTestVersionID(tc, 2)
	protocolNumbers := []int{
		1,
		0,
		-1,
		math.MaxInt64,
	}
	for _, protocolNr := range protocolNumbers {
		id := NewProtocolID(tcv, protocolNr)
		if !reflect.DeepEqual(tcv, id.TestVersionID) {
			t.Errorf("Testcaseversion wasn't saved correctly to the new ProtocolID. Expected %v but got %v.",
				tcv, id.TestVersionID)
		}
		if protocolNr != id.Protocol() {
			t.Errorf("Protocol number wasn't saved correctly to new ProtocolID. Expected %q but got %q.",
				protocolNr, id.Protocol())
		}
	}
}

type ProtocolExistenceCheckerStub struct {
	exists bool
	err    error
}

func (tvc ProtocolExistenceCheckerStub) Exists(ProtocolID) (bool, error) {
	return tvc.exists, tvc.err
}
func TestProtocolID_Validate(t *testing.T) {
	testVersion := NewTestVersionID(NewTestID(NewProjectID(NewActorID("John"), "Ice-cream"), "test name", true), 3)
	testData := []struct {
		versionNr     int
		returnExists  bool
		returnError   error
		errorExpected bool
	}{
		{-2, false, nil, true},
		{0, false, nil, true},
		{1, false, nil, false},
		{3, true, nil, true},
		{4, false, fmt.Errorf("i'm an error"), true},
	}
	for _, set := range testData {
		id := NewProtocolID(testVersion, set.versionNr)
		result := id.Validate(ProtocolExistenceCheckerStub{set.returnExists, set.returnError})
		if set.errorExpected && (result == nil) {
			t.Errorf("Validate returned no error, but expected one. \nID: %+v", id)
		}
		if !set.errorExpected && (result != nil) {
			t.Errorf("Validate returned an error, but expected none. \nID: %+v \nError: %v", id, result)
		}
	}
}

func TestProtocolID_GobEnDecode(t *testing.T) {
	owner := NewActorID("testUser")
	project := NewProjectID(owner, "Project-1")
	tc := NewTestID(project, "TestCase", true)
	tcv := NewTestVersionID(tc, 2)
	protocolNr := 1
	id := NewProtocolID(tcv, protocolNr)

	data, err := id.GobEncode()
	if err != nil {
		t.Error(err)
	}
	newID := ProtocolID{}
	err = newID.GobDecode(data)
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(id, newID) {
		t.Errorf("ID has changed while decoding and encoding. \nOriginal ID: %+v \nResult: %+v", id, newID)
	}
}
