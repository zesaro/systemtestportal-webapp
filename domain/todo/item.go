/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package todo

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

// Item is a single todo-item in the todo-list
// belonging to a single user
type Item struct {
	Index     int          // The unique index of the item in the todo-list
	Author    id.ActorID   // The "Name" of the user that created this todo-item
	ProjectID id.ProjectID // The project in which the todo-item was created

	Type      Type
	Reference itemReference // Unique reference to the item that is linked to this todo-item

	Deadline     time.Time
	CreationDate time.Time

	Done bool
}

// SetDone sets an item to done
func (item Item) SetDone() {
	item.Done = true
}
