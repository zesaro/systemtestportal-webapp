/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package todo

import (
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

// List is a collection of todo-assignments
type List struct {
	Username string // The "Name" of the user this todo-list belongs to
	TODOs    []Item
}

// New List creates a new todo-list for the user with the
// given name. The list contains no todo-items.
//
// Returns the created list.
func NewList(username string) *List {
	return &List{
		Username: username,
		TODOs:    []Item{},
	}
}

// AddItem creates a new todo item and adds it to the list.
// The amount of todo-items is the id of the newly created item.
//
// Returns the list
func (l *List) AddItem(author id.ActorID, projectID id.ProjectID, todoType Type, refType ReferenceType, refID string, deadline time.Time) *List {
	item := Item{
		Index:     len(l.TODOs),
		Author:    author,
		ProjectID: projectID,
		Type:      todoType,
		Reference: itemReference{
			Type: refType,
			ID:   refID,
		},
		Deadline:     deadline,
		CreationDate: time.Now().UTC().Round(time.Second),
		Done:         false,
	}
	l.TODOs = append(l.TODOs, item)
	return l
}
