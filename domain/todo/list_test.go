/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package todo

import (
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
)

func TestNewList(t *testing.T) {
	username := "test"
	todoList := NewList(username)

	if todoList.Username != username {
		t.Errorf("username was not correct. Was %v but expected %v", todoList.Username, username)
	}
	if len(todoList.TODOs) > 0 {
		t.Errorf("list of todos is not empty")
	}
}

func TestAddItem(t *testing.T) {
	todoList := List{
		Username: "test",
		TODOs:    []Item{},
	}

	author := id.NewActorID("test")
	projectID := id.NewProjectID("test", "testproject")
	refType := Case
	refID := "testCase"
	deadline := time.Now().UTC().Round(time.Second)

	todoList.AddItem(author, projectID, Assignment, refType, refID, deadline)

	newItem := todoList.TODOs[len(todoList.TODOs)-1]

	if newItem.Author != author {
		t.Errorf("name of author is %v, but %v was expected", newItem.Author, author)
	}
	if newItem.ProjectID != projectID {
		t.Errorf("project id was %v, but %v was expected", newItem.ProjectID, projectID)
	}
	if newItem.Reference.Type != refType {
		t.Errorf("type of reference was %v, but %v was expected", newItem.Reference.Type, refType)
	}
	if newItem.Reference.ID != refID {
		t.Errorf("id of reference was %v, but %v was expected", newItem.Reference.ID, refID)
	}
	if newItem.Index != len(todoList.TODOs)-1 {
		t.Errorf("index of item was %v, but %v was expected", newItem.Index, 0)
	}
	if newItem.Deadline != deadline {
		t.Errorf("deadline of item was %v, but %v was expected", newItem.Deadline, deadline)
	}
	if newItem.Done {
		t.Errorf("item was set to done. Expected was done to be false")
	}
}
