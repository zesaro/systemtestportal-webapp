/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"strings"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// The Case struct contains the information needed for describing a test case
type Case struct {
	Name             string
	TestCaseVersions []CaseVersion
	Project          id.ProjectID
	Labels           []project.Label
}

// CaseVersion contains the information needed for describing a version of a test case
type CaseVersion struct {
	VersionNr     int
	Message       string
	IsMinor       bool
	Description   string
	Preconditions []Precondition
	Versions      map[string]*project.Version
	Duration      duration.Duration
	Steps         []Step
	CreationDate  time.Time
	Case          id.TestID
	Tester        map[string]*user.User
}

// Rename renames the testcase. This might change its ID.
func (tc *Case) Rename(n string) {
	tc.Name = n
}

// ItemName returns a test cases name
func (tc Case) ItemName() string {
	return tc.Name
}

// ID returns a test cases id
func (tc Case) ID() id.TestID {
	return id.NewTestID(tc.Project, tc.Name, true)
}

// ID returns a testcase versions id
func (tcv CaseVersion) ID() id.TestVersionID {
	return id.NewTestVersionID(tcv.Case, tcv.VersionNr)
}

// NewTestCase creates a new test case with the given information
func NewTestCase(name, description string, preconditions []Precondition, labels []project.Label, versions map[string]*project.Version,
	dur duration.Duration, project id.ProjectID) Case {

	name = strings.TrimSpace(name)

	tc := Case{
		Name:    name,
		Project: project,
		Labels:  labels,
	}

	initVer := 1
	initMessage := "Initial test case created"

	tcv := NewTestCaseVersion(initVer, false, initMessage, description, preconditions, versions, dur, tc.ID())
	tc.TestCaseVersions = append(tc.TestCaseVersions, tcv)

	return tc
}

// NewTestCaseVersion creates a new version for a test case
// Returns the created test case version
func NewTestCaseVersion(version int, isMinor bool, message, description string, preconditions []Precondition,
	versions map[string]*project.Version, dur duration.Duration, testcase id.TestID) CaseVersion {

	description = strings.TrimSpace(description)
	message = strings.TrimSpace(message)

	tcv := CaseVersion{
		VersionNr:     version,
		Message:       message,
		IsMinor:       isMinor,
		Description:   description,
		Preconditions: preconditions,
		Versions:      versions,
		Duration:      dur,
		CreationDate:  time.Now().UTC().Round(time.Second),
		Case:          testcase,
		Tester:        map[string]*user.User{},
	}

	return tcv
}

//SetTestersOfTestCase sets the testers of the test case
func (tc *Case) SetTestersOfTestCase(tester []*user.User) {
	tc.TestCaseVersions[0].Tester = map[string]*user.User{}
	for _, t := range tester {
		tc.TestCaseVersions[0].Tester[t.Name] = t
	}
}
