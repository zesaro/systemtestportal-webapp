/*
This file is part of SystemTestPortal.
Copyright (C) 2017  Institute of Software Technology, University of Stuttgart

SystemTestPortal is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SystemTestPortal is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
*/

package test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/duration"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/id"
	"gitlab.com/stp-team/systemtestportal-webapp/domain/project"

	"gitlab.com/stp-team/systemtestportal-webapp/domain/user"
)

// TestNewTestSequence contains tests for the creation function of test sequences
func TestNewTestSequence(t *testing.T) {
	type tsArgs struct {
		name          string
		description   string
		preconditions []Precondition
		cases         []Case
	}
	tss := []struct {
		in   tsArgs
		want Sequence
	}{
		{
			in: tsArgs{
				cases: []Case{},
			},
			want: Sequence{
				Name:             "",
				Project:          defaultProject(),
				Labels:           []project.Label{},
				SequenceVersions: defaultTSVersion(id.NewTestID(defaultProject(), "", false)),
			},
		},
		{
			in: tsArgs{
				name:  "    ",
				cases: []Case{},
			},
			want: Sequence{
				Project:          defaultProject(),
				Labels:           []project.Label{},
				SequenceVersions: defaultTSVersion(id.NewTestID(defaultProject(), "", false)),
			},
		},
		{
			in:   tsArgs{name: "Test", cases: []Case{}},
			want: wantSequence("Test"),
		},
		{
			in:   tsArgs{name: "Test 1", cases: []Case{}},
			want: wantSequence("Test 1"),
		},
	}

	for _, ts := range tss {
		got, _ := NewTestSequence(ts.in.name, ts.in.description, ts.in.preconditions, []project.Label{}, ts.in.cases,
			defaultProject())

		if !equalSequences(got, ts.want) {
			t.Errorf("NewTestSequence(%s, %s, %v, %v) = \n%+v \nwant \n%+v", ts.in.name, ts.in.description,
				ts.in.preconditions, ts.in.cases, got, ts.want)
		}
	}
}

func wantSequence(name string) Sequence {
	return Sequence{
		Name:             name,
		Project:          defaultProject(),
		Labels:           []project.Label{},
		SequenceVersions: defaultTSVersion(id.NewTestID(defaultProject(), name, false)),
	}
}

// defaultTSVersion returns the default version of a test sequence that is created when the test sequence is created
func defaultTSVersion(parentSequence id.TestID) []SequenceVersion {
	return []SequenceVersion{
		{
			VersionNr:    1,
			Message:      "Initial test sequence created",
			IsMinor:      false,
			CreationDate: time.Now().UTC().Round(time.Second),
			Testsequence: parentSequence,
			Cases:        []Case{},
			SequenceInfo: SequenceInfo{
				Versions:      map[string]*project.Version{},
				DurationHours: 0,
				DurationMin:   0,
			},
			Tester: map[string]*user.User{},
		},
	}
}

func equalSequences(s1, s2 Sequence) bool {
	if s1.Project != s2.Project {
		return false
	}

	if s1.Name != s2.Name {
		return false
	}

	if !reflect.DeepEqual(s1.Labels, s2.Labels) {
		return false
	}

	if len(s1.SequenceVersions) != len(s2.SequenceVersions) {
		return false
	}

	for i := range s1.SequenceVersions {
		if !equalSequenceVersions(s1.SequenceVersions[i], s2.SequenceVersions[i]) {
			return false
		}
	}

	return true
}

func equalSequenceVersions(sv1, sv2 SequenceVersion) bool {
	if sv1.Testsequence != sv2.Testsequence {
		return false
	}

	if sv1.VersionNr != sv2.VersionNr {
		return false
	}

	if sv1.Description != sv2.Description {
		return false
	}

	if !reflect.DeepEqual(sv1.Preconditions, sv2.Preconditions) {
		return false
	}

	if sv1.CreationDate.Sub(sv2.CreationDate) > time.Second {
		return false
	}

	if !reflect.DeepEqual(sv1.Cases, sv2.Cases) {
		return false
	}

	return true
}

// TestCalculateSUTVersion tests the calculation of the SUTVersionFrom of a testsequence
func TestCalculateSUTVariants(t *testing.T) {
	sutGet, _ := CalculateSUTVariants(getTestCases())
	sutWant := map[string]*project.Version{
		"Variant 1": {
			Name: "Variant 1",
			Variants: []project.Variant{
				0: {Name: "0.0.1"},
				1: {Name: "0.1.0"},
			},
		},
	}
	if !reflect.DeepEqual(sutGet, sutWant) {
		t.Errorf("CalculateSUTVariants: \ngot: \n%s \nwant: \n%s", sutGet, sutWant)
	}

	sutGet, _ = CalculateSUTVariants(getTestCases2())
	sutWant = map[string]*project.Version{}
	if !reflect.DeepEqual(sutGet, sutWant) {
		t.Errorf("CalculateSUTVariants: \ngot: \n%s \nwant: \n%s", sutGet, sutWant)
	}
}

// TestCalculateSUTVersion2 tests the calculation of applicable variants in
// a test sequence. This test compares the sut variants of two test cases.
// One test case has one version with three variants, the other does not have
// any applicable versions. There should not be any applicable versions as result.
func TestCalculateSUTVariants2(t *testing.T) {
	testTCS := []Case{
		{
			Name:    "Test Case 1",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the first test case",
					Duration:    duration.NewDuration(1, 1, 0),
					Versions: map[string]*project.Version{"Variant 1": {
						Name: "Variant 1",
						Variants: []project.Variant{
							0: {Name: "0.0.1"},
							1: {Name: "0.1.0"},
							2: {Name: "1.0.0"},
						},
					}},
					Steps: ExampleSteps[0],
				},
			},
		},
		{
			Name:    "Test Case 2",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the second test case",
					Duration:    duration.NewDuration(2, 59, 0),
					Versions:    map[string]*project.Version{},
					Steps:       ExampleSteps[0],
				},
			},
		},
	}
	sutGet, _ := CalculateSUTVariants(testTCS)
	sutWant := map[string]*project.Version{}
	if !reflect.DeepEqual(sutGet, sutWant) {
		t.Errorf("CalculateSUTVersions: \ngot: \n%s \nwant: \n%s", sutGet, sutWant)
	}

	sutGet, _ = CalculateSUTVariants(getTestCases2())
	sutWant = map[string]*project.Version{}
	if !reflect.DeepEqual(sutGet, sutWant) {
		t.Errorf("CalculateSUTVersions: \ngot: \n%s \nwant: \n%s", sutGet, sutWant)
	}
}

// TestCalculateSUTVersion3 tests the calculation of applicable versions in
// a test sequence. This test compares the sut versions of three test cases.
// The first case has one version with three variants.
// The second case has two versions with each two variants. The first version contains
// two variants of the version of the first test case.
// The third case has three versions where one variant is equal to the version of
// the first test case.
// The result should be the version of the first test case with two variants.
func TestCalculateSUTVersion3(t *testing.T) {
	testTCS := []Case{
		{
			Name:    "Test Case 1",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the first test case",
					Duration:    duration.NewDuration(1, 1, 0),
					Versions: map[string]*project.Version{"Variant 1": {
						Name: "Variant 1",
						Variants: []project.Variant{
							0: {Name: "0.0.1"},
							1: {Name: "0.1.0"},
							2: {Name: "1.0.0"},
						},
					}},
					Steps: ExampleSteps[0],
				},
			},
		},
		{
			Name:    "Test Case 2",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the second test case",
					Duration:    duration.NewDuration(2, 59, 0),
					Versions: map[string]*project.Version{
						"Variant 1": {
							Name: "Variant 1",
							Variants: []project.Variant{
								0: {Name: "0.0.1"},
								1: {Name: "0.1.0"},
							},
						},
						"Variant 2": {
							Name: "Variant 2",
							Variants: []project.Variant{
								0: {Name: "asdf"},
								1: {Name: "xyz"},
								2: {Name: "abc"},
							},
						}},
					Steps: ExampleSteps[0],
				},
			},
		},
		{
			Name:    "Test Case 3",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "Third test case",
					Duration:    duration.NewDuration(2, 10, 0),
					Versions: map[string]*project.Version{
						"Variant 1": {
							Name: "Variant 1",
							Variants: []project.Variant{
								0: {Name: "0.0.1"},
								1: {Name: "0.1.0"},
								2: {Name: "someDeprecatedVersion"},
							},
						},
						"Variant 2": {
							Name: "Variant 2",
							Variants: []project.Variant{
								0: {Name: "0.0.1"},
								1: {Name: "0.1.0"},
								2: {Name: "1.0.0"},
							},
						},
						"Variant 3": {
							Name: "Variant 3",
							Variants: []project.Variant{
								0: {Name: "qwertzuiopü"},
								1: {Name: "asdfghjklöä"},
								2: {Name: "yxcvbnm"},
							},
						},
					},
					Steps: ExampleSteps[0],
				},
			},
		},
	}
	sutGet, _ := CalculateSUTVariants(testTCS)
	sutWant := map[string]*project.Version{
		"Variant 1": {
			Name: "Variant 1",
			Variants: []project.Variant{
				0: {Name: "0.0.1"},
				1: {Name: "0.1.0"},
			},
		},
	}
	if !reflect.DeepEqual(sutGet, sutWant) {
		t.Errorf("CalculateSUTVersions: \ngot: \n%s \nwant: \n%s", sutGet, sutWant)
	}
}

// TestCalculateSUTVersion tests the calculation of the Duration of a testsequence
func TestCalculateDuration(t *testing.T) {
	var cases = getTestCases()
	d := CalculateDuration(cases)
	if d != duration.NewDuration(4, 0, 0) {
		t.Errorf("CalculateSUTVersionFrom: got: %v+ want: %v+", d,
			duration.NewDuration(4, 0, 0))
	}
}

// TestUpdateInfo tests whether UpdateInfo correctly sets the SequenceInfo for a SequenceVersion
func TestUpdateInfo(t *testing.T) {
	var sequence = wantSequence("sequence")
	var sequenceVersion = &sequence.SequenceVersions[0]

	sequenceVersion.Cases = getTestCases()

	if len(sequenceVersion.SequenceInfo.Versions) > 0 {
		t.Errorf("Expected size of SequenceInfo.Versions to be 0, but is %+v", len(sequenceVersion.SequenceInfo.Versions))
	}
	sequenceVersion, err := UpdateInfo(sequenceVersion)

	if err != nil {
		t.Errorf("Got error when calling UpdateInfo")
	}

	sutGet := sequenceVersion.SequenceInfo
	sutWant := SequenceInfo{
		Versions: map[string]*project.Version{"Variant 1": {
			Name: "Variant 1",
			Variants: []project.Variant{
				0: {Name: "0.0.1"},
				1: {Name: "0.1.0"},
			},
		}},
		DurationHours: 4,
		DurationMin:   0,
	}

	if !reflect.DeepEqual(sutGet, sutWant) {
		t.Errorf("UpdateInfo: \ngot: \n%+v \nwant: \n%+v", sutGet, sutWant)
	}

}

func TestCheckVersionForApplicableVariantsOutOfBounds(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			t.Errorf("Panic occured during test")
		}
	}()
	tc := getTestCases()[0]
	applicableVersions := make(map[string]*project.Version)
	applicableVersions["Variant 1"] = &project.Version{
		Name: "Variant 1",
		Variants: []project.Variant{
			project.Variant{
				Name: "not",
			},
			project.Variant{
				Name: "in",
			},
			project.Variant{
				Name: "there",
			},
			project.Variant{
				Name: ".",
			},
		},
	}

	checkVersionForApplicableVariants(applicableVersions, tc)
}

//getTestCases returns some example testcases with one variant and two similar versions.
// The second test case has one additional version
func getTestCases() []Case {
	var cases = []Case{
		{
			Name:    "Test Case 1",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the first test case",
					Duration:    duration.NewDuration(1, 1, 0),
					Versions: map[string]*project.Version{"Variant 1": {
						Name: "Variant 1",
						Variants: []project.Variant{
							0: {Name: "0.0.1"},
							1: {Name: "0.1.0"},
						},
					}},
					Steps:  ExampleSteps[0],
					Tester: map[string]*user.User{},
				},
			},
		},
		{
			Name:    "Test Case 2",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the second test case",
					Duration:    duration.NewDuration(2, 59, 0),
					Versions: map[string]*project.Version{"Variant 1": {
						Name: "Variant 1",
						Variants: []project.Variant{
							0: {Name: "0.0.1"},
							1: {Name: "0.1.0"},
							2: {Name: "1.0.0"},
						},
					}},
					Steps:  ExampleSteps[0],
					Tester: map[string]*user.User{},
				},
			},
		},
	}
	return cases
}

//getTestCases returns an test case with one version containing one version. The other test case
// contains no versions.
func getTestCases2() []Case {
	var cases = []Case{
		{
			Name:    "Test Case 1",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the first test case",
					Duration:    duration.NewDuration(1, 1, 0),
					Versions: map[string]*project.Version{"Variant 1": {
						Name: "Variant 1",
						Variants: []project.Variant{
							0: {Name: "0.0.1"},
						},
					}},
					Steps:  ExampleSteps[0],
					Tester: map[string]*user.User{},
				},
			},
		},
		{
			Name:    "Test Case 2",
			Project: defaultProject(),
			TestCaseVersions: []CaseVersion{
				0: {
					VersionNr:   1,
					Description: "An example test case version for the second test case",
					Duration:    duration.NewDuration(2, 59, 0),
					Versions:    map[string]*project.Version{},
					Steps:       ExampleSteps[0],
					Tester:      map[string]*user.User{},
				},
			},
		},
	}
	return cases
}
