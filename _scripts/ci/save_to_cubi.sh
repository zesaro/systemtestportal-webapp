#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2018  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

# test if current commit is tagged
if git describe --exact-match --tags HEAD ; then
    # save build artifacts to a folder with the same name as the tag
    TARGET_FOLDER="$(git describe --tags)"
else
    # save build artifacts to nightly folder as they are from a regular non tagged nightly build
    TARGET_FOLDER="nightly"
fi

echo "The build files will be saved to:"
echo $TARGET_FOLDER

rm -rf /cubitruck-volume/${TARGET_FOLDER}
mkdir /cubitruck-volume/${TARGET_FOLDER}

cp -a /builds/stp-team/systemtestportal-webapp/binaries/. /cubitruck-volume/${TARGET_FOLDER}/


# set symlink from the latest folder to the most recent tagged build

# get the most recent tag from origin master
MOST_RECENT_TAG="$(git describe --tags --abbrev=0 --match v* `git rev-list --tags --max-count=1`)"

# if the current tag is equal to the most recent tag we add the symlink to latest
# if we rerun a tagged build pipline and it is not the most recent tag, the latest symlink will
# not be changed
if [ "$TARGET_FOLDER" == "$MOST_RECENT_TAG" ]; then
    rm -rf /cubitruck-volume/latest # remove old link
    ln -sfv /cubitruck-volume/${TARGET_FOLDER} /cubitruck-volume/latest
    echo $(readlink -- /cubitruck-volume/latest) # print symlink
fi

# print saved files on cubi for debugg purposes
cd /cubitruck-volume
ls -l

cd /cubitruck-volume/${TARGET_FOLDER}/
ls -l

