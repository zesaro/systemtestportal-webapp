#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2018  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

# import gpg keys
echo "$STP_GPG_PUBLIC" > stp-public.key
gpg --import stp-public.key
echo "$STP_GPG_PRIVATE" > stp-private.key
gpg --allow-secret-key-import --import stp-private.key

# output directory of the compile jobs: this directory has to be located in the /builds folder for
# gitlab to be able to save it as artifacts
FILES=/builds/stp-team/systemtestportal-webapp/binaries/*

# iterate over all binaries, sign them and package them
for file in $FILES; do
  echo $PWD
  echo "FILE: "$file
  # determine correct packaging method
  if [[ $file == *"windows"* ]]; then 
    pack_cmd="zip -r"
    file_name=$file".zip"
  else 
    pack_cmd="tar -cvzf"
    file_name=$file".tar.gz"
  fi

  echo "FILE_NAME"$file_name

  # sign the file
  gpg --detach-sign $file
  # verify that the file is properly signed
  gpg --verify $file.sig $file

  # copy file and signature to prevent having the entire directory structure in the tar/zip
  cp $file .
  cp $file.sig .
  
  # assume that the script is being called from .../cmd/stp
  $pack_cmd $file_name ./../../templates ./../../static ./../../migrations ./../../data ${file##*/} ${file##*/}.sig ./../../README.md ./../../LICENSE

done