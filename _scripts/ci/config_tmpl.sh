#!/usr/bin/env bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2017  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

BASE_PATH=$1
DATA_DIR=$2

cat  << EOF
# ------------------------------------------- #
#                SystemTestPortal             #
# ------------------------------------------- #

; The directory that STP should search for its
; resources.
basepath="$BASE_PATH"

; The directory that STP should stores its data
; in. When build with sqlite STP will store the database file
; there.
data-dir="$DATA_DIR"

; Sets the port that STP should listen on
; port=8080

; Sets the host that STP should interface with
; host=""

; Enable this for a more detailed log
; debug=false
EOF