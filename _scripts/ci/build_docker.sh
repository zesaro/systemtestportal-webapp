#!/bin/bash
#
# This file is part of SystemTestPortal.
# Copyright (C) 2018  Institute of Software Technology, University of Stuttgart
#
# SystemTestPortal is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SystemTestPortal is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with SystemTestPortal.  If not, see <http://www.gnu.org/licenses/>.
#

# get build artefacts
cp "${CI_PROJECT_DIR}/${NAME}-linux-amd64_${CI_COMMIT_REF_NAME}.tar.gz" ./stp.tar.gz
cp "${CI_PROJECT_DIR}/${NAME}-linux-amd64_${CI_COMMIT_REF_NAME}.tar.gz.sig" ./stp.tar.gz.sig

# verify signature
echo "$STP_GPG_PUBLIC" > stp-public.key
gpg --import stp-public.key
gpg --verify ./stp.tar.gz.sig ./stp.tar.gz && echo "Signature Valid" || { echo "Signature Invalid"; exit 1; }

GITLAB_CONTAINER_NAME=$CI_REGISTRY_IMAGE/signed:$CI_COMMIT_REF_SLUG
DOCKERHUB_CONTAINER_NAME=$DOCKERHUB_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

# Gitlab Registry
docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
docker build -t $GITLAB_CONTAINER_NAME .
docker push $GITLAB_CONTAINER_NAME

# Dockerhub
docker login -u "$DOCKERHUB_REGISTRY_USER" -p "$DOCKERHUB_REGISTRY_PASSWORD" $DOCKERHUB_REGISTRY #login to dockerhub
docker tag $GITLAB_CONTAINER_NAME $DOCKERHUB_CONTAINER_NAME
docker push $DOCKERHUB_CONTAINER_NAME # push to dockerhub

# test if current commit is tagged
if git describe --exact-match --tags HEAD ; then
    CURRENT_TAG="$(git describe --tags)"

    # get most recent tag from all branches that matches the pattern v*
    MOST_RECENT_TAG="$(git describe --tags --abbrev=0 --match v* `git rev-list --tags --max-count=1`)"
    echo "Most Recent Tags is: $MOST_RECENT_TAG"

    # if the current tag is equal to the most recent tag we push the image as latest
    if [ "$CURRENT_TAG" == "$MOST_RECENT_TAG" ]; then
    # tag the image as latest
        echo "Push Image as latest"
        docker tag $GITLAB_CONTAINER_NAME $DOCKERHUB_REGISTRY_IMAGE:latest
        docker push $DOCKERHUB_REGISTRY_IMAGE:latest
    fi
fi
